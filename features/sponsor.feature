@javascript
Feature: Question
  create and update with valid attributes
  Scenario: Create Question with multi choice
    Given I have a user, a city
    And I am on "/users/sign_in"
    Then I fill in "user_email" with "abc@gmail.com"
    And I fill in "user_password" with "123456"
    And I press "Login"
    And I am on "/sponsors/new"
    And I fill in "sponsor_company" with "Sponsor Company"
    And I fill in "sponsor_firstname" with "First"
    And I fill in "sponsor_lastname" with "Last"
    And I fill in "sponsor_street" with "The Street"
    And I type in "Bay Minette" into autocomplete list "city_name" and I choose "Bay Minette"
    And I fill in "sponsor_state" with "123"
    And I fill in "sponsor_postalcode" with "1234"
    And I fill in "sponsor_phone" with "123456789"
    And I fill in "sponsor_email" with "company@gmail.com"
    And I fill in "sponsor_amount" with "1"
    And I click "Create Sponsor"
    Then I should see "Sponsor was successfully created."

  Scenario: Update Sponsor with multi choice
    Given I have a user, a city
    And I have a Sponsor
    And I am on "/users/sign_in"
    Then I fill in "user_email" with "abc@gmail.com"
    And I fill in "user_password" with "123456"
    And I press "Login"
    And I am on "/sponsors/1/edit"
    And I fill in "sponsor_company" with "Sponsor Company Update"
    And I fill in "sponsor_firstname" with "First"
    And I fill in "sponsor_lastname" with "Last"
    And I fill in "sponsor_street" with "The Street"
    And I type in "Bay Minette" into autocomplete list "city_name" and I choose "Bay Minette"
    And I fill in "sponsor_state" with "123"
    And I fill in "sponsor_postalcode" with "1234"
    And I fill in "sponsor_phone" with "123456789"
    And I fill in "sponsor_email" with "company@gmail.com"
    And I fill in "sponsor_amount" with "1"
    And I click "Update Sponsor"
    Then I should see "Sponsor was successfully updated.."

Scenario: Delete Sponsor with multi choice
    Given I have a user, a city
    And I have a Sponsor
    And I am on "/users/sign_in"
    Then I fill in "user_email" with "abc@gmail.com"
    And I fill in "user_password" with "123456"
    And I press "Login"
    And I am on "/sponsors"
    And I should see "Listing sponsors"
    And I click "Destroy"
    Then A confirm message show for me
    And I wait for 2 seconds
    And I should not see "Sponsor Company Update"
