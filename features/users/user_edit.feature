@javascript

Feature: Edit User
  Scenario: : Edit User
    As a registered user of the website
    I want to edit my user profile
    so I can change my username

  #    Scenario: I sign in and edit my account
  #      Given I am logged in with student
  #      When I edit my account details
  #      Then I should see "newname"

#  Scenario: I add new emergency contact
#    Given I am logged in with student
#    And I am on "/users/edit"
#    And I click "btn_new_additional_emergency_contacts"
#    And The form emergency contact show
#    And I fill in "additional_emergency_contact_name" with "name"
#    And I fill in "additional_emergency_contact_address" with "address"
#    And I fill in "additional_emergency_contact_phone" with "123456"
#    And I fill in "additional_emergency_contact_email" with "test@gmail.com"
#    And I click "save-emergency-contact"
#    And I wait for 3 seconds

  Scenario: I edit emergency contact
    Given I am logged in with student
    And I have an additional_emergency_contacts
    And I am on "/users/edit"

    And I click edit emergency contact
    And I fill in "additional_emergency_contact_name" with "name edit"
    And I fill in "additional_emergency_contact_address" with "address edit"
    And I fill in "additional_emergency_contact_phone" with "123456123"
    And I fill in "additional_emergency_contact_email" with "test@gmail.com"
    And I click "update_emergency_contact"
    And I wait for 3 seconds
