@javascript
Feature: Assignment
  create and update with valid attributes
  Scenario: Create Assignment
    Given I am logged in with teacher
    And I have list assignment library
    And I have list student
    And I am on "/assignments/new"
    And I fill in "assignment_name" with "Assignment name"
    And I fill in "assignment_description" with "Assignment description"
    And I select "assignment library" from "assignment_assignment_library_id"
    And I click "Choice student"
    And I wait for 3 seconds
    And The popup show
    And I click "Create Assignment"
    Then I should see "Assignment name"

  Scenario: edit Assignment
    Given I am logged in with teacher
    And I have list assignment library
    And I have list student
    And I have an assignment
    And I am on "/assignments/1/edit"
    Then I should see "Editing assignment"
    And I fill in "assignment_name" with "Assignment name edit"
    And I fill in "assignment_description" with "Assignment description edit"
    And I select "assignment library 2" from "assignment_assignment_library_id"
    And I click "Update Assignment"
    Then I should see "Assignment name edit"

  Scenario: delete Assignment
    Given I am logged in with teacher
    And I have an assignment
    And I am on "/assignments"
    And I click "Destroy"
    Then A confirm message show for me
    Then I should not see "Assignment name"