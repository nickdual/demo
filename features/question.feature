@javascript
Feature: Question
  create and update with valid attributes
    Scenario: Create Question with multi choice
      Given I have a user
      And I am on "/users/sign_in"
      Then I fill in "user_email" with "abc@gmail.com"
      And I fill in "user_password" with "123456"
      And I press "Login"
      And I am on "/questions/new"
      And I fill in "question_name" with "multiple choice"
      And  I select "Multiple Choice" from "question_bodies_question_type"
      And I fill in "question_bodies_point" with "100"
      And I fill in "question_bodies_question" with "What?"
      And I fill in "choices_1" with "A"
      And I fill in "choices_2" with "B"
      And I fill in "choices_3" with "C"
      And I fill in "choices_4" with "D"
      And I fill in "choices_5" with "E"
      And I click "Create Question"
      Then I should see "Question was successfully created."


    Scenario: Create Question with multi answer
      Given I have a user
      And I am on "/users/sign_in"
      Then I fill in "user_email" with "abc@gmail.com"
      And I fill in "user_password" with "123456"
      And I press "Login"
      And I am on "/questions/new?choice=multiple_answer"
      And I fill in "question_name" with "multiple answer"
      And  I select "Multiple Answer" from "question_bodies_question_type"
      And I fill in "question_bodies_point" with "100"
      And I fill in "question_bodies_question" with "What?"
      And I fill in "answer_1" with "B"
      And I fill in "answer_2" with "C"
      And I fill in "answer_3" with "D"
      And I fill in "answer_4" with "E"
      And I fill in "answer_5" with "F"
      And I click "Create Question"
      Then I should see "Question was successfully created."

    Scenario: Create Question with true false
      Given I have a user
      And I am on "/users/sign_in"
      Then I fill in "user_email" with "abc@gmail.com"
      And I fill in "user_password" with "123456"
      And I press "Login"
      And I am on "/questions/new?choice=true/false"
      And I fill in "question_name" with "true false"
      And  I select "True/False" from "question_bodies_question_type"
      And I fill in "question_bodies_point" with "100"
      And I fill in "question_bodies_question" with "What?"
      And I choose "question_bodies_options_value_false"
      And I check "question_bodies_questions_random_order"
      And I click "Create Question"
      Then I should see "Question was successfully created."


    Scenario: Create Question with matching
      Given I have a user
      And I am on "/users/sign_in"
      Then I fill in "user_email" with "abc@gmail.com"
      And I fill in "user_password" with "123456"
      And I press "Login"
      And I am on "/questions/new?choice=matching"
      And I fill in "question_name" with "true false"
      And  I select "Matching" from "question_bodies_question_type"
      And I fill in "question_bodies_point" with "100"
      And I fill in "question_bodies_question" with "What?"
      And I fill in "stems_1" with "A"
      And I fill in "choices_1" with "1"
      And I fill in "stems_2" with "B"
      And I fill in "choices_2" with "2"
      And I fill in "stems_3" with "C"
      And I fill in "choices_3" with "3"
      And I fill in "stems_4" with "D"
      And I fill in "choices_4" with "4"
      And I fill in "stems_5" with "E"
      And I fill in "choices_5" with "5"
      And I click "Create Question"
      Then I should see "Question was successfully created."


    Scenario: Create Question with sorting
      Given I have a user
      And I am on "/users/sign_in"
      Then I fill in "user_email" with "abc@gmail.com"
      And I fill in "user_password" with "123456"
      And I press "Login"
      And I am on "/questions/new?choice=sorting"
      And I fill in "question_name" with "sorting"
      And  I select "Sorting" from "question_bodies_question_type"
      And I fill in "question_bodies_point" with "100"
      And I fill in "question_bodies_question" with "What?"
      And I fill in "answer_1" with "B"
      And I fill in "answer_2" with "C"
      And I fill in "answer_3" with "D"
      And I fill in "answer_4" with "E"
      And I fill in "answer_5" with "F"
      And I click "Create Question"
      Then I should see "Question was successfully created."


    Scenario: Create Question with short answer
      Given I have a user
      And I am on "/users/sign_in"
      Then I fill in "user_email" with "abc@gmail.com"
      And I fill in "user_password" with "123456"
      And I press "Login"
      And I am on "/questions/new?choice=short_answer"
      And I fill in "question_name" with "short answer"
      And  I select "Short Answer" from "question_bodies_question_type"
      And I fill in "question_bodies_point" with "100"
      And I fill in "question_bodies_question" with "What?"
      And I fill in "question_bodies_answer" with "answer test short answer"
      And I click "Create Question"
      Then I should see "Question was successfully created."


    Scenario: Create Question with long answer
      Given I have a user
      And I am on "/users/sign_in"
      Then I fill in "user_email" with "abc@gmail.com"
      And I fill in "user_password" with "123456"
      And I press "Login"
      And I am on "/questions/new?choice=long_answer"
      And I fill in "question_name" with "long answer"
      And  I select "Long Answer" from "question_bodies_question_type"
      And I fill in "question_bodies_point" with "100"
      And I fill in "question_bodies_question" with "What?"
      And I fill in "question_bodies_answer" with "answer test long answer"
      And I click "Create Question"
      Then I should see "Question was successfully created."



    Scenario: Update Question with multi choice
      Given I have a user
      And I have a Question "multi choice"
      And I am on "/users/sign_in"
      Then I fill in "user_email" with "abc@gmail.com"
      And I fill in "user_password" with "123456"
      And I press "Login"
      And I am on "/questions/1/edit?choice=multiple_choice"
      And I fill in "question_name" with "multiple choice update"
      And I fill in "question_bodies_point" with "100"
      And I fill in "question_bodies_question" with "What?"
      And I fill in "choices_1" with "A"
      And I fill in "choices_2" with "B"
      And I fill in "choices_3" with "C"
      And I fill in "choices_4" with "D"
      And I fill in "choices_5" with "E"
      And I click "Update Question"
      Then I should see "Question was successfully updated."


    Scenario: Update Question with multi answer
      Given I have a user
      And I have a Question "multi answer"
      And I am on "/users/sign_in"
      Then I fill in "user_email" with "abc@gmail.com"
      And I fill in "user_password" with "123456"
      And I press "Login"
      And I am on "/questions/2/edit?choice=multiple_answer"
      And I fill in "question_name" with "multiple answer update"
      And I fill in "question_bodies_point" with "200"
      And I fill in "question_bodies_question" with "What is this?"
      And I fill in "answer_1" with "B"
      And I fill in "answer_2" with "C"
      And I fill in "answer_3" with "D"
      And I fill in "answer_4" with "E"
      And I fill in "answer_5" with "F"
      And I click "Update Question"
  #    Then I should see "Question was successfully updated."


    Scenario: Update Question with true false
      Given I have a user
      And I have a Question "true false"
      And I am on "/users/sign_in"
      Then I fill in "user_email" with "abc@gmail.com"
      And I fill in "user_password" with "123456"
      And I press "Login"
      And I am on "/questions/3/edit?choice=true/false"
      And I fill in "question_name" with "true false update"
      And I fill in "question_bodies_point" with "300"
      And I fill in "question_bodies_question" with "What is this?"
      And I choose "question_bodies_options_value_false"
      And I check "question_bodies_questions_random_order"
      And I click "Update Question"
      Then I should see "Question was successfully updated."

    Scenario: Update Question with matching
      Given I have a user
      And I have a Question "matching"
      And I am on "/users/sign_in"
      Then I fill in "user_email" with "abc@gmail.com"
      And I fill in "user_password" with "123456"
      And I press "Login"
      And I am on "/questions/4/edit?choice=matching"
      And I fill in "question_name" with "matching update"
      And I fill in "question_bodies_point" with "3000"
      And I fill in "question_bodies_question" with "What is this?"
      And I fill in "stems_1" with "AA"
      And I fill in "choices_1" with "1"
      And I fill in "stems_2" with "BB"
      And I fill in "choices_2" with "2"
      And I fill in "stems_3" with "CC"
      And I fill in "choices_3" with "3"
      And I fill in "stems_4" with "DD"
      And I fill in "choices_4" with "4"
      And I fill in "stems_5" with "EE"
      And I fill in "choices_5" with "5"
      And I click "Update Question"
      Then I should see "Question was successfully updated."


    Scenario: Update Question with sorting
      Given I have a user
      And I have a Question "sorting"
      And I am on "/users/sign_in"
      Then I fill in "user_email" with "abc@gmail.com"
      And I fill in "user_password" with "123456"
      And I press "Login"
      And I am on "/questions/5/edit?choice=sorting"
      And I fill in "question_name" with "sorting update"
      And I fill in "question_bodies_point" with "200"
      And I fill in "question_bodies_question" with "What is this?"
      And I fill in "answer_1" with "BB"
      And I fill in "answer_2" with "CC"
      And I fill in "answer_3" with "DD"
      And I fill in "answer_4" with "EE"
      And I fill in "answer_5" with "FF"
      And I click "Update Question"
      Then I should see "Question was successfully updated."


    Scenario: Update Question with short answer
      Given I have a user
      And I have a Question "short answer"
      And I am on "/users/sign_in"
      Then I fill in "user_email" with "abc@gmail.com"
      And I fill in "user_password" with "123456"
      And I press "Login"
      And I am on "/questions/6/edit?choice=short_answer"
      And I fill in "question_name" with "short answer update"
      And I fill in "question_bodies_point" with "200"
      And I fill in "question_bodies_question" with "What is this?"
      And I fill in "question_bodies_answer" with "answer test short update"
      And I click "Update Question"
      Then I should see "Question was successfully updated."

  #
    Scenario: Update Question with long answer
      Given I have a user
      And I have a Question "long answer"
      And I am on "/users/sign_in"
      Then I fill in "user_email" with "abc@gmail.com"
      And I fill in "user_password" with "123456"
      And I press "Login"
      And I am on "/questions/7/edit?choice=long_answer"
      And I fill in "question_name" with "long answer update"
      And I fill in "question_bodies_point" with "200"
      And I fill in "question_bodies_question" with "What is this?"
      And I fill in "question_bodies_answer" with "answer test long update"
      And I click "Update Question"
      Then I should see "Question was successfully updated."


  Scenario: Delete Question
    Given I am logged in with teacher
    And I have a Question "long answer"
    And I am on "/questions"
    And I should see "Long Answer"
    Then I click "Destroy"
    Then A confirm message show for me
    And I should not see "Long Answer"

  Scenario: show Question
    Given I am logged in with teacher
    And I have a Question "long answer"
    And I am on "/questions"
    And I should see "Long Answer"
    Then I click "Show"
    And I should see "Long Answer"


  Scenario: render edit Question page
    Given I am logged in with teacher
    And I have a Question "long answer"
    And I am on "/questions"
    And I should see "Long Answer"
    Then I click "Edit"
    And I should see "Long Answer" in "question_name"

  Scenario: render new Question page
    Given I am on "/questions"
    And I click "New Question"
    Then I must be on "/questions/new"