When /^I have list student/ do
  @user3 = FactoryGirl.create(:user3)
  @user4 = FactoryGirl.create(:user4)
  @user5 = FactoryGirl.create(:user5)
  @user3.add_role :student
  @user4.add_role :student
  @user5.add_role :student
  end
When /^I have list question/ do
  FactoryGirl.create(:question_multi_choice)
  FactoryGirl.create(:question_multi_answer)
  FactoryGirl.create(:question_true_false)
  FactoryGirl.create(:question_match)
  FactoryGirl.create(:question_sorting)
  FactoryGirl.create(:question_sort_answer)
  FactoryGirl.create(:question_long_answer)
end

Given /^I have a content data$/ do
  @content = FactoryGirl.create(:content)
  end
Given /^I have an additional_emergency_contacts$/ do
  @emergency_contact = FactoryGirl.create(:additional_emergency_contact)
  puts(@emergency_contact.to_yaml)
end
Given /^I have a user, a city$/ do
  @user = FactoryGirl.create(:user)
  @user.add_role :teacher
  @country = FactoryGirl.create(:country)
  @city = FactoryGirl.create(:city)

end
Given /^I have a user$/ do
  @user = FactoryGirl.create(:user)
  @user.add_role :teacher
  end
Given /^I have list assignment library$/ do
  @assignment_library = FactoryGirl.create(:assignment_library)
  @assignment_library2 = FactoryGirl.create(:assignment_library2)
end
Given /^I have an assignment library$/ do
  @assignment_library = FactoryGirl.create(:assignment_library)
end

Given /^I have an assignment/ do
  @assignment = FactoryGirl.create(:assignment)
end
Given /^I have list grade system/ do
  @grade_system = FactoryGirl.create(:grade_system)
  @grade_system2 = FactoryGirl.create(:grade_system2)
end
Given /^I have a grade system/ do
  @grade_system = FactoryGirl.create(:grade_system)
end

Given /^I have a school class/ do
  @school_class = FactoryGirl.create(:school_class)
end
Given /^I have list institution/ do
  @institution = FactoryGirl.create(:institution)
end
Given /^I have list room/ do
  @room = FactoryGirl.create(:room)
end

Given /^I am on "(.*?)"$/ do |path|
  visit path
end
Then /^I fill in "(.*?)" with "(.*?)"$/ do |field, value|
  fill_in(field, :with => value)
  end
Then /^I fill in tinymce with "(.*?)"$/ do |value|
  evaluate_script("tinyMCE.activeEditor.setContent('" + value + "');")
end
Then /^I select "(.*?)" from "(.*?)"$/ do |option, field|
  page.select option, :from => field
end

Then /^I check "([^"]*)"/ do |class_name|
  check(class_name)
end

When /^I press "([^"]*)"$/ do |button|
  click_button(button)
end

When /^(?:|I )click "([^"]*)"$/ do |button|
  click_on(button)

end
When /^(?:|I )follow "([^"]*)"$/ do |link|
  click_link(link)
end
Then /^I should see "([^"]*)" in "([^"]*)"$/ do |content, id|
  find_field(id).value.should == content
  end
Then /^I should see "([^"]*)"$/ do |content|
  page.should have_content(content)
  end
Then /^I should not see "([^"]*)"$/ do |content|
  page.should_not have_content(content)
end
When /^I wait for (\d+) seconds?$/ do |secs|
  sleep secs.to_i
end

Then /^A confirm message show for me$/ do
  page.driver.browser.switch_to.alert.accept
end

Then /^I must be on "([^"]*)"$/ do |page_name|
  URI.parse(current_url).path.should == page_name
end

When /^I choose "([^\"]*)"$/ do |field|
  choose(field)
end
When /^I have a Question "([^\"]*)"$/ do |type|
  case type
    when /multi choice/
      multi_choice = FactoryGirl.create(:question_multi_choice)
    when /multi answer/
      multi_answer = FactoryGirl.create(:question_multi_answer)
    when /true false/
     true_false = FactoryGirl.create(:question_true_false)
    when /matching/
      matching = FactoryGirl.create(:question_match)
    when /sorting/
      sorting = FactoryGirl.create(:question_sorting)
    when /short answer/
      short_answer = FactoryGirl.create(:question_sort_answer)
    when /long answer/
     long_answer = FactoryGirl.create(:question_long_answer)
  end
end
When /^I have a Sponsor$/ do
  sponsor = FactoryGirl.create(:sponsor)
end
When(/^I select the option containing "([^\"]*)" in the autocomplete$/) do |text|
  locate("li:ui-menu-item('#{text}')").click
end
When /^I type in "([^\"]*)" into autocomplete list "([^\"]*)" and I choose "([^\"]*)"$/ do |typed, input_name,should_select|
  page.driver.browser.execute_script %Q{ $('#sponsor_city_id').val('103800') }
  page.driver.browser.execute_script %Q{ $('input.ui-autocomplete-input').trigger("focus") }
  fill_in("#{input_name}",:with => typed)
  page.driver.browser.execute_script %Q{ $('input.ui-autocomplete-input').trigger("keydown") }
  sleep 10
  page.driver.browser.execute_script %Q{ $('.ui-menu-item a:contains("#{should_select}")').trigger("mouseenter").trigger("click"); }
end

When /^I check all question$/ do
  page.check('check_all')
end
When /^I check all student$/ do
  page.check('checkall')
  end
When /^I check check_all$/ do
  page.check('check_all')
end
When /^I click edit emergency contact$/ do
  page.click('Edit')
end

And /^The popup show$/ do
  begin
    main, popup = page.driver.browser.window_handles
    within_window(popup) do
      page.check "assignment_user_ids_13"
      page.check "assignment_user_ids_14"
      page.check "Close"

    end
  rescue
  end

end
When /^I sleep "([^"]*)"$/ do |second|
  sleep second.to_i
end
When /^I am on invitation_accept$/ do
  path = "/users/invitation/accept?invitation_token=" + User.find_by_email("abc@gmail.com").invitation_token
  puts path
  visit path
end
Given /^I am not signed in$/ do
  page.driver.submit :delete, "/users/sign_out", {}
end
When /^I have list races,health, residence$/ do
  require 'rake'
  Rake::Task.clear
  Cfa::Application.load_tasks
  Rake::Task['db:seed_fu'].reenable
  Rake::Task['db:seed_fu'].invoke
end

Then /^The form emergency contact show$/ do
  page.driver.browser.execute_script %Q{ $('#new_emergency_contact').show() }
end