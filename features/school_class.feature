@javascript
Feature: school class
  create and update with valid attributes
  Scenario: Create school class
    Given I am logged in with teacher
    And I have list institution
    And I have list room
    And I am on "/school_classes/new"
    And I select "My institution" from "institution_id"
    And I select "My room" from "room_id"
    And I click "Create School Class"
    Then I should see "Editing school_class"
#
  Scenario: edit school class
    Given I am logged in with teacher
    And I have list institution
    And I have a school class
    And I have list room
    And I have list student
    And I am on "/school_classes/1/edit"
    And I fill in "key_search_student" with "a"
    And I click "search_student"
    And I wait for 3 seconds
    And I check all student
    And I click "Update School Class"
    Then I should see "School class was successfully updated."

  Scenario: delete school class
    Given I am logged in with teacher
    And I have list institution
    And I have a school class
    And I have list room
    And I am on "/school_classes"
    And I click "Destroy"
    Then A confirm message show for me
