@javascript
Feature: Teacher invite

  Scenario: Send mail invite
    Given I have a user
    And I am on "/users/sign_in"
    Then I fill in "user_email" with "abc@gmail.com"
    And I fill in "user_password" with "123456"
    And I press "Login"
    And I am on "/users/invitation/new"
    And I fill in "user_email" with "abc@gmail.com"
    And  I press "Send an invitation"
    And I sleep "10"
    And I am on "/users/sign_out"
    And I am on invitation_accept
    Then I should see "Set your password"
    And I fill in "user_password" with "abc123"
    And I fill in "user_password_confirmation" with "abc123"
    And  I press "Set my password"
    Then I should see "Grading"
