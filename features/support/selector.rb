module HtmlSelectorsHelper
  def selector_for(scope)
    case scope
      when /the body/
        "html > body"
      when /the fisrt input answer$/
        ("div#answer").child('p').child("question_bodies_answer__text")
      when /the (notice|error|info) flash$/
        ".flash.#{$1}"

      else
        raise "Can't find mapping from \"#{scope}\" to a selector.\n" +
                  "Now, go and add a mapping in #{__FILE__}"
    end
  end
end

World(HtmlSelectorsHelper)