@javascript
Feature: Content
  Scenario: Create content with valid attributes
    Given I am on "/contents/new"
    Then I fill in "content_name" with "content name test"
    And I fill in tinymce with "the test description"
    And I check "content_draft"
    And I press "Create Content"
    And I wait for 2 seconds
    Then I should see "Content was successfully created."
    And I should see "content name test"

  Scenario: Create content with invalid attributes
    Given I am on "/contents/new"
    Then I fill in "content_name" with ""
    And I fill in tinymce with "the test description"
    And I check "content_draft"
    And I press "Create Content"
    And I wait for 2 seconds
    Then I should see "Please review the problems below:"
    And I should see "can't be blank"

  Scenario: Update content with valid attributes
    Given I have a content data
    And I am on "/contents/1/edit"
    Then I fill in "content_name" with "content name test update"
    And I fill in tinymce with "the update description"
    And I check "content_draft"
    And I press "Update Content"
    And I wait for 2 seconds
    Then I should see "Content was successfully updated."
    And I should see "content name test update"

  Scenario: Update content with invalid attributes
    Given I have a content data
    And I am on "/contents/1/edit"
    Then I fill in "content_name" with ""
    And I fill in tinymce with "the test description"
    And I check "content_draft"
    And I press "Update Content"
    And I wait for 2 seconds
    Then I should see "Please review the problems below:"
    And I should see "can't be blank"

  Scenario: Delete content
    Given I have a content data
    And I am on "/contents"
    And I should see "MyString"
    Then I click "Destroy"
    Then A confirm message show for me
    And I should not see "MyString"

  Scenario: show content
    Given I have a content data
    And I am on "/contents"
    And I should see "MyString"
    Then I click "Show"
    And I should see "MyString"
    And I should see "MyText"

  Scenario: render edit content page
    Given I have a content data
    And I am on "/contents"
    And I should see "MyString"
    Then I click "Edit"
    And I should see "MyString" in "content_name"

  Scenario: render new content page
    Given I am on "/contents"
    And I click "New Content"
    Then I must be on "/contents/new"
