@javascript
Feature: Assignment library
  create and update with valid attributes
  Scenario: Create and edit Assignment library
    Given I have a user
    And I have list question
    And I am logged in with teacher
    And I am on "/assignment_libraries/new"
    And I fill in "assignment_library_name" with "Assignment name"
    And I fill in "assignment_library_description" with "Assignment description"
    And I click "Create Assignment library"
    Then I should see "Editing assignment library"
    And I check check_all
    And I click "Add Questions"
    And I press "Update Assignment library"
    Then I should see "Assignment Library was successfully updated."
  Scenario: delete Assignment library
    Given I have a user
    And I have list question
    And I have an assignment library
    And I am logged in with teacher
    And I am on "/assignment_libraries"
    And I click "Destroy"
    Then A confirm message show for me
    And I wait for 3 seconds
    Then I should not see "assignment library"

