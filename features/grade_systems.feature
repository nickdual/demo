@javascript
Feature: GradeSystem
  create and update with valid attributes
  Scenario: Create GradeSystem
    Given I am logged in with teacher
    And I am on "/grade_systems/new"
    And I fill in "grade_system_name" with "grade system name"
    And I fill in "grade_system_settings" with "A+"
    And I click "Create Grade system"
    Then I should see "Grade system was successfully created."

  Scenario: edit GradeSystem
    Given I am logged in with teacher
    And I have list grade system
    And I am on "/grade_systems/1/edit"
    And I fill in "grade_system_name" with "grade system name edit"
    And I fill in "grade_system_settings" with "A"
    And I click "Update Grade system"
    Then I should see "Grade system was successfully updated."
#
  Scenario: delete GradeSystem
    Given I am logged in with teacher
    And I have a grade system
    And I am on "/grade_systems"
    And I click "Destroy"
    Then A confirm message show for me
    Then I should not see "grade system name"