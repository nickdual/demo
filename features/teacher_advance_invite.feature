@javascript
Feature: Teacher invite

  Scenario: Send mail invite
    Given I have a user
    And  I have list races,health, residence
    And I am on "/users/sign_in"
    Then I fill in "user_email" with "abc@gmail.com"
    And I fill in "user_password" with "123456"
    And I press "Login"
    And I am on "/advanced_invitation"
    And I fill in "user_first" with "First"
    And I fill in "user_middle" with "Middle"
    And I fill in "user_last" with "Last"
    And I fill in "user_email" with "abc@gmail.com"
    And I fill in "user_additional_emergency_contact_name" with "abc@gmail.com"
    And I fill in "user_additional_emergency_contact_address" with "abc@gmail.com"
    And I fill in "user_additional_emergency_contact_phone" with "abc@gmail.com"
    And I fill in "user_additional_emergency_contact_email" with "abc@gmail.com"
    And I press "add more parent info"
    And I check "user_race_ids_1"
    And I check "user_health_ids_1"
    And I choose "user_residence_id_1"
    And I press "Submit invitation"

