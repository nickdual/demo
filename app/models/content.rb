require "nokogiri"
class Content < ActiveRecord::Base
  include Tire::Model::Search
  include Tire::Model::Callbacks
  attr_accessible  :id, :name, :body, :user_id, :draft, :created_at, :updated_at, :parent_id,:tag_list

  validates :name, :presence => true, :uniqueness => true
  acts_as_taggable
  has_many :sub_contents, :class_name => "Content",
           :foreign_key => "parent_id"
  belongs_to :parent, :class_name => "Content"

  belongs_to :user
  def self.content_parents
    self.where(:parent_id => nil).map{ |item| [item.name, item.id] }
  end

  def self.fix_images
    self.all.each do |content|
      doc = Nokogiri::HTML.fragment(content.body)
      doc.css('img').each do |image|
        @query = image['src'].split('/').last
        puts @query
        image['src'] = ContentPhoto.find_by_image(@query).image.url
        content.body = doc.to_html
        if content.save
          puts content.id
        end

      end
    end
  end
  def self.fix_image_azure
    number = 0
    self.all.each do |content|
      content.update_attribute('body', content.body.gsub('https://cfa-contents.s3.amazonaws.com/images/', 'http://cfa.blob.core.windows.net/cfa/contents/'))
      number = number + 1
    end
    puts number
  end
  def self.update_body
    @contents = Content.all
    @contents.each do |content|
      content.update_attribute("body", content.body)
    end
  end

  scope :by_date, order("created_at DESC")

end
