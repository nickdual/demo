class Race < ActiveRecord::Base

  attr_accessible :code, :comment, :name
  has_and_belongs_to_many :users
end
