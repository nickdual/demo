class Address < ActiveRecord::Base
  attr_accessible :city_id, :comment, :postal_code, :state_id, :street, :street2, :user_id
  belongs_to :city
  belongs_to :state
end
