class SchoolGrade < ActiveRecord::Base
  attr_accessible :name
  def self.select_school_grade
    self.all.map { |item| [item.name,item.id]}
  end
end
