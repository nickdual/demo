class GradingDetail < ActiveRecord::Base
  attr_accessible :body, :grading_id, :question_id
  serialize :body
  belongs_to :grading
  belongs_to :question

end
