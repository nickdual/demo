class Element < ActiveRecord::Base
  attr_accessible :concept_id, :name
  belongs_to :concept
end
