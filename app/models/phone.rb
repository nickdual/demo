class Phone < ActiveRecord::Base
  attr_accessible :phone, :phonetype_id, :user_id
  belongs_to :user
end
