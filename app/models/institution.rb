class Institution < ActiveRecord::Base
  attr_accessible :city_id, :comment, :logo, :name, :postal_code, :state_id, :street, :street2,:country_id
  has_many :users

  mount_uploader :logo, InstitutionLogo
  def self.select_institution
    self.all.map { |item| [item.name,item.id]}
  end


end
