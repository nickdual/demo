class AdditionalEmergencyContact < ActiveRecord::Base
  attr_accessible :address, :name, :phone, :email, :user_id,:education_ids
  validates :name, :presence => true
  validates :phone, :presence => true
  validates :email, :presence => true
  validates :user_id, :presence => true
  has_and_belongs_to_many :educations

end
                                  
