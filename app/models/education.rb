class Education < ActiveRecord::Base
  attr_accessible :code, :name
  has_and_belongs_to_many :additional_emergency_contacts
end
