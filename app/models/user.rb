class User < ActiveRecord::Base
  rolify

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  # Setup accessible (or protected) attributes for your model
  attr_accessible :role_ids, :as => :admin
  attr_accessible :name, :email, :password, :password_confirmation, :remember_me ,:first,:last,:middle,:dob ,:school_class_id, :residence_id, :race_ids, :health_ids
  validates :email, :presence => true
  # has_and_belongs_to_many(:organizations,
  #                           :join_table => "competitors_organizations",
  #                           :foreign_key => "organization_a_id",
  #                           :association_foreign_key => "organization_b_id",
  #                           :uniq => true)
  has_and_belongs_to_many(:users, :join_table => "relationships", :foreign_key => "user_id_a", :association_foreign_key => "user_id_b", :uniq => true)
  belongs_to :institution
  belongs_to :school_class
  belongs_to :residence

  has_and_belongs_to_many :races
  has_many :additional_emergency_contacts, :dependent => :destroy
  attr_accessible :additional_emergency_contacts_attributes
  accepts_nested_attributes_for :additional_emergency_contacts, :allow_destroy => true, :reject_if => proc { |attributes| attributes['name'].blank? or attributes['phone'].blank? }
  has_and_belongs_to_many :healths
  has_and_belongs_to_many :assignments
  has_many :gradings
  has_many :contents
  has_many :questions
  #validate :additional_emergency_contacts, :presence => true

  def role_names
    names = ""
    self.roles.each do |role|
      if names == ""
        names = role.name.titleize
      else
        names += ", " + role.name.titleize
      end

    end
    names
  end

  def students
    self.where()
  end
  
end
