class Grading < ActiveRecord::Base
  attr_accessible :body, :assignment_id, :user_id, :grade_system_id
  serialize :body
  belongs_to :assignment
  belongs_to :user
  belongs_to :grade_system
  has_many :grading_details
  acts_as_commentable

end
