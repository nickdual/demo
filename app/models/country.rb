class Country < ActiveRecord::Base
  attr_accessible :iso_code_two_letter, :iso_code_three_letter, :iso_number, :name, :capital, :continent, :geonames_id
  has_many :cities
  def self.select_countries
    self.all.map { |item| [item.name,item.id]}
  end
end