class Sponsor < ActiveRecord::Base
  include ActiveModel::Validations
  attr_accessible :amount, :city_id, :company, :email, :firstname, :lastname, :phone, :postalcode, :state, :street
  belongs_to :city
  validates :city_id,:presence => true
  validates :company, :presence => true
  validates :email, :presence => true,:email => true, :uniqueness => true
  acts_as_commentable

end
