class Uploader < ActiveRecord::Base
  attr_accessible :description, :filename
  require 'waz-blobs'
  #cred = {
  #  'production' =>
  #    { name: 'cfa',
  #      key: 'vBrSHGlHjd1BvCI5yot6yLUx3fEn5zXQZA8DGaTDsw7pvHa08A0uFfxnSxv8cOqs2GD5aTwYDW6cloP8zbpj6Q==' },
  #  'development' =>
  #    { name: 'cfa',
  #      key: 'vBrSHGlHjd1BvCI5yot6yLUx3fEn5zXQZA8DGaTDsw7pvHa08A0uFfxnSxv8cOqs2GD5aTwYDW6cloP8zbpj6Q=='}
  #}
  #Cfa::Application.config.credential = cred
  WAZ::Storage::Base.establish_connection!(:account_name => 'cfa', :access_key => 'vBrSHGlHjd1BvCI5yot6yLUx3fEn5zXQZA8DGaTDsw7pvHa08A0uFfxnSxv8cOqs2GD5aTwYDW6cloP8zbpj6Q==')
  def store(data)
    mime = MimeMagic.by_path(filename).to_s
    mime = 'application/octet-stream' if mime == ''
    cont = get_container('cfa')
    cont.store(_blob_name, data, mime)
  end

  def size_mb
    (storage_size.to_f / 1000.0).ceil / 1000.0
  end

  def url
    "http://#{cred}.blob.core.windows.net/#{cred}/#{_blob_name}"
  end

  def _blob_name
    'images/'+ _blob_base
  end
  #def _blob_name
  #  _blob_base + File::extname(name)
  #end
  def _blob_base
    token + File::extname(filename)
  end
  def token
    DateTime.now.year.to_s + DateTime.now.month.to_s + DateTime.now.day.to_s + DateTime.now.hour.to_s + DateTime.now.minute.to_s
  end
  def cred
    'cfa'
  end

  def get_container(name)
    container = WAZ::Blobs::Container.find(name)
    container = WAZ::Blobs::Container.create(name) if container.nil?
    container
  end

  protected
  def secure_token
    var = :"@secure_token"
    model.instance_variable_get(var) or model.instance_variable_set(var, SecureRandom.uuid)
  end
end
