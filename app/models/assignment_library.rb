class AssignmentLibrary < ActiveRecord::Base
  attr_accessible :name, :description
  has_many :assignments
  has_and_belongs_to_many :questions

end
