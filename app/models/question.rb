class Question < ActiveRecord::Base
  attr_accessible :body, :draft, :name, :user_id
  #store :body, accessors: [ :question,:points,:options ]
  serialize :body
  belongs_to :user
  has_and_belongs_to_many :assignment_libraries
  def self.select_question_type
    array = ["Multiple Choice", "Multiple Answer", "True/False","Matching","Sorting","Short Answer","Long Answer"]
    return array
  end

end
