class Assignment < ActiveRecord::Base
  attr_accessible :name, :description, :assignment_library_id, :grade_system_id,:user_ids
  belongs_to :assignment_library
  belongs_to :grade_system
  has_and_belongs_to_many :users
end
