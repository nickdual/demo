class HallOfFame < ActiveRecord::Base
  attr_accessible :amount, :description, :name, :owner, :reason
end
