class GradeSystem < ActiveRecord::Base
  attr_accessible :name, :settings
  def self.select_grading_systems
    self.all.map { |item| [item.name,item.id]}
  end
end
