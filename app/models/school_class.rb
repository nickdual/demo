class SchoolClass < ActiveRecord::Base
  attr_accessible :school_grade_id, :room_id, :institution_id, :teacher_id, :year
  belongs_to :institution
  has_many :users
  belongs_to :room
end
