class SponsorsController < ApplicationController
  # GET /sponsors
  # GET /sponsors.json
  before_filter :authenticate_user!
  def index
    authorize! :index, @sponsor, :message => 'Not authorized as an admin.'
    @sponsors = Sponsor.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sponsors }
    end
  end

  # GET /sponsors/1
  # GET /sponsors/1.json
  def show
    authorize! :show, @sponsor, :message => 'Not authorized as an admin.'
    @sponsor = Sponsor.find(params[:id])
    @comments = @sponsor.comment_threads.order('created_at desc')
    puts current_user
    puts 'a'
    puts @sponsor.to_json
    @new_comment = Comment.build_from(@sponsor, current_user.id, "")
  end

  # GET /sponsors/new
  # GET /sponsors/new.json
  def new
    @sponsor = Sponsor.new
    authorize! :new, @sponsor, :message => 'Not authorized as an admin.'
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sponsor }
    end
  end

  # GET /sponsors/1/edit
  def edit
    @sponsor = Sponsor.find(params[:id])
    authorize! :edit, @sponsor, :message => 'Not authorized as an admin.'
    @comments = @sponsor.comment_threads.order('created_at desc')
    @new_comment = Comment.build_from(@sponsor, current_user.id, "")
  end

  # POST /sponsors
  # POST /sponsors.json
  def create
    @sponsor = Sponsor.new(params[:sponsor])
    authorize! :create, @sponsor, :message => 'Not authorized as an admin.'
    respond_to do |format|
      if @sponsor.save
        format.html { redirect_to @sponsor, notice: 'Sponsor was successfully created.' }
        format.json { render json: @sponsor, status: :created, location: @sponsor }
      else
        puts @sponsor.errors.full_messages
        format.html { render action: "new" }
        format.json { render json: @sponsor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sponsors/1
  # PUT /sponsors/1.json
  def update
    @sponsor = Sponsor.find(params[:id])
    authorize! :update, @sponsor, :message => 'Not authorized as an admin.'
    @comments = @sponsor.comment_threads.order('created_at desc')
    @new_comment = Comment.build_from(@sponsor, current_user.id.to_i, "")
    respond_to do |format|
      if @sponsor.update_attributes(params[:sponsor])
        format.html { redirect_to @sponsor, notice: 'Sponsor was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @sponsor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sponsors/1
  # DELETE /sponsors/1.json
  def destroy
    @sponsor = Sponsor.find(params[:id])
    authorize! :destroy, @sponsor, :message => 'Not authorized as an admin.'
    @sponsor.destroy
    respond_to do |format|
      format.html { redirect_to sponsors_url }
      format.json { head :no_content }
    end
  end
end
