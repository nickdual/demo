class UsersController < ApplicationController
  before_filter :authenticate_user!
  layout 'home'
  def index
    @users = User.all
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
  end

  def show
    @user = User.find(params[:id])
  end
  
  def update
    authorize! :update, @user, :message => 'Not authorized as an administrator.'
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user], :as => :admin)
      redirect_to users_path, :notice => "User updated."
    else
      redirect_to users_path, :alert => "Unable to update user."
    end
  end
    
  def destroy
    authorize! :destroy, @user, :message => 'Not authorized as an administrator.'
    user = User.find(params[:id])
    unless user == current_user
      user.destroy
      redirect_to users_path, :notice => "User deleted."
    else
      redirect_to users_path, :notice => "Can't delete yourself."
    end
  end

  def preferences
    @user = current_user
  end

  def update_user
    @user = current_user
  end

  def add_emergency_contact
    @emergency_contact = current_user.additional_emergency_contacts.build(params[:additional_emergency_contact])
    if @emergency_contact.save!
      @user_emergency = User.invite!(:email => @emergency_contact.email)
      @user_emergency.add_role :parent
      @render = render_to_string  :partial => 'devise/shared/form_row_emergency_contact_table',:locals => {:emergency_contact =>  @emergency_contact }
      puts  @render
      puts @emergency_contact.to_json
      @render = @render.gsub!(/\n/, "")
      render :js =>  ' $("#new_emergency_contact").before(\''+ @render +'\');$("#new_emergency_contact").hide();'
    end
  end

  def update_emergency_contact
    @emergency_contacts = current_user.additional_emergency_contacts.find(params[:id])
    @emergency_contacts.update_attributes(params[:emergency_contacts])
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @emergency_contacts}
    end
  end

  def delete_emergency_contact
    @emergency_contacts = current_user.additional_emergency_contacts.find(params[:id])
    if @emergency_contacts.destroy
      @emergency_contact_list = current_user.additional_emergency_contacts
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @emergency_contact_list}
      end
    end
  end

  def add_race
    current_user.race_ids = params[:race_list]
    render json: :ok
  end

  def add_residence_id
    current_user.update_attribute('residence_id',params[:id])
    render json: :ok
  end

  def change_healths
    current_user.health_ids = params[:user][:health_ids]
    current_user.save
    render :js => '$("#health-options-' + current_user.id.to_s + '").modal("hide")'
  end

  def add_form_emergency_contact

    @user = User.new()
    @emergency_contact = @user.additional_emergency_contacts.build()
    render :partial => 'shared/form_more_emergency_contact',:locals => {:emergency_contact =>  @emergency_contact, :f =>  @user, :i => params[:i] }
  end

  def advanced_invitation
    @user_contact = User.new()
    @emergency_contact = AdditionalEmergencyContact.new()
    @race_list = Race.all
  end

  def create_advanced_invitation
    @user_contact = User.new({first: params[:user][:first], middle: params[:user][:middle],last: params[:user][:last],email:params[:user][:email],:password => 'abc123',race_ids:params[:user][:race_ids], health_ids:params[:user][:health_ids],residence_id:params[:user][:residence_id]} )
    if @user_contact.valid?
      @user_contact = User.invite!({first: params[:user][:first], middle: params[:user][:middle],last: params[:user][:last],email:params[:user][:email],:password => 'abc123',race_ids:params[:user][:race_ids], health_ids:params[:user][:health_ids],residence_id:params[:user][:residence_id]} )
      @user_contact.add_role :student
      emergency_contact_params =  params[:user][:additional_emergency_contact]
      emergency_contact_params.each do |ecp|
        @emergency_contact = @user_contact.additional_emergency_contacts.build(ecp.last)
        if @emergency_contact.save!
          @user_emergency = User.invite!(:email => @emergency_contact.email)
          @user_emergency.add_role :parent
        else
          puts(@emergency_contact.errors)
          respond_to do |format|
            format.html # index.html.erb
            format.js
          end

        end
      end
      respond_to do |format|
        format.html # index.html.erb
        format.js
      end
    else
      respond_to do |format|
        format.html # index.html.erb
        format.js
      end
    end
  end
end