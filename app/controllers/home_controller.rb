class HomeController < ApplicationController
  layout 'home'
  before_filter :authenticate_user!
  def index
    @users = User.all
  end
  def error

  end
  def city_autocomplete
    @query = "#{params[:term]}%".downcase
    @cities = City.where(["lower(name) LIKE ? and country_id = ? ", @query, params[:coutry_id]]).limit(10)
    @cities.collect! {|item| {:label => item.name, :id => item.id}}
    render :json => @cities
  end
end
