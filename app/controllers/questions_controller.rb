class QuestionsController < ApplicationController
  # GET /questions
  # GET /questions.json
  before_filter :authenticate_user!
  def index
    @questions = current_user.questions
    @questions.each  do |a|
    puts(a.body[:question])
    puts(a.body[:answer])
    puts(a.body)
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @questions }
    end
  end

  # GET /questions/1
  # GET /questions/1.json
  def show
    @question = Question.find(params[:id])
    authorize! :show, @question, :message => 'Not authorized as a teacher.'
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @question }
    end
  end

  # GET /questions/new
  # GET /questions/new.json
  def new
    @question = Question.new
    authorize! :new, @question, :message => 'Not authorized as a teacher.'
    @question.body = {}
    @question.body[:answer] = Array.new(5)
    @question.body[:correct] = Array.new()
    case params[:choice]
      when "multiple_choice"
        @question.body[:question_type] = "Multiple Choice"
      when "multiple_answer"
        @question.body[:question_type] = "Multiple Answer"
      when "matching"
        @question.body[:question_type] = "Matching"
      when "true/false"
        @question.body[:question_type] = "True/False"
      when "sorting"
        @question.body[:question_type] = "Sorting"
      when "short_answer"
        @question.body[:question_type] = "Short Answer"
        @question.body[:answer] = ""
      when "long_answer"
        @question.body[:question_type] = "Long Answer"
        @question.body[:answer] = ""
      else
        @question.body[:question_type] = "Multiple Choice"
    end
    respond_to do |format|
      format.html # new_bk.html.erb
      format.json { render json: @question }
    end
  end

  # GET /questions/1/edit
  def edit
    @question = Question.find(params[:id])
    authorize! :edit, @question, :message => 'Not authorized as a teacher.'
    respond_to do |format|
      format.html # new_bk.html.erb
      format.json { render json: @question }
    end
  end

  # POST /questions
  # POST /questions.json
  def create
    @question = Question.new()
    authorize! :create, @questions, :message => 'Not authorized as a teacher.'
   # @question.draft = params[:question][:draft]
    @question.name = params[:question][:name]
    @question.user_id = current_user.id
    @question.body = {}
    @question.body[:question_type] = params[:body][:question_type]
    if params[:body][:question_type] == "Multiple Choice"
      #history input
      @question.body[:answer] = params[:body][:answer]
      @question.body[:point] = params[:body][:point].to_i
      @question.body[:correct] = params[:body][:correct].to_i if params[:body][:correct].present?
      @question.body[:partial]= params[:body][:partial] if params[:body][:partial].present?
      #check question nil
      if params[:body][:question].blank?
        respond_to do |format|
          format.html {
            flash[:error] = "Question is  blank."
            render action: "new"}
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      @question.body[:question] = params[:body][:question]
      @question.body[:point] = params[:body][:point].to_i
      #
      if params[:body][:correct].present?
        @answer_correct = params[:body][:answer][params[:body][:correct].to_i]
      else
        unless params[:body][:partial].present?
          respond_to do |format|
            format.html {
              flash[:error] =  'Question  needs at least check correct.'
              render action: "new"}
            format.json { render json: @question.errors, status: :unprocessable_entity }
          end
          return
        end
      end
      select = []
      params[:body][:answer].each_index do |i|
          select << i unless params[:body][:answer][i].blank?
      end
      params[:body][:answer] = params[:body][:answer].reject(&:empty?)
      params[:body][:correct] = @question.body[:answer].index(@answer_correct)
      @N = params[:body][:answer].count.to_i  - 1
      # check number answer
      if @N < 1
        respond_to do |format|
            format.html {
              flash[:error] =  'Question  needs at least two choices.'
              render action: "new"}
            format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      @question.body[:answer] = params[:body][:answer]
      #check index correct
      @question.body[:index] = (0..@N).to_a
      #check Partial or extra credit
      if params[:body][:partial].present?
        @question.body[:partial] = params[:body][:partial]
        params[:points] = params[:points].values_at(*select)
        @question.body[:points] = params[:points].map{ |x| x.to_i }
      else
        if @question.body[:index].include?(params[:body][:correct].to_i)
          @question.body[:correct] = params[:body][:correct].to_i
        else
          @question.body[:correct] = 0
        end
      end
      @question.body[:choices_random_order] = params[:body][:choices_random_order]  if params[:body][:choices_random_order].present?
      @question.body[:questions_random_order] =  params[:body][:questions_random_order] if params[:body][:questions_random_order].present?
    end

    if params[:body][:question_type] == "Multiple Answer"
      #history input

      @question.body[:answer] = params[:body][:answer].collect { |a| a['text'] }
      @question.body[:point] = params[:body][:point].to_i
      @question.body[:correct] = params[:body][:answer].collect { |a| a['correct'].to_i unless a['correct'].nil?  }
      #check question nil
      if params[:body][:question].blank?
        puts "Question is  blank."
        respond_to do |format|
          format.html {
            flash[:error] = "Question is  blank."
            render action: "new" }
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      @question.body[:question] = params[:body][:question]
      unless @question.body[:correct].any?
        respond_to do |format|
          format.html {
            flash[:error] = "Question is not check correct."
            render action: "new"}
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      @question.body[:for_each_item] = params[:body][:for_each_item] if params[:body][:for_each_item].present?
      correct_index = []
      params[:body][:answer] = params[:body][:answer].reject { |a| a['text'].blank? }
      params[:body][:answer].each_index do |i|
        correct_index << i unless params[:body][:answer][i]['correct'].nil?
      end
      @question.body[:answer] =  params[:body][:answer].collect {|x| x['text'] }
      @question.body[:correct] = correct_index
      @N = @question.body[:answer].count.to_i  - 1
      if @N < 1
        respond_to do |format|
          format.html {
            flash[:error] =  'Question  needs at least check correct.'
            render action: "new"
            }
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      @question.body[:index] = (0..@N).to_a
      @question.body[:choices_random_order] = params[:body][:choices_random_order]  if params[:body][:choices_random_order].present?
      @question.body[:questions_random_order] =  params[:body][:questions_random_order] if params[:body][:questions_random_order].present?
    end
    if params[:body][:question_type] == "Matching"
      #history input
      @question.body[:stems] = params[:body][:answer].collect { |x| x['stems']}
      @question.body[:choice] = params[:body][:answer].collect { |x| x['choice']}
      @question.body[:point] = params[:body][:point].to_i
      @question.body[:answer] = params[:body][:answer]
      #check question nil
      if params[:body][:question].blank?
        puts "Question is  blank."
        respond_to do |format|
          format.html {
            flash[:error] = "Question is  blank."
            render action: "new"}
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      @question.body[:question] = params[:body][:question]
      params[:body][:answer] = params[:body][:answer].reject { |a| a['stems'].blank? and a['choice'].blank?  }
      @N = params[:body][:answer].count.to_i  - 1
      if @N < 1
        respond_to do |format|
          format.html {
            flash[:notice] = 'Question  needs at least two stems, choice.'
            render action: "new"}
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      params[:body][:answer].each_index do |i|
        if params[:body][:answer][i]['stems'].blank? or params[:body][:answer][i]['choice'].blank?
          puts "Stems not matching choice."
          respond_to do |format|
            format.html {
              flash[:error] = "Stems not matching choice."
              render action: "new"}
            format.json { render json: @question.errors, status: :unprocessable_entity }
          end
          return
        end
      end
      stems = []
      choice = []
      stems = params[:body][:answer].collect { |x| x['stems']}
      choice = params[:body][:answer].collect { |x| x['choice']}
      index = (1..9).to_a
      index_stems =  index.shuffle[0..@N]
      index_choice = index.shuffle[0..@N]
      @question.body[:stems] = []
      @question.body[:choice] = []
      index_stems.each_index do |i|
        @question.body[:stems] << {index_stems[i] => stems[i]}
      end
      index_choice.each_index do |i|
        @question.body[:choice] << {index_choice[i] => choice[i]}
      end
      index_correct = []
      index_stems.each_index do |i|
        index_correct << (index_stems[i].to_s + '-' + index_choice[i].to_s)
      end
      @question.body[:correct] = index_correct
      @question.body[:index] = (0..@N).to_a
      @question.body[:rand_stems] = params[:body][:stems_random_order] if params[:body][:stems_random_order].present?
      @question.body[:rand_choices] = params[:body][:choices_random_order] if params[:body][:choices_random_order].present?
      @question.body[:questions_random_order] = params[:body][:questions_random_order] if params[:body][:questions_random_order].present?
      @question.body.delete(:answer)
    end
    if params[:body][:question_type] == "Sorting"
      #history input
      @question.body[:answer] = params[:body][:answer]
      @question.body[:point] = params[:body][:point].to_i
      @question.body[:correct] = params[:body][:correct].to_i if params[:body][:correct].present?
      @question.body[:question] = params[:body][:question]
      #check question nil
      if params[:body][:question].blank?
        puts "Question is  blank."
        respond_to do |format|
          format.html {
            flash[:error] = "Question is  blank."
            render action: "new"
          }
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      params[:body][:answer] = params[:body][:answer].reject(&:empty?)
      if params[:body][:answer].count.to_i < 2
        respond_to do |format|
          format.html {
            flash[:notice] = 'Question  needs at least two stems'
            render action: "new"
          }
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      @N = params[:body][:answer].count.to_i  - 1
      answers = params[:body][:answer]
      @question.body[:answer] = []
      index = (1..9).to_a
      index_answer =  index.shuffle[0..@N]
      index_answer.each_index do |i|
        @question.body[:answer] << {index_answer[i] => answers[i]}
      end
      @question.body[:correct] =  index_answer.join('-')
      @question.body[:questions_random_order] = params[:body][:questions_random_order] if params[:body][:questions_random_order].present?
    end
    if params[:body][:question_type] == "True/False"
      #history input
      @question.body[:answer] = params[:body][:answer]
      @question.body[:point] = params[:body][:point].to_i
      #check question nil
      if params[:body][:question].blank?
        puts "Question is  blank."
        respond_to do |format|
          format.html {
            flash[:error] = "Question is  blank."
            render action: "new"  }
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      @question.body[:question] = params[:body][:question]
      @question.body[:point] = params[:body][:point].to_i
      @question.body[:answer]  =  params[:body][:answer] if params[:body][:answer].present?
      @question.body[:questions_random_order] = params[:body][:questions_random_order] if params[:body][:questions_random_order].present?
    end

    if params[:body][:question_type] == "Short Answer"
      #history input
      @question.body[:answer] = params[:body][:answer]
      @question.body[:point] = params[:body][:point].to_i
      @question.body[:answer]  =  params[:body][:answer_correct] if params[:body][:answer_correct].present?
      #check question nil
      if params[:body][:question].blank?
        puts "Question is  blank."
        respond_to do |format|
          format.html {
            flash[:error] = "Question is  blank."
            render action: "new"}
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      @question.body[:question] = params[:body][:question]
      if params[:body][:answer_correct].blank?
        puts "Answer is not blank."
        respond_to do |format|
          format.html {
            flash[:error] = "Answer is not blank."
            render action: "new"}
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      @question.body[:questions_random_order] = params[:body][:questions_random_order] if params[:body][:questions_random_order].present?
    end
    if params[:body][:question_type] == "Long Answer"
      #history input
      @question.body[:question] = params[:body][:question]
      @question.body[:point] = params[:body][:point].to_i
      @question.body[:answer]  =  params[:body][:answer_correct] if params[:body][:answer_correct].present?
      #check question nil
      if params[:body][:question].blank?
        puts "Question is  blank."
        respond_to do |format|
          format.html {
            flash[:error] = "Question is  blank."
            render action: "new"
          }
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      if params[:body][:answer_correct].blank?
        puts "Answer is not blank."
        respond_to do |format|
          format.html {
            flash[:error] = "Answer is not blank."
            render action: "new"}
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      @question.body[:questions_random_order] = params[:body][:questions_random_order] if params[:body][:questions_random_order].present?
    end
    respond_to do |format|
      if @question.save
        format.html { redirect_to @question, notice: 'Question was successfully created.' }
        format.json { render json: @question, status: :created, location: @question }
      else
        format.html { render action: "new" }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /questions/1
  # PUT /questions/1.json
  def update
    @question = Question.find(params[:id])
    authorize! :update, @question, :message => 'Not authorized as a teacher.'
    @question.name = params[:question][:name]
    #history input
    if @question.body[:question_type] == "Multiple Choice"
      @question.body[:answer] = params[:body][:answer]
      @question.body[:point] = params[:body][:point].to_i
      @question.body[:correct] = params[:body][:correct].to_i if params[:body][:correct].present?
      @question.body[:partial]= params[:body][:partial] if params[:body][:partial].present?
      @question.body[:question] = params[:body][:question]
      #check question nil
      if params[:body][:question] .blank?
        puts 'Question is blank.'
        respond_to do |format|
          format.html {
            flash[:error] = "There was a problem when trying to save, Question is  blank."
            render action: "edit"
          }
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      #
      if params[:body][:correct].present?
        @answer_correct = params[:body][:answer][params[:body][:correct].to_i]
      end
      puts @answer_correct
      select = []
      if params[:body][:answer].present?
        params[:body][:answer].each_index do |i|
          select << i unless params[:body][:answer][i].blank?
        end
      end
      params[:body][:answer] = params[:body][:answer].reject(&:empty?) if params[:body][:answer].present?
      params[:body][:correct] = @question.body[:answer].index(@answer_correct)
      @N = params[:body][:answer].count.to_i  - 1
      # check number answer
      if @N < 1
        puts 'Question  needs at least two choices '
        respond_to do |format|
          format.html {
            flash[:error] = "There was a problem when trying to save, Question  needs at least two choices "
            render action: "edit"
          }
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      @question.body[:answer] = params[:body][:answer]
      #check index correct
      @question.body[:index] = (0..@N).to_a

      #check Partial or extra credit
      if params[:body][:partial].present?
        @question.body[:partial] = params[:body][:partial]
        params[:points] = params[:points].values_at(*select)
        @question.body[:points] = params[:points].map{ |x| x.to_i }
      else
        if @question.body[:index].include?(params[:body][:correct].to_i)
          @question.body[:correct] = params[:body][:correct].to_i
        else
          @question.body[:correct] = 0
        end
      end
      @question.body[:choices_random_order] = params[:body][:choices_random_order]  if params[:body][:choices_random_order].present?
      @question.body[:questions_random_order] =  params[:body][:questions_random_order] if params[:body][:questions_random_order].present?
    end

    if @question.body[:question_type] == "Multiple Answer"
      #history input
      @question.body[:question] = params[:body][:question]
      @question.body[:answer] = params[:body][:answer].collect { |a| a['text'] }
      @question.body[:point] = params[:body][:point].to_i
      @question.body[:correct] = params[:body][:answer].collect { |a| a['correct'].to_i unless a['correct'].nil?  }
      @question.body[:for_each_item] = params[:body][:for_each_item] if params[:body][:for_each_item].present?
      #check question nil
      if params[:body][:question].blank?
        puts "Question is  blank."
        respond_to do |format|
          format.html {
            flash[:error] = "Question is  blank."
            render action: "new"
          }
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      unless @question.body[:correct].any?
        puts "Question is not check correct."
        respond_to do |format|
          format.html {
            flash[:error] = "Question is not check correct."
            render action: "new"
          }
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      correct_index = []
      params[:body][:answer] = params[:body][:answer].reject { |a| a['text']== "" }
      params[:body][:answer].each_index do |i|
        correct_index << i unless params[:body][:answer][i]['correct'].nil?
      end
      @N = params[:body][:answer].count.to_i  - 1
      if @N < 1
        puts 'Question  needs at least two choices.'
        respond_to do |format|
          format.html {
            flash[:error] = 'Question  needs at least two choices.'
            render action: "new"
          }
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end

      @question.body[:answer] =  params[:body][:answer].collect {|x| x['text'] }
      @question.body[:correct] = correct_index
      @question.body[:index] = (0..@N).to_a
      @question.body[:choices_random_order] = params[:body][:choices_random_order]  if params[:body][:choices_random_order].present?
      @question.body[:questions_random_order] =  params[:body][:questions_random_order] if params[:body][:questions_random_order].present?

    end

    if @question.body[:question_type] == "Matching"
      @question.body[:stems]  = []
      stems = params[:body][:answer].collect { |x| x['stems']}
      stems_index = params[:body][:answer].collect { |x| x['stems_index']}
      stems.each_index do |i|
        @question.body[:stems] << {stems_index[i] => stems[i]}
      end
      @question.body[:choice] = []
      choice = params[:body][:answer].collect { |x| x['choice']}
      choice_index = params[:body][:answer].collect { |x| x['choice_index']}
      choice.each_index do |i|
        @question.body[:choice] << {choice_index[i] => choice[i]}
      end

      @question.body[:point] = params[:body][:point].to_i
      @question.body[:answer] = params[:body][:answer]
      @question.body[:question] = params[:body][:question]
      #check question nil
      if params[:body][:question].blank?
        puts "Question is  blank."
        respond_to do |format|
          format.html {
            flash[:error] = "Question is  blank."
            render action: "new"}
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      params[:body][:answer] = params[:body][:answer].reject { |a| a['stems']== "" and a['choice']==""  }
      #check params[:body][:answer]
      @N = params[:body][:answer].count.to_i  - 1
      puts params[:body][:answer]
      puts @N
      if @N < 1
        respond_to do |format|
          format.html {
            flash[:notice] = 'Question  needs at least two stems, choice.'
            render action: "new"}
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      params[:body][:answer].each_index do |i|
        if params[:body][:answer][i]['stems'].blank? or params[:body][:answer][i]['choice']==""
          puts "Stems not matching choice."
          respond_to do |format|
            format.html {
              flash[:error] = "Stems not matching choice."
              render action: "new"}
            format.json { render json: @question.errors, status: :unprocessable_entity }
          end
          return
        end
      end
      stems = []
      choice = []
      stems = params[:body][:answer].collect { |x| x['stems']}
      choice = params[:body][:answer].collect { |x| x['choice']}
      index_stems =  params[:body][:answer].collect { |x| x['stems_index'] }
      index_choice = params[:body][:answer].collect { |x| x['choice_index'] }
      index_stems_keys = (0..9).to_a - @question.body[:correct].map {|x| x.split('-')[0].to_i}
      index_stems_keys = index_stems_keys.shuffle
      index_choice_keys = (0..9).to_a - @question.body[:correct].map {|x| x.split('-')[1].to_i}
      index_choice_keys = index_choice_keys.shuffle
      index_stems.each_index do |i|
        if index_stems[i].blank?
          index_stems[i] = index_stems_keys.first
          index_stems_keys.delete(index_stems_keys.first)
        end
      end
      index_choice.each_index do |i|
        if index_choice[i].blank?
          index_choice[i] = index_choice_keys.first
          index_choice_keys.delete(index_choice_keys.first)
        end
      end

      @question.body[:stems] = []
      @question.body[:choice] = []
      index_stems.each_index do |i|
        @question.body[:stems] << {index_stems[i] => stems[i]}
      end
      index_choice.each_index do |i|
        @question.body[:choice] << {index_choice[i] => choice[i]}
      end
      index_correct = []
      index_stems.each_index do |i|
        index_correct << (index_stems[i].to_s + '-' + index_choice[i].to_s)
      end
      @question.body[:correct] = index_correct
      @question.body[:index] = (0..@N).to_a
      @question.body.delete(:answer)
      @question.body[:rand_stems] = params[:body][:stems_random_order] if params[:body][:stems_random_order].present?
      @question.body[:rand_choices] = params[:body][:choices_random_order] if params[:body][:choices_random_order].present?
      @question.body[:questions_random_order] = params[:body][:questions_random_order] if params[:body][:questions_random_order].present?
    end

    if @question.body[:question_type] == "Sorting"
      #history input
      @question.body[:answer] = []
      params[:body][:answer].each do |ans|
        @question.body[:answer] << { ans['index'].to_i => ans['text']}
      end
      @question.body[:point] = params[:body][:point].to_i
      @question.body[:question] = params[:body][:question]
      #check question nil
      if params[:body][:question].blank?
        puts "Question is  blank."
        respond_to do |format|
          format.html {
            flash[:error] = "Question is  blank."
            render action: "new"}
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      params[:body][:answer] = params[:body][:answer].reject { |a| a['text'].blank?  }
      answer = params[:body][:answer].collect {|x| x['text']}
      @index = params[:body][:answer].collect {|x| x['index']}
      answer = answer.reject(&:empty?)
      # answer_index_nil =  [{"text"=>"E ", "index"=>""}, {"text"=>"F ", "index"=>""}]
      answer_index_nil = params[:body][:answer].select {|x| not x['text'].blank? and x['index'].blank?}
      keys = (0..9).to_a - @question.body[:correct].split('-').map {|x| x.to_i}
      keys = keys.shuffle
      if answer.count.to_i < 2
        respond_to do |format|
          format.html {
            flash[:notice] = 'Question  needs at least two stems'
            render action: "new"}
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      @index.each_index do |i|
        if @index[i].blank?
          @index[i] = keys.first
          keys.delete(keys.first)
        end
      end
      @question.body[:answer]  =  []
      answer.each_with_index do |ans,i|
        @question.body[:answer] << { @index[i] => ans}
      end
      @question.body[:correct] =  @index.join('-')
      @question.body[:questions_random_order] = params[:body][:questions_random_order] if params[:body][:questions_random_order].present?
    end
    if @question.body[:question_type] == "True/False"
      #history input
      @question.body[:question] = params[:body][:question]
      if params[:body][:question].blank?
        puts 'Question is blank.'
        respond_to do |format|
          format.html {
            flash[:error] = "There was a problem when trying to save, Question is  blank."
            render action: "edit"}
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      @question.body[:point] = params[:body][:point].to_i
      @question.body[:answer]  =  params[:body][:answer] if params[:body][:answer].present?
      @question.body[:questions_random_order] = params[:body][:questions_random_order] if params[:body][:questions_random_order].present?
    end
    if @question.body[:question_type] == "Short Answer"
      #history input
      @question.body[:question] = params[:body][:question]
      if params[:body][:question].blank?
        puts 'Question is blank.'
        respond_to do |format|
          format.html {
            flash[:error] = "There was a problem when trying to save, Question is  blank."
            render action: "edit"}
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      if params[:body][:answer_correct].blank?
        puts "Answer is not blank."
        respond_to do |format|
          format.html {
            flash[:error] = "Answer is not blank."
            render action: "new"}
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      @question.body[:point] = params[:body][:point].to_i
      @question.body[:answer]  =  params[:body][:answer_correct] if params[:body][:answer_correct].present?
      @question.body[:questions_random_order] = params[:body][:questions_random_order] if params[:body][:questions_random_order].present?
    end
    if @question.body[:question_type] == "Long Answer"
      #history input
      @question.body[:question] = params[:body][:question]
      if params[:body][:question].blank?
        puts 'Question is blank.'
        respond_to do |format|
          format.html {
            flash[:error] = "There was a problem when trying to save, Question is  blank."
            render action: "edit"}
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      if params[:body][:answer_correct].blank?
        puts "Answer is not blank."
        respond_to do |format|
          format.html {
            flash[:error] = "Answer is not blank."
            render action: "new"}
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
        return
      end
      @question.body[:point] = params[:body][:point].to_i
      @question.body[:answer]  =  params[:body][:answer_correct] if params[:body][:answer_correct].present?
      @question.body[:questions_random_order] = params[:body][:questions_random_order] if params[:body][:questions_random_order].present?
    end
    respond_to do |format|
      if @question.save
        format.html { redirect_to @question , notice: 'Question was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /questions/1
  # DELETE /questions/1.json
  def destroy
    @question = Question.find(params[:id])
    authorize! :destroy, @question, :message => 'Not authorized as a teacher.'
    @question.destroy
    respond_to do |format|
      format.html { redirect_to questions_url }
      format.json { head :no_content }
    end
  end

  private
  def covert_multiple_choice(params)
    str =  []
    str << params[:question][:body][:question]
    str << params[:question][:body][:point]
    params[:question][:body][:options][:answer].each do |answer|
      str << answer
    end
    params[:question][:body] = str.to_s
    params
  end
end
