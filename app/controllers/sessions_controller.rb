class SessionsController < Devise::SessionsController
 layout 'home'
 def destroy
  super
 end

 def create
   resource = warden.authenticate!(auth_options)

   puts '*******************'

   set_flash_message(:notice, :signed_in) if is_navigational_format?
   sign_in(resource_name, resource)
   session[:username] = current_user.name
   respond_with resource, :location => after_sign_in_path_for(resource)
 end
end



