require "nokogiri"
class ContentsController < ApplicationController
  # GET /contents
  # GET /contents.json
  before_filter :authenticate_user!
  def index
    if params[:query].present?
      @contents = Content.search params
    else
      if params[:tag].present?
        @contents = Content.tagged_with(params[:tag])
      else
        @contents = current_user.contents
      end
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @contents }
    end

  end

  # GET /contents/1
  # GET /contents/1.json
  def show
    @content = Content.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @content }
    end
  end

  # GET /contents/new
  # GET /contents/new.json
  def new
    @content = Content.new
    authorize! :new, @content, :message => 'Not authorized as a teacher.'
    @pictures = ContentPhoto.paginate(:per_page => 10, :page => params[:page])
    respond_to do |format|
      format.html # new_bk.html.erb
      format.json { render json: @content }
      format.js  # new_bk.js.erb
    end
  end

  # GET /contents/1/edit
  def edit
    @content = Content.find(params[:id])
    authorize! :edit, @content, :message => 'Not authorized as a teacher.'
    @files = Dir.glob("app/assets/images/content/*")
    @directories = []
    @files.each do |file|
      @directories << File.basename(file)
    end
  end

  # POST /contents
  # POST /contents.json
  def create
    @content = Content.new(params[:content])
    authorize! :create, @content, :message => 'Not authorized as a teacher.'
    #doc = Nokogiri::HTML.fragment(params[:content][:body])
    #doc.css('img').each do |image|
    #  #Sputs image['src']
    #  @query = image['src'].split('/').last
    #  puts @query
    #  image['src'] = ContentPhoto.find_by_image(@query).image.url
    #end

    @content.user_id = current_user.id if current_user
    @content.tag_list = JSON.parse params[:content][:tag_list]
    respond_to do |format|
      if @content.save
        format.html { redirect_to @content, notice: 'Content was successfully created.' }
        format.json { render json: @content, status: :created, location: @content }
      else
        @pictures = ContentPhoto.paginate(:per_page => 10, :page => params[:page])
        format.html { render action: "new",:object => @pictures }
        format.json { render json: @content.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /contents/1
  # PUT /contents/1.json
  def update
    authorize! :update, @content, :message => 'Not authorized as a teacher.'
    @content = Content.find(params[:id])
    @content.name = params[:content][:name]
    @content.body = params[:content][:body]
    @content.draft = params[:content][:draft]
    @content.tag_list = JSON.parse params[:content][:tag_list]
    puts params[:content][:tag_list]
    respond_to do |format|
      if @content.save
        format.html { redirect_to @content, notice: 'Content was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @content.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contents/1
  # DELETE /contents/1.json
  def destroy
    authorize! :destroy, @content, :message => 'Not authorized as a teacher.'
    @content = Content.find(params[:id])
    @content.destroy

    respond_to do |format|
      format.html { redirect_to contents_url }
      format.json { head :no_content }
    end
  end

  def tag
    @content_tags = Content.tag_counts_on(:tags)
    @contents = Content.tagged_with(params[:tag])
    respond_to do |format|
      format.html
      format.json { head :no_content }
    end
  end
  def load_tags
    @content = Content.find(params[:id])
    @content_tags = @content.tag_list

    respond_to do |format|
      format.html
      format.json {render json: @content_tags }
    end
  end
  def remove_tag
    @content_tags = Content.tag_counts_on(:tags)
    @content_tags.find(params[:tag_id]).destroy
    redirect_to '/contents/tags'
  end
  def edit_tag_name
    @content_tags = Content.tag_counts_on(:tags)
    @tag_edit = @content_tags.find(params[:tag_id])
    @tag_edit.name = params[:tag_name_edit]
    if @tag_edit.save
      render json: @tag_edit
    end
  end

  def search_content
    if params[:query].present?
      @contents = Content.search(params[:query],load: true)
    else
      @contents = Content.search(params)
    end
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @contents }
    end
  end


end

