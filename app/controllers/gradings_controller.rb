class GradingsController < ApplicationController
  before_filter :authenticate_user!
  def index
    if current_user.has_role? :student
      @gradings = current_user.gradings
    end
    if current_user.has_role? :teacher
      @gradings = Grading.all
    end
    if current_user.has_role? :parent
      user_id =   AdditionalEmergencyContact.find_by_email(current_user.email).user_id
      @gradings = Grading.find(:all, :conditions => {:user_id => user_id})
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @gradings }
    end
  end

  def edit
  end


  def show
    @question_list = []
    @grading =  Grading.find(params[:id])
    @grading.point = 0;
    @grading_details = @grading.grading_details
    @questions = @grading.assignment.assignment_library.questions
    @questions.each  do |question|
      @grading_detail =  @grading_details.where('grading_id = ? AND question_id = ? ',params[:id],question.id).first
      @grading.point += @grading_detail.point.to_f
      @question_list.push([question ,@grading_detail.id, @grading_detail.body, @grading_detail.point])
    end
    @grading.save
    @comments = @grading.comment_threads.order('created_at desc')
    @new_comment = Comment.build_from(@grading, current_user.id, "")
    @score = ''
    if @grading.grade_system_id.present?
      @grade_system = GradeSystem.find(@grading.grade_system_id)
      @score = @grade_system.present? ? @grade_system.name: ''
    end

  end
  def new
  end

  def answers
    @grading_detail = GradingDetail.find(params[:id])
    @question = Question.find(@grading_detail.question_id)
    if params[:body][:correct].present?
      @grading_detail.body = {}
      if params[:body][:correct].kind_of?(Array)
        @grading_detail.body[:correct] = params[:body][:correct].collect {|x| x.to_i}
      else
        @grading_detail.body[:correct] = params[:body][:correct].to_i
      end
    end
    if params[:body][:points].present?
      @grading_detail.body = {}
      @grading_detail.body[:correct] = params[:body][:points].to_i
    end
    if params[:body][:answer].present?
      @grading_detail.body = {}
      @grading_detail.body[:answer] = params[:body][:answer]
    end
    if params[:body][:answer_text].present?
      @grading_detail.body = {}
      @grading_detail.body[:answer] = params[:body][:answer_text]
    end
    # Matching
    if params[:body][:stems].present? and  params[:body][:choice].present?
      @grading_detail.body = {}
      @grading_detail.body[:stems] = params[:body][:stems]
      params[:body][:choice] = params[:body][:choice].split('-/-').map {|x| x.to_s}
      @grading_detail.body[:choice] = params[:body][:choice]
      @grading_detail.body[:correct] = []
      params[:body][:stems].each_index do |i|
        @grading_detail.body[:correct] << (params[:body][:stems][i].to_s + '-' + params[:body][:choice][i].to_s)
      end
    end
    #sorting
    if params[:body][:answer].present?  and params[:body][:choice].present?
      @grading_detail.body = {}
      params[:body][:correct] = params[:body][:choice].split('-/-')
      @grading_detail.body[:answer] = params[:body][:answer]
      @grading_detail.body[:correct] = params[:body][:correct].join('-')
    end
    if @grading_detail.save
        if @question.body[:question_type] == 'Multiple Choice'
          if(@question.body[:partial].nil?)
            if @grading_detail.body[:correct] == @question.body[:correct]
              @grading_detail.update_attribute("point",@question.body[:point])
            else
              @grading_detail.update_attribute("point",'0')
            end
          else
            @grading_detail.update_attribute("point",params[:body][:points].to_i)
          end
        end
        if @question.body[:question_type] == 'Multiple Answer'
          if(@question.body[:for_each_item].present?)
            i = 0; j = 0;
            @grading_detail.body[:correct].each do |correct|
               correct.in?(@question.body[:correct]) ? i += @question.body[:point] :j += 1
            end
            j == 0 ? @grading_detail.update_attribute("point",i) : @grading_detail.update_attribute("point",'')
          else
            arr = @grading_detail.body[:correct] - @question.body[:correct]
            arr.empty? ? @grading_detail.update_attribute("point",@question.body[:point]):@grading_detail.update_attribute("point",0)
          end
        end
       if @question.body[:question_type] == 'True/False'
         if @grading_detail.body[:answer] == @question.body[:answer]
           @grading_detail.update_attribute("point",@question.body[:point])
         else
           @grading_detail.update_attribute("point",0)
         end
       end
       if @question.body[:question_type] == 'Matching'
         size = @grading_detail.body[:correct].count.to_i
         arr = @grading_detail.body[:correct] - @question.body[:correct]
           if arr.empty?
             @grading_detail.update_attribute("point",size * @question.body[:point].to_i)
           else
             @grading_detail.update_attribute("point",(size- arr.count.to_i) * @question.body[:point].to_i)
           end
       end
       if @question.body[:question_type] == 'Sorting'
         arr = @grading_detail.body[:correct].to_s.casecmp  @question.body[:correct].to_s
           if arr == 0
             @grading_detail.update_attribute("point", @question.body[:point].to_i)
           else
             @grading_detail.update_attribute("point",0)
           end
       end
      render :js => 'alert("Save success")'
    else
      render :js => 'alert("Error")'
    end
  end

  def add_grade_system
    @grading = Grading.find(params[:id])
    if @grading.update_attribute("grade_system_id", params[:grade_system_id])
      render json: :ok
    end
    end
  def add_point_grading_detail
    @grading_detail = GradingDetail.find(params[:id])
    if @grading_detail.update_attribute("point", params[:point])
      render json:  @grading_detail.point
    end
    end
  def add_point_grading
    @grading = Grading.find(params[:id])
    if @grading.update_attribute("point", params[:point])
      render json: :ok
    end
  end

end
