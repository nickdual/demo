class RegistrationsController < Devise::RegistrationsController
  layout 'home'
  def create
    @user = User.new(params[:user])
    if @user.save
      if params[:role].to_s == "Teacher"
        @user.add_role :teacher
      else
        @user.add_role :student
      end

      flash[:notice] = t("users.confirmationsent")
      sign_in(@user)
      respond_to do |format|
        format.html {redirect_to root_url}
        format.json { render json: @user }
      end
    else
      respond_to do |format|
        format.html {render :action => :new}
        format.json { render json: @user.errors }
      end
    end
  end

  def edit
    @additional_emergency_contacts_list = current_user.additional_emergency_contacts
    puts @additional_emergency_contacts_list.to_json
    @races_ids = current_user.race_ids
    @race_list = Race.all
    @residence = Residence.all
    @emergency_contact = AdditionalEmergencyContact.new()

  end

  def update
    super
  end

  def destroy
    super
  end

  def cancel
    super
  end



end
