class AssignmentLibrariesController < ApplicationController
  # GET /addresses
  # GET /addresses.json
  before_filter :authenticate_user!
  def index
    @assignment_libraries = AssignmentLibrary.all
    authorize! :index, @assignment_libraries, :message => 'Not authorized as a teacher.'
    respond_to do |format|
      format.html # index.html.erb
      format. json { render json: @assignment_libraries }
    end
  end


  def show
    @assignment_library = AssignmentLibrary.find(params[:id])
    @questions = @assignment_library.questions
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @assignment_library }
    end
  end

  def new
    authorize! :new, @assignment_libraries, :message => 'Not authorized as a teacher.'
    @assignment_library = AssignmentLibrary.new
  end

  def edit
    @assignment_library = AssignmentLibrary.find(params[:id])
    authorize! :edit, @assignment_library, :message => 'Not authorized as a teacher.'
    @questions_list = @assignment_library.questions
    if @assignment_library.question_ids.length() != 0
      @query = Question.where(['id NOT IN (?)', @assignment_library.question_ids])
      @questions = @query.paginate(:per_page => 10, :page => params[:page])
    else
      @questions = Question.paginate(:per_page => 10, :page => params[:page])
    end

    respond_to do |format|
      format.html # new_bk.html.erb
      format.json { render json: @questions }
      format.js
      end
  end
  def create
    @assignment_library = AssignmentLibrary.new(params[:assignment_library])
    authorize! :create, @assignment_library, :message => 'Not authorized as a teacher.'
    respond_to do |format|
      if @assignment_library.save
        format.html { redirect_to edit_assignment_library_path(@assignment_library), notice: 'Assignment library was successfully created.' }
        format.json { render json: @assignment_library, status: :created, location: @assignment_library }
      else
        format.html { render action: "new" }
        format.json { render json: @assignment_library.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @assignment_library = AssignmentLibrary.find(params[:id])
    authorize! :update, @assignment_library, :message => 'Not authorized as a teacher.'
    respond_to do |format|
      if @assignment_library.update_attributes(params[:assignment_library])
        format.html { redirect_to @assignment_library, notice: 'Assignment Library was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @assignment_library.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @assignment_library = AssignmentLibrary.find(params[:id])
    authorize! :destroy, @assignment_library, :message => 'Not authorized as a teacher.'
    @assignment_library.destroy
    respond_to do |format|
      format.html { redirect_to assignment_libraries_url }
      format.json { head :no_content }
    end
  end

  def question_search
    @assignment_library = AssignmentLibrary.find(params[:id])
    @list_questions = @assignment_library.question_ids
    @key_search ="%#{params[:key_question].downcase}%"
    @questions = Question.where("(name like ? OR body like ?) AND id NOT IN (?)", @key_search, @key_search,@list_questions).paginate(:per_page => 10, :page => params[:page])
    if @questions.length != 0
      @render = render_to_string  :partial => 'shared/form_row_question_table',:locals => {:questions =>  @questions }
      @render = @render.gsub!(/\n/, "")
      respond_to do |format|
        format.html {render :text =>  @render}
        format.json { render json: @questions }
        format.js
      end
    else
      render :text => :no_result
    end
  end

  def add_question
    @list =   params[:list_question_ids]
    @assignment_library = AssignmentLibrary.find(params[:id])
    @assignment = @assignment_library.assignments
    @list.each do |l|
      @assignment_library.questions << Question.find(l)
      @assignment.each do |assignment|
        puts(assignment.to_yaml)
        @user_list = assignment.user_ids
        @user_list.each do |user_id|
          @grading_list = Grading.where('assignment_id = ? AND user_id = ?',assignment.id, user_id)
          @grading_list.each do |grading|
            @grading_detail = GradingDetail.new({:grading_id => grading.id , :question_id => l })
            @grading_detail.save
          end
        end
      end
    end
    @questions = @assignment_library.questions
    @render = render_to_string  :partial => 'shared/form_row_question_table',:locals => {:questions =>  @questions }
    @render = @render.gsub!(/\n/, "")
    respond_to do |format|
      format.html {render :text =>  @render}
    end
  end

  def destroy_question
    @assignment_library = AssignmentLibrary.find(params[:id])
    @question_list_remove = params[:list]
    @question_list_remove.each do |s_list|
      @question = @assignment_library.questions.find(s_list)
      @assignment_library.questions.delete(@question)
    end
    render json: :ok
  end

end
