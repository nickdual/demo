class PhonetypesController < ApplicationController
  # GET /phonetypes
  # GET /phonetypes.json
  before_filter :authenticate_user!
  layout 'home'
  def index
    @phonetypes = Phonetype.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @phonetypes }
    end
  end

  # GET /phonetypes/1
  # GET /phonetypes/1.json
  def show
    @phonetype = Phonetype.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @phonetype }
    end
  end

  # GET /phonetypes/new
  # GET /phonetypes/new.json
  def new
    @phonetype = Phonetype.new

    respond_to do |format|
      format.html # new_bk.html.erb
      format.json { render json: @phonetype }
    end
  end

  # GET /phonetypes/1/edit
  def edit
    @phonetype = Phonetype.find(params[:id])
  end

  # POST /phonetypes
  # POST /phonetypes.json
  def create
    @phonetype = Phonetype.new(params[:phonetype])

    respond_to do |format|
      if @phonetype.save
        format.html { redirect_to @phonetype, notice: 'Phonetype was successfully created.' }
        format.json { render json: @phonetype, status: :created, location: @phonetype }
      else
        format.html { render action: "new" }
        format.json { render json: @phonetype.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /phonetypes/1
  # PUT /phonetypes/1.json
  def update
    @phonetype = Phonetype.find(params[:id])

    respond_to do |format|
      if @phonetype.update_attributes(params[:phonetype])
        format.html { redirect_to @phonetype, notice: 'Phonetype was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @phonetype.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /phonetypes/1
  # DELETE /phonetypes/1.json
  def destroy
    @phonetype = Phonetype.find(params[:id])
    @phonetype.destroy

    respond_to do |format|
      format.html { redirect_to phonetypes_url }
      format.json { head :no_content }
    end
  end
end
