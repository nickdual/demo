class CommentsController < ApplicationController
  # GET /comments
  # GET /comments.json
  before_filter :authenticate_user!
  def index
    @comments = Comment.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @comments }
    end
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
    @comment = Comment.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @comment }
    end
  end

  # GET /comments/new
  # GET /comments/new.json
  def new
    @comment = Comment.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @comment }
    end
  end

  # GET /comments/1/edit
  def edit
    @comment = Comment.find(params[:id])
  end

  # POST /comments
  # POST /comments.json
  def create
    @comment_hash = params[:comment]
    @Object_type =  params[:comment][:commentable_type]
    if @Object_type == 'Sponsor'
      @comment = Comment.build_from(Sponsor.find(params[:comment][:commentable_id].to_i), current_user.id, @comment_hash[:body])
      @comments = Sponsor.find(params[:comment][:commentable_id].to_i).comment_threads.order('created_at desc')
    elsif @Object_type == 'Grading'
      @comment = Comment.build_from(Grading.find(params[:comment][:commentable_id].to_i), current_user.id, @comment_hash[:body])
      @comments = Grading.find(params[:comment][:commentable_id].to_i).comment_threads.order('created_at desc')
    end
    if @comment.save
      puts 'ddd'
      render :partial => "comments/comment", :locals => { :comment => @comment, :comments => @comments, :organization => @organization }, :layout => false, :status => :created
    else
      puts 'aaa'
      render :js => "alert('error saving comment');"
    end
  end

  # PUT /comments/1
  # PUT /comments/1.json
  def update
    @comment = Comment.find(params[:id])

    respond_to do |format|
      if @comment.update_attributes(params[:comment])
        format.html { redirect_to @comment, notice: 'Comment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment = Comment.find(params[:id])
    if @comment.destroy
      render :json => @comment, :status => :ok
    else
      render :js => "alert('error deleting comment');"
    end
  end
end
