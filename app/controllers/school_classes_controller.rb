class SchoolClassesController < ApplicationController
  before_filter :authenticate_user!, :except => [:add_student, :remove_student]
  # GET /school_classes
  # GET /school_classes.json
  def index
    @school_classes = SchoolClass.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @school_classes }
    end
  end

  # GET /school_classes/1
  # GET /school_classes/1.json
  def show
    @school_class = SchoolClass.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @school_class }
    end
  end

  # GET /school_classes/new
  # GET /school_classes/new.json
  def new
    @school_class = SchoolClass.new
    @school_grade = SchoolGrade.all
    @room = Room.all
    @current_user = current_user
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @school_class }
    end
  end

  # GET /school_classes/1/edit
  def edit
    @school_class = SchoolClass.find(params[:id])
    @current_user = current_user
    @student_list_already = @school_class.users
  end

  # POST /school_classes
  # POST /school_classes.json
  def create
    @school_class = SchoolClass.new(params[:school_class])

    respond_to do |format|
      if @school_class.save
        format.html { redirect_to edit_school_class_path(@school_class.id), notice: 'School class was successfully created.' }
        format.json { render json: @school_class, status: :created, location: @school_class }
      else
        format.html { render action: "new" }
        format.json { render json: @school_class.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /school_classes/1
  # PUT /school_classes/1.json
  def update
    @school_class = SchoolClass.find(params[:id])

    respond_to do |format|
      if @school_class.update_attributes(params[:school_class])
        format.html { redirect_to @school_class, notice: 'School class was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @school_class.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /school_classes/1
  # DELETE /school_classes/1.json
  def destroy
    @school_class = SchoolClass.find(params[:id])
    @school_class.destroy

    respond_to do |format|
      format.html { redirect_to school_classes_url }
      format.json { head :no_content }
    end
  end

  def student_search
    @student_list = []
    @key_search_email = "%#{params[:key_student].downcase}%"
    @key_search_name ="%#{params[:key_student].downcase}%"
    @users = User.where("(name like ? OR email like ?) AND school_class_id IS NULL" , @key_search_name ,@key_search_email)
    @users.each do |user|
      @student_list << user if(user.has_role? :student)
    end
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @student_list }
    end
  end

  def add_student
    @student_list = []
    @student_list_add = params[:list]
    @student_list_add.each do |s_list|
      @user = User.find(s_list)
      @user.school_class_id = params[:id]
      if @user.save
        @student_list << @user
      end
    end
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @student_list }
    end
  end

  def remove_student
    @school_class = SchoolClass.find(params[:id])
    @student_list_remove = params[:list]
    @student_list_remove.each do |s_list|
      @school_class.users.find(s_list).update_attribute('school_class_id' , nil)
    end
    @student_list =  @school_class.users
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @student_list }
    end
  end

end
