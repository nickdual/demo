class AssignmentsController < ApplicationController
  before_filter :authenticate_user!
  def index
    @assignments = Assignment.all
    authorize! :index, @assignments, :message => 'Not authorized as a teacher.'
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @assignments }
    end
  end

  def show
    @assignment = Assignment.find(params[:id])
    authorize! :show, @assignment, :message => 'Not authorized as a teacher.'
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @assignment}
    end
  end
  def new
    @assignment = Assignment.new()
    authorize! :new, @assignment, :message => 'Not authorized as a teacher.'
    @students = User.with_role(:student).paginate(:page => params[:page], :per_page => 10)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @assignment }
      format.js
    end
  end

  def create
    @assignment = Assignment.new(params[:assignment])
    authorize! :index, @assignments, :message => 'Not authorized as a teacher.'
    respond_to do |format|
      if @assignment.save
        @assignment.users.each do |user|
          @grading = Grading.new({:assignment_id => @assignment.id,:grade_system_id => @assignment.grade_system.nil? ? nil : @assignment.grade_system.id, :user_id => user.id})
          if @grading.save && @assignment.assignment_library.present?
            @assignment.assignment_library.questions.each do |question|
              @grading_detail = GradingDetail.new({:grading_id => @grading.id , :question_id => question.id })
              @grading_detail.save
            end
          end
        end
        format.html { redirect_to @assignment, notice: 'Assignment was successfully created.' }
        format.json { render json: @assignment, status: :created, location: @assignment }
      else
        format.html { render action: "new" }
        format.json { render json: @assignment.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @assignment = Assignment.find(params[:id])
    @students = User.with_role(:student).paginate(:page => params[:page], :per_page => 10)
  end

  def update
    @assignment = Assignment.find(params[:id])
    @user_list = @assignment.user_ids
    puts(@user_list)
    respond_to do |format|
      if @assignment.update_attributes(params[:assignment])
        format.html { redirect_to @assignment, notice: 'Assignment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @assignment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @assignment = Assignment.find(params[:id])
    @assignment.destroy

    respond_to do |format|
      format.html { redirect_to assignments_url }
      format.json { head :no_content }
    end
  end
  def student_search
    @students = []
    @key_search ="%#{params[:key_student].downcase}%"
    @users = User.where("(name like ? OR email like ?)" , @key_search ,@key_search)
    @users.each do |user|
      @students << user if(user.has_role? :student)
    end
    if @students.length != 0
      @render = render_to_string  :partial => 'shared/form_row_student_table',:locals => {:students =>  @students }
      @render = @render.gsub!(/\n/, "")
      respond_to do |format|
        format.html {render :text =>  @render}
        format.json { render json: @students }
        format.js
      end
    else
      render :text => :no_result
    end
  end
end
