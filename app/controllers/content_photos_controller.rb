class ContentPhotosController < ApplicationController
  # GET /pictures
  # GET /pictures.json
  before_filter :authenticate_user!
  def index
    @pictures = ContentPhoto.all(:limit => 10)
    authorize! :index, @pictures, :message => 'Not authorized as a teacher.'
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pictures }
    end
  end

  # GET /pictures/1
  # GET /pictures/1.json
  def show
    @picture = ContentPhoto.find(params[:id])
    authorize! :show, @pictures, :message => 'Not authorized as a teacher.'
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @picture }
    end
  end

  # GET /pictures/new
  # GET /pictures/new.json
  def new
    @picture = ContentPhoto.new
    authorize! :new, @picture, :message => 'Not authorized as a teacher.'
    @pictures = ContentPhoto.all(:limit => 100)
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @picture }
    end
  end

  # GET /pictures/1/edit
  def edit
    @picture = ContentPhoto.find(params[:id])
    authorize! :edit, @picture, :message => 'Not authorized as a teacher.'
    # @picture = Picture.find(params[:id])
  end

  # POST /pictures
  # POST /pictures.json
  def create
    p_attr = params[:content_photo]
    p_attr[:image] = params[:content_photo][:image].first if params[:content_photo][:image].class == Array
    p_attr[:filename] = params[:content_photo][:image].original_filename
    @picture =ContentPhoto.new(p_attr)
    authorize! :create, @picture, :message => 'Not authorized as a teacher.'
    if @picture.save
      respond_to do |format|
        format.html {
          puts 'c'
          render :json => [@picture.to_jq_upload].to_json,
          :content_type => 'text/html',
          :layout => false
        }
        format.json {
          puts 'b'
          render :json => [@picture.to_jq_upload].to_json
        }
      end
    else
      puts @picture.errors.full_messages
      render :json => [{:error => "custom_failure"}], :status => 304
    end
  end

  # PUT /pictures/1
  # PUT /pictures/1.json
  def update
    @picture = ContentPhoto.find(params[:id])
    authorize! :update, @picture, :message => 'Not authorized as a teacher.'
    respond_to do |format|
      if @picture.update_attributes(params[:picture])
        format.html { redirect_to new_content_photo_path, notice: 'Picture was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @picture.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pictures/1
  # DELETE /pictures/1.json
  def destroy
    @picture = ContentPhoto.find(params[:id])
    authorize! :destroy, @picture, :message => 'Not authorized as a teacher.'
    @picture.destroy
    respond_to do |format|
      format.html { redirect_to gallery_pictures_url }
      format.js
    end
  end

  def search
    @query ="%#{params[:file_name].downcase}%"
    @files = ContentPhoto.where("image like ?",@query).limit(10)
    @photos = []
    @files.each do |file|
     @photos << [file.id,file.image.thumb.url,file.image.url]
    end
    render json: @files.to_json
  end

  def load_image
    @pictures = ContentPhoto.paginate(:per_page => 10, :page => params[:page])
    render json: @pictures.to_json
  end

end
