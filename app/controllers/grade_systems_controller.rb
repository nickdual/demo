class GradeSystemsController < ApplicationController
  # GET /grade_systems
  # GET /grade_systems.json
  before_filter :authenticate_user!
  def index
    @grade_systems = GradeSystem.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @grade_systems }
    end
  end

  # GET /grade_systems/1
  # GET /grade_systems/1.json
  def show
    @grade_system = GradeSystem.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @grade_system }
    end
  end

  # GET /grade_systems/new
  # GET /grade_systems/new.json
  def new
    @grade_system = GradeSystem.new

    respond_to do |format|
      format.html # new_bk.html.erb
      format.json { render json: @grade_system }
    end
  end

  # GET /grade_systems/1/edit
  def edit
    @grade_system = GradeSystem.find(params[:id])
  end

  # POST /grade_systems
  # POST /grade_systems.json
  def create
    @grade_system = GradeSystem.new(params[:grade_system])

    respond_to do |format|
      if @grade_system.save
        format.html { redirect_to @grade_system, notice: 'Grade system was successfully created.' }
        format.json { render json: @grade_system, status: :created, location: @grade_system }
      else
        format.html { render action: "new" }
        format.json { render json: @grade_system.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /grade_systems/1
  # PUT /grade_systems/1.json
  def update
    @grade_system = GradeSystem.find(params[:id])

    respond_to do |format|
      if @grade_system.update_attributes(params[:grade_system])
        format.html { redirect_to @grade_system, notice: 'Grade system was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @grade_system.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /grade_systems/1
  # DELETE /grade_systems/1.json
  def destroy
    @grade_system = GradeSystem.find(params[:id])
    @grade_system.destroy

    respond_to do |format|
      format.html { redirect_to grade_systems_url }
      format.json { head :no_content }
    end
  end
end
