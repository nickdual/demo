class SchoolGradesController < ApplicationController
  # GET /school_grades
  # GET /school_grades.json
  def index
    @school_grades = SchoolGrade.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @school_grades }
    end
  end

  # GET /school_grades/1
  # GET /school_grades/1.json
  def show
    @school_grade = SchoolGrade.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @school_grade }
    end
  end

  # GET /school_grades/new
  # GET /school_grades/new.json
  def new
    @school_grade = SchoolGrade.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @school_grade }
    end
  end

  # GET /school_grades/1/edit
  def edit
    @school_grade = SchoolGrade.find(params[:id])
  end

  # POST /school_grades
  # POST /school_grades.json
  def create
    @school_grade = SchoolGrade.new(params[:school_grade])

    respond_to do |format|
      if @school_grade.save
        format.html { redirect_to @school_grade, notice: 'School grade was successfully created.' }
        format.json { render json: @school_grade, status: :created, location: @school_grade }
      else
        format.html { render action: "new" }
        format.json { render json: @school_grade.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /school_grades/1
  # PUT /school_grades/1.json
  def update
    @school_grade = SchoolGrade.find(params[:id])

    respond_to do |format|
      if @school_grade.update_attributes(params[:school_grade])
        format.html { redirect_to @school_grade, notice: 'School grade was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @school_grade.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /school_grades/1
  # DELETE /school_grades/1.json
  def destroy
    @school_grade = SchoolGrade.find(params[:id])
    @school_grade.destroy
    respond_to do |format|
      format.html { redirect_to school_grades_url }
      format.json { head :no_content }
    end
  end
end
