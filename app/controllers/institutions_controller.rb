class InstitutionsController < ApplicationController
  # GET /institutions
  # GET /institutions.json
  def index
    @institutions = Institution.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @institutions }
    end
  end

  # GET /institutions/1
  # GET /institutions/1.json
  def show
    @institution = Institution.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @institution }
    end
  end

  # GET /institutions/new
  # GET /institutions/new.json
  def new
    @institution = Institution.new
    @countries = Country.all
    respond_to do |format|
      format.html # new_bk.html.erb
      format.json { render json: @institution }
    end
  end

  # GET /institutions/1/edit
  def edit
    @institution = Institution.find(params[:id])
    @countries = Country.all
  end

  # POST /institutions
  # POST /institutions.json
  def create
    p_attr = params[:institution]
    p_attr[:logo] = params[:institution][:logo].first if params[:institution][:logo].class == Array
    #p_attr[:filename] = params[:institution][:logo].original_filename
    @institution = Institution.new(p_attr)

    respond_to do |format|
      if @institution.save
        format.html { redirect_to @institution, notice: 'Institution was successfully created.' }
        format.json { render json: @institution, status: :created, location: @institution }

      else
        puts @institution.errors.full_messages
        format.html { render action: "new" }
        format.json { render json: @institution.errors, status: :unprocessable_entity }
      end

    end
  end

  # PUT /institutions/1
  # PUT /institutions/1.json
  def update
    @institution = Institution.find(params[:id])

    respond_to do |format|
      if @institution.update_attributes(params[:institution])
        format.html { redirect_to @institution, notice: 'Institution was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @institution.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /institutions/1
  # DELETE /institutions/1.json
  def destroy
    @institution = Institution.find(params[:id])
    @institution.destroy

    respond_to do |format|
      format.html { redirect_to institutions_url }
      format.json { head :no_content }
    end
  end

  def delete_institution
    Institution.destroy_all(["id IN (?)", params['list']])
    @institutions = Institution.all
    render json: :ok
  end

  def city_autocomplete

    country_id = params[:country_id]
    if (country_id.blank?)
      render json: {:data => nil}
      return
    end
    content = params[:term].downcase
    cities = City.where('lower(name) like ? AND country_id = ?', '%' + content + '%', country_id).limit(50)
    cities.collect! {|item| {:label => item.name, :id => item.id}}
    render json: {:data => cities}
  end


end
