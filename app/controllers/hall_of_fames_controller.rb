class HallOfFamesController < ApplicationController
  # GET /hall_of_fames
  # GET /hall_of_fames.json
  def index
    @hall_of_fames = HallOfFame.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @hall_of_fames }
    end
  end

  # GET /hall_of_fames/1
  # GET /hall_of_fames/1.json
  def show
    @hall_of_fame = HallOfFame.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @hall_of_fame }
    end
  end

  # GET /hall_of_fames/new
  # GET /hall_of_fames/new.json
  def new
    @hall_of_fame = HallOfFame.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @hall_of_fame }
    end
  end

  # GET /hall_of_fames/1/edit
  def edit
    @hall_of_fame = HallOfFame.find(params[:id])
  end

  # POST /hall_of_fames
  # POST /hall_of_fames.json
  def create
    @hall_of_fame = HallOfFame.new(params[:hall_of_fame])

    respond_to do |format|
      if @hall_of_fame.save
        format.html { redirect_to @hall_of_fame, notice: 'Hall of fame was successfully created.' }
        format.json { render json: @hall_of_fame, status: :created, location: @hall_of_fame }
      else
        format.html { render action: "new" }
        format.json { render json: @hall_of_fame.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /hall_of_fames/1
  # PUT /hall_of_fames/1.json
  def update
    @hall_of_fame = HallOfFame.find(params[:id])

    respond_to do |format|
      if @hall_of_fame.update_attributes(params[:hall_of_fame])
        format.html { redirect_to @hall_of_fame, notice: 'Hall of fame was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @hall_of_fame.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hall_of_fames/1
  # DELETE /hall_of_fames/1.json
  def destroy
    @hall_of_fame = HallOfFame.find(params[:id])
    @hall_of_fame.destroy

    respond_to do |format|
      format.html { redirect_to hall_of_fames_url }
      format.json { head :no_content }
    end
  end
end
