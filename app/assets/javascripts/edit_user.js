

$(document).ready(function(){

    $("#btn_new_additional_emergency_contacts").click(function(){

      $("#new_emergency_contact").show();
      $("#new_emergency_contact").find('.additional_emergency_contacts_name').val('');
      $("#new_emergency_contact").find('.additional_emergency_contacts_address').val('');
      $("#new_emergency_contact").find('.additional_emergency_contacts_phone').val('');
      $("#new_emergency_contact").find('.additional_emergency_contacts_email').val('');

    });

    $('.save-education').click(function(){
        $("#educationModal").modal("hide")

    })

    $('span.edit-additional-emergency-contacts').live('click',function(){

        var tr = jQuery(this).parents('tr')
        var name = tr.find('td.name').text()
        var address = tr.find('td.address').text()
        var phone = tr.find('td.phone').text()
        var email = tr.find('td.email').text()
        row = '<td><input class = "additional_emergency_contacts_name" type="text" value="'+name+'"/> </td>'
        row += '<td> <input class="additional_emergency_contacts_address" type="text" value="'+ address +'"/></td>'
        row += '<td> <input class="additional_emergency_contacts_phone" type="text" value="' + phone + '"/></td>'
        row += '<td> <input class="additional_emergency_contacts_email" type="text" value="' + email + '"/></td>'
        row += '<td class="educations">' + tr.find('td.educations').html()  + '</td>'
        row += '<td> <input class="additional_emergency_contacts_update" id="update_emergency_contact" type="button" value="Save"/></td>'
        tr.html(row)
        $('.additional_emergency_contacts_name').focus()
    });


    $('span.delete-additional-emergency-contacts').live('click',function(){
        var tr = jQuery(this).parents('tr')
        var emergency_contact_id = tr.attr('id').substr(30)
//        puts(emergency_contact_id)
        $.ajax({
            type: 'POST',
            url: '/users/delete_emergency_contact',
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
            },
            data: {id: emergency_contact_id},
            success: function(data){
                tr.remove()

            }
        });

    });



    $('.cancel-emergency-contact').live('click',function(){
        $("#new_emergency_contact").hide();
        $("#btn_new_additional_emergency_contacts").removeAttr("disabled")
    })

    $('.additional_emergency_contacts_update').live('click',function(){
        var tr = jQuery(this).parents('tr')
        var emergency_contact_id = tr.attr('id').substr(30)
        var name = tr.find('input.additional_emergency_contacts_name').val()
        var address = tr.find('input.additional_emergency_contacts_address').val()
        var phone = tr.find('input.additional_emergency_contacts_phone').val()
        var email = tr.find('input.additional_emergency_contacts_email').val()
        var html =  tr.find('td.educations').html()
        $.ajax({
            type: 'POST',
            url: '/users/update_emergency_contact',
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
            },
            data: {id: emergency_contact_id, emergency_contacts: { name: name, address: address, phone: phone, email: email }},
            success: function(data){
                tr.html('')
                row = '<td class="name">' + data.name + '</td>'
                row += '<td class="address">' + data.address + '</td>'
                row += '<td class="phone">' + data.phone + '</td>'
                row += '<td class="email">' + data.email + '</td>'
                row += '<td class="educations">' + html  + '</td>'
                row += '<td> <span class="edit edit-additional-emergency-contacts">Edit</span> <span class="delete delete-additional-emergency-contacts" >Delete</span></td></td>'
                tr.append(row)
            }
        });
    });

    $('input.save-race').live('click', function(){
       var race_list = []
        $.each($('div.race-list > div.race_content_form').find('div.race-content span.checkbox input[type=checkbox][checked=checked]'), function(index, value) {
            div = $(value).closest('div');
            race_list.push(div.attr('id').substr(5));
        });
        $.ajax({
            type: 'PUT',
            url: '/users/add_race',
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
            },
            data: {race_list:race_list },
            success: function(data){

            }
        });

    });

    $('input.save-residence').live('click', function(){
        var residence_list = []
        var el = $("div.residence > div.residence-list > div.residence_content input[type='radio']:checked")
        var residence_id  = el.closest('div').attr('id').substr(10);
        $.ajax({
            type: 'PUT',
            url: '/users/add_residence_id',
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
            },
            data: {id: residence_id },
            success: function(data){

            }
        });
    });



});
