

$(document).ready(function(){
    $('#search_question').live('click', function(){
        $.ajax({
            type: 'GET',
            url: '/assignment_libraries/question_search',
            dataType: 'text',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
            },
            data: {id: $('.assignment_id').val(), key_question: $('#key_search_question').val()},
            success: function(data){
                if(data != 'no_result')
                {
                    $('.no-result').hide()
                    $('div.question-search').show()
                    $('div.question-default').hide()
                    $('.question-search tbody.question-content').html(data)
                }
                else
                {
                    $('div.question-search').hide()
                    $('.no-result').show()
                }
            }
        });
    });
    $('#close_search_question').live('click', function(){
        $('div.question-search').hide()
        $('div.question-default').show()

    });

    $('.btn-delete-question').live('click', function(){
        var list = []
        var assignment_library_id = $('input.assignment_id').val();
        $.each($('.question-list.stdtable > tbody').find('span.checkbox input[type=checkbox][checked=checked]'), function(index, value) {
            tr = $(value).closest('tr');
            list.push(tr.attr('id').substr(9));
            tr.remove();
        });
        $.ajax({
            type: 'GET',
            url: '/assignment_libraries/destroy_question',
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
            },
            data: {id: assignment_library_id,list: list},
            success: function(data){

            }
        });
    });


    $('.btn-add-question').live('click', function(){
        var list = []
        var assignment_library_id = $('input.assignment_id').val();
        $.each($('.question-list-result.stdtable > tbody').find('input[type=checkbox][checked=checked]'), function(index, value) {
            tr = $(value).closest('tr');
            list.push(tr.attr('id').substr(9));
            tr.remove();
        });
        $.ajax({
            type: 'GET',
            url: '/assignment_libraries/add_question',
            dataType: 'text',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
            },
            data: {id: assignment_library_id, list_question_ids:list },
            success: function(data){
                $('.question-list-show table.question-list>tbody').html(data)
            }
        });
    });



    $('.search_student').live('click', function(){
        var el = $('.student-list').find('.result-list')
        $.ajax({
            type: "GET",
            url: "/assignments/student_search",
            dataType: "text",
            data:{ key_student: $("#key_search_student").val()},
            success: function(data){
                if(data != 'no_result')
                {
                    $('.no-result').hide()
                    $('.student-list-result').show()
                    $('.student-search-result tbody.student-content').html(data)
                }
                else{
                    $('.no-result').show()
                    $('.student-list-result').hide()
                }
            }
        });
    });

//    $('.save-grading-system').live('click', function(){
//        var grade_system_id = $('p > #grading_system').val()
//        var grading_id = $('p > input#grading_id').val()
//        $.ajax({
//            type: "GET",
//            url: "/gradings/add_grade_system",
//            dataType: "json",
//            data:{ id:grading_id, grade_system_id: grade_system_id},
//            success: function(data){
//
//            }
//        });
//    });

    //    js for grding score

    $('#save_score').live('click', function(){
        var point_total = 0;
        var arr_empty = []
        var temp = 0;
        var id, point, point_now, point_before;
        var point_total_before = $('#grading_point').val();
        $.each($('tbody#question_list').find('input.question-point') , function(index, value) {
            id = $(value).attr('id').substr(6)
            point_now =  $(value).val()
            if(point_now == ''){
                temp += 1;
                arr_empty.push($(value).closest('td').attr('id').substr(21));
            }
            point = (point_now != '')? parseFloat(point_now):parseFloat(0)
            point_total += point
            point_before = $(value).closest('td').find('input#point_hidden').val()
            if(point_now != point_before){
                $.ajax({
                    type: "GET",
                    url: "/gradings/add_point_grading_detail",
                    dataType: "json",
                    data:{ id:id, point: point_now},
                    success: function(data){
                        $(value).closest('td').find('input#point_hidden').val(data)
                    }
                });
            }
        });
        if(temp == 0){
            $('#alert_warming').hide()
            if(point_total_before != point_total){
                $.ajax({
                    type: "GET",
                    url: "/gradings/add_point_grading",
                    dataType: "json",
                    data:{ id:$('#grading_id').val(), point: point_total},
                    success: function(data){
                        $('#grading_point').val(parseFloat(point_total))
                    }
                });
            }
        }
        else{
            $('#alert_warming').html('Maybe some answers are not get point yet. Please check the following answers: ' )
                for(var i = 0;i < arr_empty.length; i ++){
               $('#alert_warming').append(arr_empty[i])
               if((i + 1) < arr_empty.length)
               {
                   $('#alert_warming').append(',')
               }
           }
            $('#alert_warming').show()


        }


    })
});
