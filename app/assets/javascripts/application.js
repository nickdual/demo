// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//

//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require template/jquery-isotope
//= require template/jquery-prettyPhoto
//= require template/jquery-cslider
//= require template/modernizr-custom-28468
//= require template/filter
//= require template/cycle
//= require chosen.jquery.min
//= require bootstrap-datepicker
//= require sponsors
//= require pagination
//= require twitter/bootstrap
//= require tinymce
//= require jquery_upload_files/jquery.Jcrop
//= require jquery_upload_files/load-image.min
//= require jquery_upload_files/canvas-to-blob.min
//= require jquery_upload_files/jquery.iframe-transport
//= require jquery_upload_files/jquery.fileupload
//= require jquery_upload_files/jquery.fileupload-ip
//= require jquery_upload_files/jquery.fileupload-ui
//= require jquery_upload_files/locale
//= require jquery_upload_files/main
//= require jquery_upload_files/pictures
//= require jquery_upload_files/slimbox2
 //= require private_pub

 //= require i18n/translations
//= require assignment_grading

//= require edit_user



$(document).ready(function(){

    $(function() {
        $( ".sortable" ).sortable({

            update: function(event, ui){
                var sortedIDs = $(this).sortable( "toArray",{attribute: 'data'});
                $("#sortable_" + $(this).attr("id")).find('#body_choice').val(sortedIDs.join('-/-'));
                console.log(sortedIDs) ;
                console.log("#sortable_" +  $(this).attr("id"))  ;
            }
        });
        $( ".sortable" ).disableSelection();
    });

    if($("#body_partial").is(':checked')){
        $("#answer").find(':radio').hide();  // checked
        $("#answer").find('input[name=\'points[]\']').show()
    }

    else{
        $("#answer").find(':radio').show();  // unchecked
        $("#answer").find('input[name=\'points[]\']').hide()
    }
    $("#body_partial").live('click', function(){
        if($(this).is(':checked')){
           $("#answer").find(':radio').hide();  // checked
            $("#answer").find(':radio').prop('checked', false);
           $("#answer").find('input[name=\'points[]\']').show()
        }
        else{
            $("#answer").find(':radio').show();  // unchecked
            $("#answer").find('input[name=\'points[]\']').hide()
        }


    });
    $("#body_question_type").live('click', function(){
        ch = $(this).val().replace(' ',"_").toLowerCase()  ;
        window.location = "?choice=" + ch ;
    });
    $("#del_answer").live('click', function(){
        el = $(this).closest('p')  ;
        el.remove();
    });

    $("#add_answer").on('click', function(){
        var N = parseInt($("#answer").find(':radio:last').val()) + 1;

        el = '<p>' ;
        el += '<input id="body_answer_" name="body[answer][]" type="text"/>';

        if(jQuery('#body_partial').is(':checked')) {
            el += '<input  id="body_correct_' + N + ' " name="body[correct]" type="radio" value=" '+ N + ' " style ="margin-left: 5px; display: none;"> ';
            el += '<input id="points_" name="points[]" type="text" style="display: inline-block; margin-right: 5px;">';
        }
        else{
            el += '<input  id="body_correct_' + N + ' " name="body[correct]" type="radio" value=" '+ N + ' " style ="margin-left: 5px;"> ';
            el += '<input id="points_" name="points[]" type="text" style="display: none;">';
        }
        el += '<button id="del_answer" type="button" class="btn" >Del</button>' ;
        el += '</p>' ;
        $("#answer").append(el);
    });

    $("#add_multiple_answer").live('click', function(){
        var i = parseInt($("#answer").find('input[type = "checkbox"]:last').val()) + 1;
        el = '<p>' ;
        el += '<input id="body_answer__text" name="body[answer][][text]" type="text"/>' ;
        el += '<span class="checkbox"><input id="body_answer__correct" name="body[answer][][correct]" type="checkbox" value="'+ i +'"/> </span>'
        el += '<button id="del_answer" type="button" class="btn">Del</button>' ;
        el += '</p>' ;
        $("#answer").append(el);
    });

    $("#add_matching").live('click', function(){
        el = '<p><input id="body_answer__stems" name="body[answer][][stems]" type="text"/><input id="body_answer__stems_index" name="body[answer][][stems_index]" type="hidden">';
        el += '<input id="body_answer__choice" name="body[answer][][choice]" type="text"/> <input id="body_answer__choice_index" name="body[answer][][choice_index]" type="hidden"><button id="del_answer" type="button" class="btn">Del</button></p>' ;
        $("#answer").append(el);
    });

    $("#add_sorting").live('click', function(){
        el = '<p><input id="body_answer_" name="body[answer][][text]" type="text"/> <input id="body_answer__index" name="body[answer][][index]" type="hidden">'
        el +=    ' <button id="del_answer" type="button" class="btn">Del</button></p>' ;
        $("#answer").append(el);
    });


    $("#search_file_name").live('click',function(){
        var el = $("#list_photos").find('.thumbnails.files');
        el.html('');
        $.ajax({
            type: "POST",
            url: "/content_photos/search/"  + $("#content_photo_file_name").val(),
            dataType: "json",
            success: function(data){
                $.each(data, function (index, value) {
                    template = '<li class="span3" id="picture_' + value.id + '">' ;
                    template +=  '<div class="thumbnail">';
                    template +=  '<a href="' + value.image.url +'" rel="lightbox-pictures" title="">';
                    template +=  '<img src="' + value.image.url +'"> </a>';
                    template +=  '<div class="caption">'  ;
                    template +=  '<p style="text-align: center;">';
                    template +=  '<a href="/content_photos/'+ value.id + '/edit" class="btn btn-mini">'
                    template +=  '<i class="icon-edit "></i>'  ;
                    template +=   '  Edit'  ;
                    template +=   '   </a>' ;
                    template +=  ' <a class="btn btn-mini btn-delete" confirm="Are you delete ?" data-method="delete" href="/content_photos/'+value.id +'" data-remote="true">';
                    template +=  ' <i class="icon-trash"></i>'     ;
                    template +=  ' Delete'  ;
                    template +=   ' </a>' ;
                    template +=   ' </p>' ;
                    template +=   '</div>' ;
                    template +=    ' </div>' ;
                    template +=    '</li>';
                    el.append(template);
                });
            }
        });
    });

    $("#search_content").live('click',function(){
        var el = $("#content-list");
        $.ajax({
            type: "GET",
            url: "/contents/search_content",
            dataType: "json",
            data:{ query: $("#key_tag_search").val()},
            success: function(data){
                if(data.length != 0)
                {
                    table = '<table>'
                    table += '<tr>'
                    table += '<th style="width: 85%; text-align: left;">Name</th>'
                    table +=   '<th style="width: 5%; text-align: center;">Draft</th>'
                    table +=    '<th style="width: 5%; text-align: center;"></th>'
                    table +=    '<th style="width: 5%; text-align: center;"></th>'
                    table +=   ' <th></th>'
                    table +='</tr>'
                    table +='</table>'
                    el.html(table);
                    el = el.find('tbody')
                    $.each(data, function (index, value) {
                        template = '<tr>';
                        template += '<td>' + value.name + '</td>';
                        template += '<td style="text-align:center;"><input id="content_draft" name="content[draft]" type="checkbox" value="' + value.draft + '"/>  </td>';
                        template += '<td><a href="/contents/'+ value.id+'">Show</a> </td>';
                        template += '<td><a href="/contents/'+ value.id+'/edit">Edit</a></td>';
                        template += '</tr>';
                        el.append(template);
                    });
                }
                else{
                    template = '<div> Search result not found!</div>'
                    el.html(template);
                }

            }
        });
    });

    $('#Edit_button').live('click',function()
    {
        var id = $('#tag_id_edit').val();
        var el = $('#row_'+id)
        $.ajax({
            type: "GET",
            url: "/contents/edit_tag_name",
            dataType: "json",
            data:{ tag_name_edit: $("#name_tag").val(),
                tag_id:  id
            },
            success: function(data){
                $('#modal_edit').modal('hide')
                el.html('')
                template = '<td><a href="?tag='+data.name+'" class="btn">'+data.name+'</a> </td>'
                template +=   '<td><a href="#modal_edit" id="edit_tag" data-toggle="modal">Edit</a>'
                template +=   '<input id="tag_id" type="hidden" value="'+data.id+'"/>'
                template +=   '<input id="tag_name" type="hidden" value="'+data.name+'"/></td>'
                template +=   '<td><a href="/contents/remove_tag/'+data.id+'">Delete</a></td>'
                el.html(template)
            }
        });
    });
    $('#edit_tag').live('click',function()
    {
        text = $(this).parent().children('#tag_name').val()
        id = $(this).parent().children('#tag_id').val()
        $('input[name="name_tag"]').attr('value',text);
        $('input[name="tag_id"]').attr('value',id);
    });

    $("#btn-delete").live('click', function(){
        var list = []
        $.each($('.stdtable > tbody').find('span.checkbox input[type=checkbox][checked=checked]'), function(index, value) {
            tr = $(value).closest('tr');
            list.push(tr.attr('id').substr(12));
        });
        if (list.length > 0){
            $.each($('.stdtable > tbody').find('span.checkbox input[type=checkbox][checked=checked]'), function(index, value){
                var tr = $(value).closest('tr');
                tr.remove();
            });
            $.ajax({
                type: 'POST',
                url: '/institutions/delete_institution',
                async: true,
                jsonpCallback: 'jsonCallback',
                dataType: 'json',
                data: {list: list},
                success: function(data){

                }
            });
        }

    });

    jQuery('input[type=checkbox]').each(function(){
        var t = jQuery(this);
        t.wrap('<span class="checkbox"></span>');

    });

    jQuery('input[type=checkbox]').live('click',function(){
        var t = jQuery(this);
        if(jQuery(this).is(':checked')) {
            t.attr('checked',true);
            t.parent().addClass('checked');
            t.parents('tr').addClass('selected');
        } else {
            t.attr('checked',false);
            t.parent().removeClass('checked');
            t.parents('tr').removeClass('selected');
        }
    });

    jQuery('.stdtable .checkall').live('click',function(){
        var parentTable = jQuery(this).parents('table');
        var ch = parentTable.find('tbody input[type=checkbox]');
        if(jQuery(this).is(':checked')) {
            //check all rows in table
            ch.each(function(){
                jQuery(this).attr('checked',true);
                console.log(jQuery(this).parents('tr'))
                jQuery(this).parent().addClass('checked');      //used for the custom checkbox style
                jQuery(this).parents('tr').addClass('selected');
            });
            //check both table header and footer
            parentTable.find('.checkall').each(function(){ jQuery(this).attr('checked',true); });
        } else {
           //uncheck all rows in table
            ch.each(function(){
                jQuery(this).attr('checked',false);
                console.log(jQuery(this).parents('tr'))
                jQuery(this).parent().removeClass('checked');   //used for the custom checkbox style
                jQuery(this).parents('tr').removeClass('selected');
            });
            //uncheck both table header and footer
            parentTable.find('.checkall').each(function(){ jQuery(this).attr('checked',false); });
        }
    });


    $('#city_autocomplete').autocomplete({
        source: function(req, add){
            $.getJSON('/institutions/city_autocomplete?country_id='+$("#country_id").val()+'', req, function(data){
                add(data.data);

            })
        },

        select: function(event, ui){

            $('#city_id').val(ui.item.id);
        }
    });

    $('#city_name').autocomplete({
        source: function(req, add){
            $.getJSON('/home/city_autocomplete?coutry_id=' + $("#country_id").val(), req, function(data){
                console.info(data);
                add(data);
            })
        },
        select: function(event, ui){

            $('#sponsor_city_id').val(ui.item.id);
        }
    });

   //define chosen
    $('.select-country').chosen()
    $(".chzn-select").chosen();
    $(".chzn-select-school-grade").chosen();
    $(".chzn-select-room").chosen();
    $(".chzn-select-institution").chosen();

//    call datepicker
    $(function() {
        $("#dpYears").datepicker( {
            format: " yyyy",
            viewMode: "years",
            minViewMode: "years"
        })
            .on('changeDate', function(ev){
                $('.dropdown-menu').hide()
            })

    });

//    search student for class

    $('.notification_search').hide()
    $('.student-list-result').hide()
    $('#search_student').live('click', function(){
        var el = $('.student-list').find('.result-list')
        $.ajax({
            type: "GET",
            url: "/school_classes/student_search",
            dataType: "json",
            data:{ key_student: $("#key_search_student").val()},
            success: function(data){
                if(data.length != 0){
                    $('.notification_search').hide()
                    $('.student-list-result').show()
                    el.html('')
                    $.each(data, function (index, value) {
                        template = '<tr id="student_'+value.id+'">'
                        template += '<td><span class="checkbox"><input  type="checkbox"/></span></td>'
                        template += '<td>'+(value.name ? value.name : '') +' </td>'
                        template += '<td>'+ value.email +'</td>'
                        template +=  '</tr>'
                        el.append(template)
                    })
                }
                else {
                     $('.student-list-result').hide()
                     $('.notification_search').show()
                    }
            }
        });
    });
    //    add student to class
    $("#btn-add-student").live('click', function(){
        var list = []
        var id_class = $('#id_class_edit').val()
        $.each($('.student-list-result > .stdtable > tbody').find('tr.selected span.checkbox input[type=checkbox][checked=checked]'), function(index, value) {
            tr = $(value).closest('tr');
            list.push(tr.attr('id').substr(8));
        });
        console.log(list.length)
        if (list.length > 0){
            $.each($('.stdtable > tbody').find('span.checkbox input[type=checkbox][checked=checked]'), function(index, value){
                var tr = $(value).closest('tr');
                tr.remove();
            });
            $.ajax({
                type: 'PUT',
                url: '/school_classes/add_student',
                async: true,
                jsonpCallback: 'jsonCallback',
                dataType: 'json',
                data: { id: id_class , list: list},
                success: function(data){
                    $('.no-student').hide()
                    $('.student-list-already').show()
                    var el = $('.student-list-already').find('.class-student-list')
                    $.each(data, function (index, value) {
                        template = '<tr id="student_'+value.id+'">'
                        template += '<td><span class="checkbox"><input  type="checkbox"/></span></td>'
                        template += '<td>'+ (value.name ? value.name : '') +' </td>'
                        template += '<td>'+ value.email +'</td>'
                        template +=  '</tr>'
                        el.append(template)
                    })
                }
            });
        }
    });

    $("#btn-delete-student").live('click', function(){
        var list = []
        var id_class = $('#id_class_edit').val()
        $.each($('.student-list-already > .stdtable > tbody').find('tr.selected span.checkbox input[type=checkbox][checked=checked]'), function(index, value) {
            tr = $(value).closest('tr');
            list.push(tr.attr('id').substr(8));
        });
        if (list.length > 0){
            $.each($('.stdtable > tbody').find('span.checkbox input[type=checkbox][checked=checked]'), function(index, value){
                var tr = $(value).closest('tr');
                tr.remove();
            });
            $.ajax({
                type: 'PUT',
                url: '/school_classes/remove_student',
                async: true,
                jsonpCallback: 'jsonCallback',
                dataType: 'json',
                data: { id: id_class , list: list},
                success: function(data){
                   if(data.length == 0){
                       $('.no-student').show()
                       $('.student-list-already').hide()
                   }
                }
            });
        }

    });

    $('#loading').ajaxStart(function(){
        $(this).show();
    }).ajaxStop(function(){
            $(this).hide();
    });




});

