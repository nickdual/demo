Cfa::Application.routes.draw do

  resources :uploaders do
    match '/uploaders' => 'uploaders#index'

  end

  resources :gradings  do
    collection do
      post '/answers'
      get '/add_grade_system'
      get '/add_point_grading_detail'
      get '/add_point_grading'
    end
  end

  resources :rooms

  resources :school_grades
  resources :assignment_libraries  do
    collection do
      get 'question_search'
      get 'destroy_question'
      get 'add_question'
    end
  end
  resources :school_classes do
    collection do
      get 'student_search'
      put 'add_student'
      put 'remove_student'
    end
  end

  resources :hall_of_fames
  
  resources :sponsors  do

  end
  resources :comments
  resources :relationship_types

  resources :questions

  resources :institutions do
    collection do
      post  'delete_institution'
      get 'city_autocomplete'
    end
  end


  resources :menu_items

  resources :game_assets

  resources :grade_systems

  resources :hobbies

  resources :elements

  resources :concepts

  resources :subjects

  resources :healths

  resources :residences

  resources :assignments do
    collection do
      get 'student_search'
    end
  end

  match '/educations/change_emergency_contacts'  => "educations#change_emergency_contacts"
  match '/users/change_healths' => "users#change_healths"
  resources :educations

  resources :eductations

  resources :races

  resources :phonetypes

  resources :phones

  resources :emails

  resources :states

  resources :cities

  resources :addresses

  resources :links

  resources :badges



  resources :contents do
    collection do
      match '/load_images/:id' => "contents#load_images"
      get  "search_content"
      match '/tags'  => "contents#tag"
      get 'load_tags'
      match '/edit_tag_name' => 'contents#edit_tag_name'
      match '/remove_tag/:tag_id' => 'contents#remove_tag'

    end

  end
  resources :content_photos do
    collection do
      post 'make_default'
      post 'search/:file_name' => "content_photos#search"
      post 'load_image' => "content_photos#load_image"
    end
  end
  resources :pictures
  resources :galleries do
    resources :pictures do
      collection do
        post 'make_default'
      end
    end
  end

  authenticated :user do
    root :to => 'home#index'
  end
  root :to => "home#index"
  devise_for :users,:controllers => { :registrations => "registrations",:sessions => "sessions",:passwords => "passwords",:invitations => "invitations" }

  devise_scope :user do
    match 'users/create_parent' => 'invitations#create_parent'
    match '/advanced_invitation' => 'users#advanced_invitation'

  end
  resources :users do
    resources :additional_emergency_contacts
    collection do
      post 'create_advanced_invitation'
    end

  end

  get  '/chatroom' => 'chats#room', :as => :chat
  get '/home/city_autocomplete'
  post '/new_message' => 'chats#new_message', :as => :new_message
  match '/user/update' => "users#update_user"
  match 'users/add_emergency_contact' => 'users#add_emergency_contact'
  match 'users/update_emergency_contact' => 'users#update_emergency_contact'
  match 'users/delete_emergency_contact' => 'users#delete_emergency_contact'
  match 'users/add_race' => 'users#add_race'
  match 'users/add_residence_id' => 'users#add_residence_id'
  match 'users/add_form_emergency_contact' => 'users#add_form_emergency_contact'
  match '*path'  => 'home#error'

end