require 'spec_helper'

describe Content do
  it "has a valid factory" do
    FactoryGirl.create(:content).should be_valid
  end
  it "is invalid without a name" do
    @content = FactoryGirl.build(:content,:name => nil)
    @content.should_not be_valid
  end
  it "is invalid without a name duplicate" do
    @content1 = FactoryGirl.create(:content,:name => 'abc')
    @content2 = FactoryGirl.build(:content,:name => 'abc')
    @content2.should_not be_valid
  end
  it "is invalid without a body" do
    @content = FactoryGirl.build(:content,:body => nil)
    @content.should_not be_valid
  end

  it "is invalid without a user_id" do
    @content = FactoryGirl.build(:content,:user_id => nil)
    @content.should_not be_valid
  end
end
