require 'spec_helper'

describe Question do
  it "has a valid factory" do
    FactoryGirl.create(:question).should be_valid
  end
  it "is invalid without a name" do
    @question = FactoryGirl.create(:question,:name => nil)
    @question.should_not be_valid
  end

  it "is invalid without a body" do
    @question = FactoryGirl.create(:question,:body => nil)
    @question.should_not be_valid
  end

  it "is invalid without a user_id" do
    @question = FactoryGirl.create(:question,:user_id => nil)
    @question.should_not be_valid
  end

end
