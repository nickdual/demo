require "spec_helper"

describe UploadersController do
  describe "routing" do

    it "routes to #index" do
      get("/uploaders").should route_to("uploaders#index")
    end

    it "routes to #new" do
      get("/uploaders/new").should route_to("uploaders#new")
    end

    it "routes to #show" do
      get("/uploaders/1").should route_to("uploaders#show", :id => "1")
    end

    it "routes to #edit" do
      get("/uploaders/1/edit").should route_to("uploaders#edit", :id => "1")
    end

    it "routes to #create" do
      post("/uploaders").should route_to("uploaders#create")
    end

    it "routes to #update" do
      put("/uploaders/1").should route_to("uploaders#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/uploaders/1").should route_to("uploaders#destroy", :id => "1")
    end

  end
end
