require 'spec_helper'

describe UsersController do

  before (:each) do
    @user = FactoryGirl.create(:user)
    @user.add_role :student
    sign_in @user
    @education = FactoryGirl.create(:education)
    @education2 = FactoryGirl.create(:education2)
    @education3 = FactoryGirl.create(:education3)
    @education4 = FactoryGirl.create(:education4)
    @education5 = FactoryGirl.create(:education5)
  end

  describe "add emergency contact" do
    it "add success" do
      post :add_emergency_contact, :additional_emergency_contact => {"name"=>"new", "address"=>"here", "phone"=>"3333", "email"=>"emergency@gmail.com", "education_ids"=>["1", "3"]}, :format => :js
      @emergency = User.find_by_email("emergency@gmail.com")
      @emergency.role_names.to_s.should == 'Parent'
    end

  end
  describe "update emergency contact" do
    it "update success" do
      @emergency_current = @user.additional_emergency_contacts.build({"name"=>"new", "address"=>"here", "phone"=>"3333", "email"=>"emergency@gmail.com", "education_ids"=>["1", "3", ""]})
      @emergency_current.save
      post :update_emergency_contact, :id => @emergency_current.id, :additional_emergency_contact => {"name"=>"bbbb", "address"=>"bbbb", "phone"=>"22222", "email"=>"emergency@gmail.com", "education_ids"=>["1", "2", ""] }, :format => :js
      @emergency_update = AdditionalEmergencyContact.find(@emergency_current.id)
      @emergency_update.should_not be(@emergency_current)
    end
  end

end
