require 'spec_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

describe UploadersController do

  # This should return the minimal set of attributes required to create a valid
  # Uploader. As you add validations to Uploader, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { "filename" => "MyString" }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # UploadersController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  describe "GET index" do
    it "assigns all uploaders as @uploaders" do
      uploader = Uploader.create! valid_attributes
      get :index, {}, valid_session
      assigns(:uploaders).should eq([uploader])
    end
  end

  describe "GET show" do
    it "assigns the requested uploader as @uploader" do
      uploader = Uploader.create! valid_attributes
      get :show, {:id => uploader.to_param}, valid_session
      assigns(:uploader).should eq(uploader)
    end
  end

  describe "GET new" do
    it "assigns a new uploader as @uploader" do
      get :new, {}, valid_session
      assigns(:uploader).should be_a_new(Uploader)
    end
  end

  describe "GET edit" do
    it "assigns the requested uploader as @uploader" do
      uploader = Uploader.create! valid_attributes
      get :edit, {:id => uploader.to_param}, valid_session
      assigns(:uploader).should eq(uploader)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Uploader" do
        expect {
          post :create, {:uploader => valid_attributes}, valid_session
        }.to change(Uploader, :count).by(1)
      end

      it "assigns a newly created uploader as @uploader" do
        post :create, {:uploader => valid_attributes}, valid_session
        assigns(:uploader).should be_a(Uploader)
        assigns(:uploader).should be_persisted
      end

      it "redirects to the created uploader" do
        post :create, {:uploader => valid_attributes}, valid_session
        response.should redirect_to(Uploader.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved uploader as @uploader" do
        # Trigger the behavior that occurs when invalid params are submitted
        Uploader.any_instance.stub(:save).and_return(false)
        post :create, {:uploader => { "filename" => "invalid value" }}, valid_session
        assigns(:uploader).should be_a_new(Uploader)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        Uploader.any_instance.stub(:save).and_return(false)
        post :create, {:uploader => { "filename" => "invalid value" }}, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested uploader" do
        uploader = Uploader.create! valid_attributes
        # Assuming there are no other uploaders in the database, this
        # specifies that the Uploader created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        Uploader.any_instance.should_receive(:update_attributes).with({ "filename" => "MyString" })
        put :update, {:id => uploader.to_param, :uploader => { "filename" => "MyString" }}, valid_session
      end

      it "assigns the requested uploader as @uploader" do
        uploader = Uploader.create! valid_attributes
        put :update, {:id => uploader.to_param, :uploader => valid_attributes}, valid_session
        assigns(:uploader).should eq(uploader)
      end

      it "redirects to the uploader" do
        uploader = Uploader.create! valid_attributes
        put :update, {:id => uploader.to_param, :uploader => valid_attributes}, valid_session
        response.should redirect_to(uploader)
      end
    end

    describe "with invalid params" do
      it "assigns the uploader as @uploader" do
        uploader = Uploader.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        Uploader.any_instance.stub(:save).and_return(false)
        put :update, {:id => uploader.to_param, :uploader => { "filename" => "invalid value" }}, valid_session
        assigns(:uploader).should eq(uploader)
      end

      it "re-renders the 'edit' template" do
        uploader = Uploader.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        Uploader.any_instance.stub(:save).and_return(false)
        put :update, {:id => uploader.to_param, :uploader => { "filename" => "invalid value" }}, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested uploader" do
      uploader = Uploader.create! valid_attributes
      expect {
        delete :destroy, {:id => uploader.to_param}, valid_session
      }.to change(Uploader, :count).by(-1)
    end

    it "redirects to the uploaders list" do
      uploader = Uploader.create! valid_attributes
      delete :destroy, {:id => uploader.to_param}, valid_session
      response.should redirect_to(uploaders_url)
    end
  end

end
