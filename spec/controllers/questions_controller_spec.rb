require 'spec_helper'

describe QuestionsController do
  include Devise::TestHelpers
  def login_with_teacher
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @current_user = FactoryGirl.create(:user)
    @current_user.add_role :teacher
    sign_in @current_user
  end
  def login_with_student
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @current_user = FactoryGirl.create(:user)
    @current_user.add_role :teacher
    sign_in @current_user
  end
  before :each do
    login_with_teacher()
  end

  describe "GET index with teacher" do
    before :each do
      @multiple_choice = FactoryGirl.create(:question_multiple_choice)
      @multiple_choice_with_partial = FactoryGirl.create(:question_multiple_choice_with_partial)
      @multiple_answer = FactoryGirl.create(:question_multiple_answer)
      @true_false = FactoryGirl.create(:question_true_false)
      @question_match = FactoryGirl.create(:question_matching)
      @question_sorting = FactoryGirl.create(:question_sorting)
      @short_answer =  FactoryGirl.create(:question_short_answer)
      @long_answer =  FactoryGirl.create(:question_long_answer)
    end
    it "list questions" do
      get :index
      response.should render_template :index
    end
    it "should respond with success" do
      get :index
      response.should be_success
    end

    it "assigns all questions as @questions" do
      get :index
      assigns(:questions).should include(@question_sorting)
    end
  end

  describe "GET edit" do
    it "edit requested question as @question" do
      question = FactoryGirl.create(:question_multiple_choice)
      get :edit, {:id => question.to_param}
    end
  end
  #
  describe "POST create" do
    describe "create with valid params" do
      it "creates a new Question with multiple choice no partial" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_multiple_choice)
        }.to change(Question, :count).by(1)
        @question = Question.last
        @question.name.should == FactoryGirl.attributes_for(:params_multiple_choice)[:question][:name]
        @question.body[:question_type].should == FactoryGirl.attributes_for(:params_multiple_choice)[:body][:question_type]
        @question.body[:partial].present?.should == false
      end
      it "creates a new Question with multiple choice no partial answer correct nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_multiple_choice_with_answer_correct_nil)
        }.to change(Question, :count).by(1)
        @question = Question.last
        @question.name.should == FactoryGirl.attributes_for(:params_multiple_choice_with_answer_correct_nil)[:question][:name]
        @question.body[:question_type].should == FactoryGirl.attributes_for(:params_multiple_choice_with_answer_correct_nil)[:body][:question_type]
        @question.body[:partial].present?.should == false
      end
      it "creates a new Question with multiple choice no partial answer include nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_multiple_choice_with_answer_include_nil)
        }.to change(Question, :count).by(1)
        @question = Question.last
        @question.name.should == FactoryGirl.attributes_for(:params_multiple_choice_with_answer_include_nil)[:question][:name]
        @question.body[:question_type].should == FactoryGirl.attributes_for(:params_multiple_choice_with_answer_include_nil)[:body][:question_type]
        @question.body[:partial].present?.should == false
      end

      it "creates a new Question with multiple choice partial" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_multiple_choice_with_partial)
        }.to change(Question, :count).by(1)
        @question = Question.last
        @question.name.should == FactoryGirl.attributes_for(:params_multiple_choice_with_partial)[:question][:name]
        @question.body[:question_type].should == FactoryGirl.attributes_for(:params_multiple_choice_with_partial)[:body][:question_type]
        @question.body[:partial].should.present?.should == true
      end
      it "creates a new Question with multiple choice partial answer point nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_multiple_choice_with_partial_with_answer_point_nil)
        }.to change(Question, :count).by(1)
        puts(FactoryGirl.attributes_for(:params_multiple_choice_with_partial_with_answer_point_nil))
        @question = Question.last
        @question.name.should == FactoryGirl.attributes_for(:params_multiple_choice_with_partial_with_answer_point_nil)[:question][:name]
        @question.body[:question_type].should == FactoryGirl.attributes_for(:params_multiple_choice_with_partial_with_answer_point_nil)[:body][:question_type]
        @question.body[:partial].should.present?.should == true
      end

      it "creates a new Question with multiple answer" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_multiple_answer)
        }.to change(Question, :count).by(1)
        @question = Question.last
        @question.name.should == FactoryGirl.attributes_for(:params_multiple_answer)[:question][:name]
        @question.body[:question_type].should == FactoryGirl.attributes_for(:params_multiple_answer)[:body][:question_type]
        @question.body[:correct].size == [0,1,4]
      end
      it "creates a new Question with multiple answer with_answer_text_nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_multiple_answer_with_answer_text_nil)
        }.to change(Question, :count).by(1)
        @question = Question.last
        @question.name.should == FactoryGirl.attributes_for(:params_multiple_answer_with_answer_text_nil)[:question][:name]
        @question.body[:question_type].should == FactoryGirl.attributes_for(:params_multiple_answer_with_answer_text_nil)[:body][:question_type]
        @question.body[:correct].should == [0,3]
      end
      it "creates a new Question with multiple answer with_answer_text_nil_correct_nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_multiple_answer_with_answer_text_nil_correct_nil)
        }.to change(Question, :count).by(1)
        @question = Question.last
        @question.name.should == FactoryGirl.attributes_for(:params_multiple_answer_with_answer_text_nil_correct_nil)[:question][:name]
        @question.body[:question_type].should == FactoryGirl.attributes_for(:params_multiple_answer_with_answer_text_nil_correct_nil)[:body][:question_type]
        @question.body[:correct].should == [0,3]
      end

      it "creates a new Question with true false" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_true_false)
        }.to change(Question, :count).by(1)
        @question = Question.last
        @question.name.should == FactoryGirl.attributes_for(:params_true_false)[:question][:name]
        @question.body[:question_type].should == FactoryGirl.attributes_for(:params_true_false)[:body][:question_type]
        @question.body[:question].should ==  FactoryGirl.attributes_for(:params_true_false)[:body][:question]
      end
      it "creates a new Question with params_matching" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_matching)
        }.to change(Question, :count).by(1)
        @question = Question.last
        @question.name.should == FactoryGirl.attributes_for(:params_matching)[:question][:name]
        @question.body[:question_type].should == FactoryGirl.attributes_for(:params_matching)[:body][:question_type]
        @question.body[:question].should ==  FactoryGirl.attributes_for(:params_matching)[:body][:question]
      end
       it "creates a new Question with params_matching_with_stems_choice_nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_matching_with_stems_choice_nil)
        }.to change(Question, :count).by(1)
        @question = Question.last
        @question.name.should == FactoryGirl.attributes_for(:params_matching_with_stems_choice_nil)[:question][:name]
        @question.body[:question_type].should == FactoryGirl.attributes_for(:params_matching_with_stems_choice_nil)[:body][:question_type]
        @question.body[:question].should ==  FactoryGirl.attributes_for(:params_matching_with_stems_choice_nil)[:body][:question]
      end

      it "creates a new Question with sorting" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_sorting)
        }.to change(Question, :count).by(1)
        @question = Question.last
        @question.name.should == FactoryGirl.attributes_for(:params_sorting)[:question][:name]
        @question.body[:question_type].should == FactoryGirl.attributes_for(:params_sorting)[:body][:question_type]
        @question.body[:question].should ==  FactoryGirl.attributes_for(:params_sorting)[:body][:question]
      end
      it "creates a new Question with params_sorting_answer_include_nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_sorting_answer_include_nil)
        }.to change(Question, :count).by(1)
        @question = Question.last
        @question.name.should == FactoryGirl.attributes_for(:params_sorting_answer_include_nil)[:question][:name]
        @question.body[:question_type].should == FactoryGirl.attributes_for(:params_sorting_answer_include_nil)[:body][:question_type]
        @question.body[:question].should ==  FactoryGirl.attributes_for(:params_sorting_answer_include_nil)[:body][:question]
      end

      it "creates a new Question with short answer" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_short)
        }.to change(Question, :count).by(1)
        @question = Question.last
        @question.name.should == FactoryGirl.attributes_for(:params_short)[:question][:name]
        @question.body[:question_type].should == FactoryGirl.attributes_for(:params_short)[:body][:question_type]
        @question.body[:question].should ==  FactoryGirl.attributes_for(:params_short)[:body][:question]
      end


      it "creates a new Question with long answer" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_long)
        }.to change(Question, :count).by(1)
        @question = Question.last
        @question.name.should == FactoryGirl.attributes_for(:params_long)[:question][:name]
        @question.body[:question_type].should == FactoryGirl.attributes_for(:params_long)[:body][:question_type]
        @question.body[:question].should ==  FactoryGirl.attributes_for(:params_long)[:body][:question]
      end

      it "redirects question after created with multiple choice" do
        post :create, FactoryGirl.attributes_for(:params_multiple_choice)
        response.should redirect_to(Question.last)
      end
      it "redirects question after created with multi choice partial" do
        post :create, FactoryGirl.attributes_for(:params_multiple_choice_with_partial)
        response.should redirect_to(Question.last)
      end
      it "redirects question after created with multiple answer" do
        post :create, FactoryGirl.attributes_for(:params_multiple_answer)
        response.should redirect_to(Question.last)
      end
      it "redirects question after created with params_multiple_answer_with_answer_text_nil" do
        post :create, FactoryGirl.attributes_for(:params_multiple_answer_with_answer_text_nil)
        response.should redirect_to(Question.last)
      end

      it "redirects question after created with matching" do
        post :create, FactoryGirl.attributes_for(:params_matching)
        response.should redirect_to(Question.last)
      end
      it "redirects question after created with sorting" do
        post :create, FactoryGirl.attributes_for(:params_sorting)
        response.should redirect_to(Question.last)
      end
      it "redirects question after created with short answer" do
        post :create, FactoryGirl.attributes_for(:params_short)
        response.should redirect_to(Question.last)
      end
      it "redirects question after created with long answer" do
        post :create, FactoryGirl.attributes_for(:params_long)
        response.should redirect_to(Question.last)
      end
    end

    describe "with invalid params" do
      it "do not create a new question with params multiple choice nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_multiple_choice_nil)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template" do
        post :create, FactoryGirl.attributes_for(:params_multiple_choice_nil)
        response.should render_template("new")
      end
      it "do not create a new question with params multiple choice question nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_multiple_choice_question_nil)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params multiple choice question nil" do
        post :create, FactoryGirl.attributes_for(:params_multiple_choice_question_nil)
        response.should render_template("new")
      end
      it "do not create a new question with params multiple choice number answer less than 2" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_multiple_choice_number_answer_less_than_2)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params multiple choice number answer less than 2" do
        post :create, FactoryGirl.attributes_for(:params_multiple_choice_number_answer_less_than_2)
        response.should render_template("new")
      end
      it "do not create a new question with params multiple choice not check correct" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_multiple_choice_not_check_correct)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params multiple choice not check correct" do
        post :create, FactoryGirl.attributes_for(:params_multiple_choice_not_check_correct)
        response.should render_template("new")
      end
      it "do not create a new question with params multiple choice answer not unique" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_multiple_choice_answer_not_unique)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params multiple choice answer not unique" do
        post :create, FactoryGirl.attributes_for(:params_multiple_choice_answer_not_unique)
        response.should render_template("new")
      end
      it "do not create a new question with params multiple choice correct answer nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_multiple_choice_correct_answer_nil)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params multiple choice correct answer nil" do
        post :create, FactoryGirl.attributes_for(:params_multiple_choice_correct_answer_nil)
        response.should render_template("new")
      end
      it "do not create a new question with params multiple answer nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_multiple_answer_nil)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params multiple answer nil" do
        post :create, FactoryGirl.attributes_for(:params_multiple_answer_nil)
        response.should render_template("new")
      end
      it "do not create a new question with params multiple answer body question nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_multiple_answer_with_body_question_nil)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params multiple answer body question nil" do
        post :create, FactoryGirl.attributes_for(:params_multiple_answer_with_body_question_nil)
        response.should render_template("new")
      end
      it "do not create a new question with params multiple answer body answer nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_multiple_answer_body_answer_nil)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params multiple answer body answer nil" do
        post :create, FactoryGirl.attributes_for(:params_multiple_answer_body_answer_nil)
        response.should render_template("new")
      end
      it "do not create a new question with params multiple answer body answer not unique nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_multiple_answer_body_answer_not_unique)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params multiple answer body answer not unique nil" do
        post :create, FactoryGirl.attributes_for(:params_multiple_answer_body_answer_not_unique)
        response.should render_template("new")
      end
      it "do not create a new question with params true false question nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_true_false_body_question_nil)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params true false question nil" do
        post :create, FactoryGirl.attributes_for(:params_true_false_body_question_nil)
        response.should render_template("new")
      end
      it "do not create a new question with params matching nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_matching_nil)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params matching nil" do
        post :create, FactoryGirl.attributes_for(:params_matching_nil)
        response.should render_template("new")
      end
      it "do not create a new question with params matching question nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_matching_body_question_nil)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params matching question nil" do
        post :create, FactoryGirl.attributes_for(:params_matching_body_question_nil)
        response.should render_template("new")
      end
      it "do not create a new question with params matching number stem less than 2" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_matching_number_stems_less_than_2)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params matching number stem less than 2" do
        post :create, FactoryGirl.attributes_for(:params_matching_number_stems_less_than_2)
        response.should render_template("new")
      end

      it "do not create a new question with params matching choice nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_matching_body_stems_not_matching_choice)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params matching choice nil" do
        post :create, FactoryGirl.attributes_for(:params_matching_body_stems_not_matching_choice)
        response.should render_template("new")
      end
      it "do not create a new question with params matching stem not unique" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_matching_body_stems_not_unique)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params matching stem not unique" do
        post :create, FactoryGirl.attributes_for(:params_matching_body_stems_not_unique)
        response.should render_template("new")
      end
      it "do not create a new question with params matching choice not unique" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_matching_body_choice_not_unique)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params matching choice not unique" do
        post :create, FactoryGirl.attributes_for(:params_matching_body_choice_not_unique)
        response.should render_template("new")
      end
      it "do not create a new question with params sorting nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_sorting_nil)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params sorting nil" do
        post :create, FactoryGirl.attributes_for(:params_sorting_nil)
        response.should render_template("new")
      end
      it "do not create a new question with params sorting question nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_sorting_body_question_nil)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params sorting question nil" do
        post :create, FactoryGirl.attributes_for(:params_sorting_body_question_nil)
        response.should render_template("new")
      end
      it "do not create a new question with params sorting answer less than 2" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_sorting_number_body_answer_less_than_2)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params sorting answer less than 2" do
        post :create, FactoryGirl.attributes_for(:params_sorting_number_body_answer_less_than_2)
        response.should render_template("new")
      end
      it "do not create a new question with params sorting answer nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_sorting_body_answer_nil)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params sorting answer nil" do
        post :create, FactoryGirl.attributes_for(:params_sorting_body_answer_nil)
        response.should render_template("new")
      end
      it "do not create a new question with params sorting answer not unique" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_sorting_body_answer_not_unique)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params sorting answer unique" do
        post :create, FactoryGirl.attributes_for(:params_sorting_body_answer_not_unique)
        response.should render_template("new")
      end
      it "do not create a new question with params short nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_short_nil)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params short nil" do
        post :create, FactoryGirl.attributes_for(:params_short_nil)
        response.should render_template("new")
      end
      it "do not create a new question with params short question nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_short_body_question_nil)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params short question nil" do
        post :create, FactoryGirl.attributes_for(:params_short_body_question_nil)
        response.should render_template("new")
      end
      it "do not create a new question with params short answer nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_short_body_answer_nil)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params short answer nil" do
        post :create, FactoryGirl.attributes_for(:params_short_body_answer_nil)
        response.should render_template("new")
      end
      it "do not create a new question with params long nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_long_nil)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params long nil" do
        post :create, FactoryGirl.attributes_for(:params_long_nil)
        response.should render_template("new")
      end
      it "do not create a new question with params long question nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_long_body_question_nil)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params long question nil" do
        post :create, FactoryGirl.attributes_for(:params_long_body_question_nil)
        response.should render_template("new")
      end
      it "do not create a new question with params long answer nil" do
        expect {
          post :create, FactoryGirl.attributes_for(:params_long_body_answer_nil)
        }.to change(Question, :count).by(0)
      end
      it "re-renders the 'new' template with params long answer nil" do
        post :create, FactoryGirl.attributes_for(:params_long_body_answer_nil)
        response.should render_template("new")
      end

    end
  end

  #
  describe "PUT update" do
    describe "with valid params" do
      it "redirects to the question with multi choice" do
        question = FactoryGirl.create(:question_multiple_choice)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_choice)[:question],:body => FactoryGirl.attributes_for(:params_multiple_choice)[:body] }
        response.should redirect_to(question)
      end
      it "update success with multi choice" do
        question = FactoryGirl.create(:question_multiple_choice)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_choice)[:question],:body => FactoryGirl.attributes_for(:params_multiple_choice)[:body] }
        @question = Question.find(question.id)
        @question.name.should == FactoryGirl.attributes_for(:params_multiple_choice)[:question][:name]
      end

      it "redirects to the question with multi choice have one answer, correct nil" do
        question = FactoryGirl.create(:question_multiple_choice)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_choice_with_answer_correct_nil)[:question],:body => FactoryGirl.attributes_for(:params_multiple_choice_with_answer_correct_nil)[:body] }
        response.should redirect_to(question)
      end
      it "update success with multi choice have one answer, correct nil" do
        question = FactoryGirl.create(:question_multiple_choice)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_choice_with_answer_correct_nil)[:question],:body => FactoryGirl.attributes_for(:params_multiple_choice_with_answer_correct_nil)[:body] }
        @question = Question.find(question.id)
        @question.name.should == FactoryGirl.attributes_for(:params_multiple_choice_with_answer_correct_nil)[:question][:name]
      end
      it "redirects to the question with multi choice have one answer nil" do
        question = FactoryGirl.create(:question_multiple_choice)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_choice_with_answer_include_nil)[:question],:body => FactoryGirl.attributes_for(:params_multiple_choice_with_answer_include_nil)[:body] }
        response.should redirect_to(question)
      end
      it "update success with multi choice have one answer nil" do
        question = FactoryGirl.create(:question_multiple_choice)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_choice_with_answer_include_nil)[:question],:body => FactoryGirl.attributes_for(:params_multiple_choice_with_answer_include_nil)[:body] }
        @question = Question.find(question.id)
        @question.name.should == FactoryGirl.attributes_for(:params_multiple_choice_with_answer_include_nil)[:question][:name]
      end
      it "redirects to the question with multi choice partial" do
        question = FactoryGirl.create(:question_multiple_choice)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_choice_with_partial)[:question],:body => FactoryGirl.attributes_for(:params_multiple_choice_with_partial)[:body] }
        response.should redirect_to(question)
      end
      it "update success with multi choice partial" do
        question = FactoryGirl.create(:question_multiple_choice)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_choice_with_partial)[:question],:body => FactoryGirl.attributes_for(:params_multiple_choice_with_partial)[:body] }
        @question = Question.find(question.id)
        @question.name.should == FactoryGirl.attributes_for(:params_multiple_choice_with_partial)[:question][:name]
      end
      it "redirects to the question with multi choice partial answer point nil" do
        question = FactoryGirl.create(:question_multiple_choice)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_choice_with_partial_with_answer_point_nil)[:question],:body => FactoryGirl.attributes_for(:params_multiple_choice_with_partial_with_answer_point_nil)[:body],:points => FactoryGirl.attributes_for(:params_multiple_choice_with_partial_with_answer_point_nil)[:points] }
        response.should redirect_to(question)
      end
      it "update success with multi choice partial answer point nil" do
        question = FactoryGirl.create(:question_multiple_choice)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_choice_with_partial_with_answer_point_nil)[:question],:body => FactoryGirl.attributes_for(:params_multiple_choice_with_partial_with_answer_point_nil)[:body] ,:points => FactoryGirl.attributes_for(:params_multiple_choice_with_partial_with_answer_point_nil)[:points]}
        @question = Question.find(question.id)
        @question.name.should == FactoryGirl.attributes_for(:params_multiple_choice_with_partial_with_answer_point_nil)[:question][:name]
      end

      it "update success with multi answer" do
        question = FactoryGirl.create(:question_multiple_answer)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_answer)[:question],:body => FactoryGirl.attributes_for(:params_multiple_answer)[:body] }
        @question = Question.find(question.id)
        @question.name.should == FactoryGirl.attributes_for(:params_multiple_answer)[:question][:name]
      end
      it "redirects to the question with multi answer" do
        question = FactoryGirl.create(:question_multiple_answer)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_answer)[:question],:body => FactoryGirl.attributes_for(:params_multiple_answer)[:body] }
        response.should redirect_to(question)
      end
      it "update success with multi answer answer text nil" do
        question = FactoryGirl.create(:question_multiple_answer)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_answer_with_answer_text_nil)[:question],:body => FactoryGirl.attributes_for(:params_multiple_answer_with_answer_text_nil)[:body] }
        @question = Question.find(question.id)
        @question.name.should == FactoryGirl.attributes_for(:params_multiple_answer_with_answer_text_nil)[:question][:name]
      end
      it "redirects to the question with multi answer answer text nil" do
        question = FactoryGirl.create(:question_multiple_answer)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_answer_with_answer_text_nil)[:question],:body => FactoryGirl.attributes_for(:params_multiple_answer_with_answer_text_nil)[:body] }
        response.should redirect_to(question)
      end
      it "update success with multi answer answer text, correct nil" do
        question = FactoryGirl.create(:question_multiple_answer)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_answer_with_answer_text_nil_correct_nil)[:question],:body => FactoryGirl.attributes_for(:params_multiple_answer_with_answer_text_nil_correct_nil)[:body] }
        @question = Question.find(question.id)
        @question.name.should == FactoryGirl.attributes_for(:params_multiple_answer_with_answer_text_nil_correct_nil)[:question][:name]
      end
      it "redirects to the question with multi answer answer text, correct nil" do
        question = FactoryGirl.create(:question_multiple_answer)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_answer_with_answer_text_nil_correct_nil)[:question],:body => FactoryGirl.attributes_for(:params_multiple_answer_with_answer_text_nil_correct_nil)[:body] }
        response.should redirect_to(question)
      end
      it "update success with true false" do
        question = FactoryGirl.create(:question_true_false)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_true_false)[:question],:body => FactoryGirl.attributes_for(:params_true_false)[:body] }
        @question = Question.find(question.id)
        @question.name.should == FactoryGirl.attributes_for(:params_true_false)[:question][:name]
      end
      it "redirects to the question with true false" do
        question = FactoryGirl.create(:question_true_false)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_true_false)[:question],:body => FactoryGirl.attributes_for(:params_true_false)[:body] }
        response.should redirect_to(question)
      end
      it "update success with question_matching" do
        question = FactoryGirl.create(:question_matching)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_matching)[:question],:body => FactoryGirl.attributes_for(:params_matching)[:body] }
        @question = Question.find(question.id)
        @question.name.should == FactoryGirl.attributes_for(:params_matching)[:question][:name]
      end
      it "redirects to the question with question_matching" do
        question = FactoryGirl.create(:question_matching)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_matching)[:question],:body => FactoryGirl.attributes_for(:params_matching)[:body] }
        response.should redirect_to(question)
      end
      it "update success with params_matching_with_stems_choice_nil" do
        question = FactoryGirl.create(:question_matching)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_matching_with_stems_choice_nil)[:question],:body => FactoryGirl.attributes_for(:params_matching_with_stems_choice_nil)[:body] }
        @question = Question.find(question.id)
        @question.name.should == FactoryGirl.attributes_for(:params_matching_with_stems_choice_nil)[:question][:name]
      end
      it "redirects to the question with params_matching_with_stems_choice_nil" do
        question = FactoryGirl.create(:question_matching)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_matching_with_stems_choice_nil)[:question],:body => FactoryGirl.attributes_for(:params_matching_with_stems_choice_nil)[:body] }
        response.should redirect_to(question)
      end

      it "update success with question_sorting" do
        question = FactoryGirl.create(:question_sorting)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_sorting_to_edit)[:question],:body => FactoryGirl.attributes_for(:params_sorting_to_edit)[:body] }
        @question = Question.find(question.id)
        @question.name.should == FactoryGirl.attributes_for(:params_sorting_to_edit)[:question][:name]
      end
      it "redirects to the question with question_sorting" do
        question = FactoryGirl.create(:question_sorting)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_sorting_to_edit)[:question],:body => FactoryGirl.attributes_for(:params_sorting_to_edit)[:body] }
        response.should redirect_to(question)
      end
      it "update success with params_sorting_answer_include_nil" do
        question = FactoryGirl.create(:question_sorting)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_sorting_answer_include_nil_to_edit)[:question],:body => FactoryGirl.attributes_for(:params_sorting_answer_include_nil_to_edit)[:body] }
        @question = Question.find(question.id)
        @question.name.should == FactoryGirl.attributes_for(:params_sorting_answer_include_nil_to_edit)[:question][:name]
      end
      it "redirects to the question with params_sorting_answer_include_nil" do
        question = FactoryGirl.create(:question_sorting)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_sorting_answer_include_nil_to_edit)[:question],:body => FactoryGirl.attributes_for(:params_sorting_answer_include_nil_to_edit)[:body] }
        response.should redirect_to(question)
      end
      it "update success with question_short_answer" do
        question = FactoryGirl.create(:question_short_answer)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_short)[:question],:body => FactoryGirl.attributes_for(:params_short)[:body] }
        @question = Question.find(question.id)
        @question.name.should == FactoryGirl.attributes_for(:params_short)[:question][:name]
      end
      it "redirects to the question with question_short_answer" do
        question = FactoryGirl.create(:question_short_answer)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_short)[:question],:body => FactoryGirl.attributes_for(:params_short  )[:body] }
        response.should redirect_to(question)
      end
      it "update success with question_long_answer" do
        question = FactoryGirl.create(:question_long_answer)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_long)[:question],:body => FactoryGirl.attributes_for(:params_long)[:body] }
        @question = Question.find(question.id)
        @question.name.should == FactoryGirl.attributes_for(:params_long)[:question][:name]
      end
      it "redirects to the question with question_long_answer" do
        question = FactoryGirl.create(:question_long_answer)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_long)[:question],:body => FactoryGirl.attributes_for(:params_long)[:body] }
        response.should redirect_to(question)
      end
    end

    describe "with invalid params" do
      it "redirects to the edit page with multi choice" do
        question = FactoryGirl.create(:question_multiple_choice)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_choice_nil)[:question],:body => FactoryGirl.attributes_for(:params_multiple_choice_nil)[:body] }
        response.should render_template :edit
      end
      it "redirects to the edit page with params_multiple_choice_question_nil" do
        question = FactoryGirl.create(:question_multiple_choice)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_choice_question_nil)[:question],:body => FactoryGirl.attributes_for(:params_multiple_choice_question_nil)[:body] }
        response.should render_template :edit
      end
      it "redirects to the edit page with params_multiple_choice_number_answer_less_than_2" do
        question = FactoryGirl.create(:question_multiple_choice)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_choice_number_answer_less_than_2)[:question],:body => FactoryGirl.attributes_for(:params_multiple_choice_number_answer_less_than_2)[:body] }
        response.should render_template :edit
      end
      it "redirects to the edit page with params_multiple_choice_answer_not_unique" do
        question = FactoryGirl.create(:question_multiple_choice)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_choice_answer_not_unique)[:question],:body => FactoryGirl.attributes_for(:params_multiple_choice_answer_not_unique)[:body] }
        response.should render_template :edit
      end
      it "redirects to the edit page with params_multiple_choice_correct_answer_nil" do
        question = FactoryGirl.create(:question_multiple_choice)
        put :update, {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_choice_correct_answer_nil)[:question],:body => FactoryGirl.attributes_for(:params_multiple_choice_correct_answer_nil)[:body] }
        response.should render_template :edit
      end
      it "redirects to the question with multi answer" do
        question = FactoryGirl.create(:question_multiple_answer)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_answer_nil)[:question],:body => FactoryGirl.attributes_for(:params_multiple_answer_nil)[:body] }
        response.should render_template :new
      end
      it "redirects to the question with params_multiple_answer_with_body_question_nil" do
        question = FactoryGirl.create(:question_multiple_answer)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_answer_with_body_question_nil)[:question],:body => FactoryGirl.attributes_for(:params_multiple_answer_with_body_question_nil)[:body] }
        response.should render_template :new
      end
      it "redirects to the question with params_multiple_answer_body_answer_nil" do
        question = FactoryGirl.create(:question_multiple_answer)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_answer_body_answer_nil)[:question],:body => FactoryGirl.attributes_for(:params_multiple_answer_body_answer_nil)[:body] }
        response.should render_template :new
      end
      it "redirects to the question with params_multiple_answer_body_answer_not_unique" do
        question = FactoryGirl.create(:question_multiple_answer)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_multiple_answer_body_answer_not_unique)[:question],:body => FactoryGirl.attributes_for(:params_multiple_answer_body_answer_not_unique)[:body] }
        response.should render_template :new
      end
      it "redirects to the question with params_true_false_body_question_nil" do
        question = FactoryGirl.create(:question_true_false)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_true_false_body_question_nil)[:question],:body => FactoryGirl.attributes_for(:params_true_false_body_question_nil)[:body] }
        response.should render_template :edit
      end
      it "redirects to the question with params_matching_nil" do
        question = FactoryGirl.create(:question_matching)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_matching_nil)[:question],:body => FactoryGirl.attributes_for(:params_matching_nil)[:body] }
        response.should render_template :new
      end
      it "redirects to the question with params_matching_body_question_nil" do
        question = FactoryGirl.create(:question_matching)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_matching_body_question_nil)[:question],:body => FactoryGirl.attributes_for(:params_matching_body_question_nil)[:body] }
        response.should render_template :new
      end
      it "redirects to the question with params_matching_number_stems_less_than_2" do
        question = FactoryGirl.create(:question_matching)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_matching_number_stems_less_than_2)[:question],:body => FactoryGirl.attributes_for(:params_matching_number_stems_less_than_2)[:body] }
        response.should render_template :new
      end
      it "redirects to the question with params_matching_body_stems_not_matching_choice" do
        question = FactoryGirl.create(:question_matching)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_matching_body_stems_not_matching_choice)[:question],:body => FactoryGirl.attributes_for(:params_matching_body_stems_not_matching_choice)[:body] }
        response.should render_template :new
      end
      it "redirects to the question with params_matching_body_stems_not_unique" do
        question = FactoryGirl.create(:question_matching)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_matching_body_stems_not_unique)[:question],:body => FactoryGirl.attributes_for(:params_matching_body_stems_not_unique)[:body] }
        response.should render_template :edit
      end
      it "redirects to the question with params_matching_body_choice_not_unique" do
        question = FactoryGirl.create(:question_matching)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_matching_body_choice_not_unique)[:question],:body => FactoryGirl.attributes_for(:params_matching_body_choice_not_unique)[:body] }
        response.should render_template :edit
      end
      it "redirects to the question with params_sorting_nil" do
        question = FactoryGirl.create(:question_sorting)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_sorting_nil)[:question],:body => FactoryGirl.attributes_for(:params_sorting_nil)[:body] }
        response.should render_template :new
      end
      it "redirects to the question with params_sorting_body_question_nil" do
        question = FactoryGirl.create(:question_sorting)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_sorting_body_question_nil_to_edit)[:question],:body => FactoryGirl.attributes_for(:params_sorting_body_question_nil_to_edit)[:body] }
        response.should render_template :new
      end
      it "redirects to the question with params_sorting_number_body_answer_less_than_2" do
        question = FactoryGirl.create(:question_sorting)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_sorting_number_body_answer_less_than_2_to_edit)[:question],:body => FactoryGirl.attributes_for(:params_sorting_number_body_answer_less_than_2_to_edit)[:body] }
        response.should render_template :new
      end
      it "redirects to the question with params_sorting_body_answer_nil" do
        question = FactoryGirl.create(:question_sorting)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_sorting_body_answer_nil_to_edit)[:question],:body => FactoryGirl.attributes_for(:params_sorting_body_answer_nil_to_edit)[:body] }
        response.should render_template :new
      end
      it "redirects to the question with params_sorting_body_answer_not_unique" do
        question = FactoryGirl.create(:question_sorting)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_sorting_body_answer_not_unique_to_edit)[:question],:body => FactoryGirl.attributes_for(:params_sorting_body_answer_not_unique_to_edit)[:body] }
        response.should render_template :new
      end
      it "redirects to the question with params_short_nil" do
        question = FactoryGirl.create(:question_short_answer)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_short_nil)[:question],:body => FactoryGirl.attributes_for(:params_short_nil  )[:body] }
        response.should render_template :edit
      end
      it "redirects to the question with params_short_body_question_nil" do
        question = FactoryGirl.create(:question_short_answer)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_short_body_question_nil)[:question],:body => FactoryGirl.attributes_for(:params_short_body_question_nil)[:body] }
        response.should render_template :edit
      end
      it "redirects to the question with params_short_body_answer_nil" do
        question = FactoryGirl.create(:question_short_answer)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_short_body_answer_nil)[:question],:body => FactoryGirl.attributes_for(:params_short_body_answer_nil)[:body] }
        response.should render_template :new
      end
      it "redirects to the question with params_long_nil" do
        question = FactoryGirl.create(:question_long_answer)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_long_nil)[:question],:body => FactoryGirl.attributes_for(:params_long_nil)[:body] }
        response.should render_template :edit
      end
      it "redirects to the question with params_long_body_question_nil" do
        question = FactoryGirl.create(:question_long_answer)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_long_body_question_nil)[:question],:body => FactoryGirl.attributes_for(:params_long_body_question_nil)[:body] }
        response.should render_template :edit
      end
      it "redirects to the question with params_long_body_answer_nil" do
        question = FactoryGirl.create(:question_long_answer)
        put :update,  {:id => question.id, :question => FactoryGirl.attributes_for(:params_long_body_answer_nil)[:question],:body => FactoryGirl.attributes_for(:params_long_body_answer_nil)[:body] }
        response.should render_template :new
      end

    end

  end
  #
  #describe "DELETE destroy" do
  #  it "destroys the requested question" do
  #    question = FactoryGirl.create(:question_multi_choice)
  #    expect {
  #      delete :destroy, {:id => question.id}
  #    }.to change(Question, :count).by(-1)
  #  end
  #
  #  it "redirects to the questions list" do
  #    question = FactoryGirl.create(:question_multi_choice)
  #    delete :destroy, {:id => question.id}
  #    response.should redirect_to(questions_url)
  #  end
  #end


end
