require 'spec_helper'
describe ContentsController do

  def content_valid_attributes
    { "name"=>"my name ", "body"=>"AAAA", "draft"=>1,"tag_list" => "[\"tag1\",\"tag2\"]"}
  end
  def content_update_valid_attributes
    { "name"=>"my name update ", "body"=>"mytext", "draft"=>"1","tag_list" => "[\"tag1_update\",\"tag2_update\"]"}
    end
  def content_invalid_attributes
    {"name"=>"", "body"=>"", "draft"=>"1","tag_list" =>"[]"}
  end

  before :each do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @current_user = FactoryGirl.create(:user)
    sign_in @current_user
    #controller.stub(:authenticate_user!).and_return true
  end

  describe "GET index" do
    before :each do
      content = FactoryGirl.create(:content)
    end
    it "list content" do
      get :index
      response.should render_template :index
    end
    it "should respond with success" do
      get :index
      response.should be_success
    end
    it "assigns all contents as @contents" do
      content = FactoryGirl.create(:content)
      get :index
      @content = Content.all
      @content.should include(content)
    end
  end

  describe "GET show" do
    it "assigns the requested content as @content" do
      content = FactoryGirl.create(:content)
      get :show, {:id => content.id}
      assigns(:content).should eq(content)
    end
  end

  describe "GET new" do
    it "assigns a new content as @content" do
      get :new, {}
      assigns(:content).should be_a_new(Content)
    end
    it "render new page" do
      get :new, {}
      response.should render_template("new")
    end
  end

  describe "GET edit" do
    it "assigns the requested content as @content" do
      content = FactoryGirl.create(:content)
      get :edit, {:id => content.id}
      response.should render_template("edit")
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Content" do
        expect {
          post :create, {:content => content_valid_attributes}
        }.to change(Content, :count).by(1)
      end

      it "assigns a newly created content as @content" do
        post :create, {:content => content_valid_attributes}
        assigns(:content).should be_a(Content)
        assigns(:content).should be_persisted
      end

      it "redirects to the created content" do
        post :create, {:content => content_valid_attributes}
        response.should redirect_to(Content.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved content as @content" do
        #Content.any_instance.stub(:save).and_return(false)
        post :create, {:content => content_invalid_attributes}
        assigns(:content).should be_a_new(Content)
      end

      it "re-renders the 'new' template" do
        #Content.any_instance.stub(:save).and_return(false)
        post :create, {:content => content_invalid_attributes }
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do

      it "assigns the requested content as @content" do
        content = FactoryGirl.create(:content)
        put :update, {:id => content.id, :content => content_update_valid_attributes}
        assigns(:content).should eq(content)
      end

      it "redirects to the content" do
        content = FactoryGirl.create(:content)
        put :update, {:id => content.id, :content => content_update_valid_attributes}
        response.should redirect_to(content)
      end
    end

    describe "with invalid params" do
      it "assigns the content as @content" do
        content = FactoryGirl.create(:content)
        put :update, {:id => content.id, :content => content_invalid_attributes}
        assigns(:content).should eq(content)
      end

      it "re-renders the 'edit' template" do
        content = FactoryGirl.create(:content)
        put :update, {:id => content.id, :content => content_invalid_attributes}
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested content" do
      content = FactoryGirl.create(:content)
      expect {
        delete :destroy, {:id => content.id}
      }.to change(Content, :count).by(-1)
    end

    it "redirects to the contents list" do
      content = FactoryGirl.create(:content)
      delete :destroy, {:id => content.id}
      response.should redirect_to(contents_url)
    end
  end

end
