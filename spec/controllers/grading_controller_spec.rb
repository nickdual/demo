require 'spec_helper'

describe GradingsController do
  def import_data
    FactoryGirl.create(:question_multiple_choice_with_partial)
    FactoryGirl.create(:question_multiple_answer)
    FactoryGirl.create(:question_true_false)
    FactoryGirl.create(:question_matching)
    FactoryGirl.create(:question_sorting)
    FactoryGirl.create(:question_short)
    FactoryGirl.create(:question_short_answer)
    FactoryGirl.create(:question_long_answer)

    FactoryGirl.create(:assignment_library_with_list_question)
    FactoryGirl.create(:student2)
    FactoryGirl.create(:student3)
    FactoryGirl.create(:student4)
    FactoryGirl.create(:student5)
    FactoryGirl.create(:assignment)
  end
  before :each do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @current_user = FactoryGirl.create(:user)
    sign_in @current_user
    controller.stub(:authenticate_user!).and_return true
  end

  describe "GET 'index'" do
    it "render index page" do
      get 'index'
      response.should render_template :index
    end
  end

  describe "GET 'edit'" do
    it "returns http success" do
      get 'edit'
      response.should be_success
    end
  end

  describe "GET 'show'" do
    before(:each) do
      @grading = FacetoryGirl.create()
    end
    it "render show page" do
      get 'show'
      response.should render_template :show
      end
    it "returns http success" do
      get 'show', :id => 1

    end
  end
  #
  #describe "GET 'new'" do
  #  it "returns http success" do
  #    get 'new'
  #    response.should be_success
  #  end
  #end

end
