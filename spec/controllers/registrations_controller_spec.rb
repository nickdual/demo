require 'spec_helper'

describe RegistrationsController do
  before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:user]
  end
  describe "sign up" do
    it "should be successful with role teacher" do
      post :create, {:role => "Teacher", :user => {"name"=>"test", "email"=>"abc123@gmail.com", "password"=>"123456", "password_confirmation"=>"123456"}}
      @user = User.find_by_email('abc123@gmail.com')
      @user.role_names.to_s.should == 'Teacher'
    end

    it "should be successful with role student" do
      post :create, {:role => "Student", :user => {"name"=>"test", "email"=>"abc123@gmail.com", "password"=>"123456", "password_confirmation"=>"123456"}}
      @user = User.find_by_email('abc123@gmail.com')
      @user.role_names.to_s.should == 'Student'
    end
    it "should not be successful with already email" do
      FactoryGirl.create(:user)
      post :create, {:user => {"name"=>"test", "email"=>"abc@gmail.com", "password"=>"123456", "password_confirmation"=>"123456"}}
      response.should render_template :template => "devise/registrations/new.html.erb", :layout => "layouts/home"
    end
    it "should not be successful with email blank" do
      FactoryGirl.create(:user)
      post :create, {:user => {"name"=>"test", "email"=>"", "password"=>"123456", "password_confirmation"=>"123456"}}
      response.should render_template :template => "devise/registrations/new.html.erb", :layout => "layouts/home"
    end
    it "should not be successful with password blank " do
      FactoryGirl.create(:user)
      post :create, {:user => {"name"=>"test", "email"=>"abc@gmail.com", "password"=>"", "password_confirmation"=>"123456"}}
      response.should render_template :template => "devise/registrations/new.html.erb", :layout => "layouts/home"
    end
    it "should not be successful with password least 6 characters" do
      FactoryGirl.create(:user)
      post :create, {:user => {"name"=>"test", "email"=>"abc@gmail.com", "password"=>"12345", "password_confirmation"=>"12345"}}
      response.should render_template :template => "devise/registrations/new.html.erb", :layout => "layouts/home"
    end

    it "should not be successful with password doesn't match confirmation" do
      FactoryGirl.create(:user)
      post :create, {:user => {"name"=>"test", "email"=>"abc@gmail.com", "password"=>"12345", "password_confirmation"=>"123456"}}
      response.should render_template :template => "devise/registrations/new.html.erb", :layout => "layouts/home"
    end
    it "should response to page home after sign successful  " do
      post :create, {:role => "Teacher", :user => {"name"=>"test", "email"=>"abc123@gmail.com", "password"=>"123456", "password_confirmation"=>"123456"}}
      response.should redirect_to("/")
    end
  end

end
