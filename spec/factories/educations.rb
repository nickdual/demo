# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :education do
    id 1
    name "Graduat Degree or Higher"
    code 10
  end
  factory :education2 , :class => 'Education' do
    id 2
    name "College Graduate"
    code 11
  end
  factory :education3 , :class => 'Education' do
    id 3
    name "Some College or Associate's Degree"
    code 12
  end
  factory :education4 , :class => 'Education' do
    id 4
    name "High School Graduate"
    code 13
  end
  factory :education5 , :class => 'Education' do
    id 5
    name "Not a High School Graduate"
    code 14
  end
end

