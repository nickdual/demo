# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :school_class do
    id 1
    teacher_id 1
    institution_id 1
    year 1
    room_id 1
  end
end
