# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :city do
    id 103800
    country_id 234
    geonames_id 404625
    name "Bay Minette"
    asciiname nil
    alternate_name nil
    latitude 30.88296
    longitude -87.77305
    country_iso_code_two_letters "US"
    geonames_timezone_id 0
  end
end
