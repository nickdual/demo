# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :game_asset do
    name "MyString"
    price 1
    description "MyText"
    image_url "MyString"
  end
end
