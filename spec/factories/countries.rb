# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :country ,:class => 'Country' do
    id 234
    iso_code_two_letter "US"
    iso_code_three_letter "USA"
    iso_number 840
    name "United States"
    capital "Washington"
    continent "NA"
    geonames_id 6252001
  end
end
