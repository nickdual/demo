# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :race do
    name "MyString"
    comment "MyString"
    code 1
  end
end
