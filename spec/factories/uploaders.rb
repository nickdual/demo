# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :uploader do
    filename "MyString"
    description "MyString"
  end
end
