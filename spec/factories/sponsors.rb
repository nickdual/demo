# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :sponsor do
    id 1
    company "MyString"
    firstname "MyString"
    lastname "MyString"
    street "MyString"
    city_id 103800
    state "MyString"
    postalcode "MyString"
    phone "MyString"
    email "abcabc@gmail.com"
    amount "9.99"
  end
end
