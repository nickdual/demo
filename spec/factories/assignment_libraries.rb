# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :assignment_library do
    id 1
    name "assignment library"
    description "description to test"
  end

  factory :assignment_library_with_list_question, :class => AssignmentLibrary do
    id 1
    name "assignment library 1"
    description "description to test"
    question_ids ["1", "2", "3", "4", "", "5", "6", "7", "8"]
  end


  factory :assignment_add_question, :class => Hash do
    id 1
    list_question_ids ["1", "2", "3", "4", "", "5", "6", "7", "8", "9"]
  end
  factory :assignment_destroy_question, :class => Hash do
    id 1
    list ["1", "2", "3"]
  end
  #invaild
  factory :assignment_library_name_nil ,:class => AssignmentLibrary do
    id 2
    name ""
    description "description to test"
  end

end
