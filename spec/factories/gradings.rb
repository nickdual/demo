# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :gradings, :class => "Grading" do |f|
    f.body ""
    f.assignment_id "1"
    f.user_id "1"
  end
end
