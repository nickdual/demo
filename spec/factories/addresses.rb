# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :address do
    user_id 1
    street "MyString"
    street2 "MyString"
    city_id 1
    state_id 1
    postal_code 1
    comment "MyString"
  end
end
