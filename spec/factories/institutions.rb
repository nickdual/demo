# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :institution do
    name "My institution"
    logo "MyString"
    street "MyString"
    street2 "MyString"
    city_id 1
    state_id 1
    postal_code "MyString"
    comment "MyString"
  end
end
