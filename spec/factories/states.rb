# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :state do
    name_short "MyString"
    name "MyString"
  end
end
