# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :grade_system do
    id 1
    name "grade system name"
    settings "A+"
    end
  factory :grade_system2, :class => "GradeSystem" do
    id 2
    name "grade system name"
    settings "B+"
  end
end
