# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :badge do
    name "MyString"
    description "MyText"
    image_url "MyString"
  end
end
