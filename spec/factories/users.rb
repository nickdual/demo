# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    id 10
    name 'Test User'
    email 'abc@gmail.com'
    password '123456'
    password_confirmation '123456'
    created_at "2013-01-11 07:10:04"
    updated_at "2013-01-11 07:10:04"
  end
  factory :user2,:class => "User" do
    id 11
    name 'Test User 2'
    email 'aaaa@gmail.com'
    password '123456'
    password_confirmation '123456'
    created_at "2013-01-11 07:10:04"
    updated_at "2013-01-11 07:10:04"
  end
  factory :user3,:class => "User" do
    id 12
    name 'Test User 3'
    email 'abbb@gmail.com'
    password '123456'
    password_confirmation '123456'
    created_at "2013-01-11 07:10:04"
    updated_at "2013-01-11 07:10:04"
  end
  factory :user4,:class => "User" do
    id 13
    name 'Test User 4'
    email 'accc@gmail.com'
    password '123456'
    password_confirmation '123456'
    created_at "2013-01-11 07:10:04"
    updated_at "2013-01-11 07:10:04"
  end

  factory :user5,:class => "User" do
    id 14
    name 'Test User 5'
    email 'addd@gmail.com'
    password '123456'
    password_confirmation '123456'
    created_at "2013-01-11 07:10:04"
    updated_at "2013-01-11 07:10:04"
  end

  factory :student2,:class => "User" do
    id 2
    name 'student2'
    email 'student2@gmail.com'
    password '123456'
    password_confirmation '123456'
    created_at "2013-01-11 07:10:04"
    updated_at "2013-01-11 07:10:04"
  end
  factory :student3,:class => "User" do
    id 3
    name 'student3'
    email 'student3@gmail.com'
    password '123456'
    password_confirmation '123456'
    created_at "2013-01-11 07:10:04"
    updated_at "2013-01-11 07:10:04"
  end
  factory :student4,:class => "User" do
    id 4
    name 'student4'
    email 'student4@gmail.com'
    password '123456'
    password_confirmation '123456'
    created_at "2013-01-11 07:10:04"
    updated_at "2013-01-11 07:10:04"
  end
  factory :student5,:class => "User" do
    id 5
    name 'student5'
    email 'student5@gmail.com'
    password '123456'
    password_confirmation '123456'
    created_at "2013-01-11 07:10:04"
    updated_at "2013-01-11 07:10:04"
  end


end