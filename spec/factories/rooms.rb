# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :room do
    name "My room"
    location "MyString"
    institution_id 1
    descritption "MyString"
  end
end
