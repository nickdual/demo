# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do

  factory :question_multiple_choice ,:class => 'Question' do |f|
    f.id 1
    f.name "multiple choice"
    f.user_id 1
    f.body({:question_type=>"Multiple Choice", :answer=>["7", "8", "5", "9"], :point=>2, :correct=>2, :question=>"2 + 3 = ? ", :index=>[0, 1, 2, 3], :choices_random_order=>"true", :questions_random_order=>"true"})

  end

  factory :question_multiple_choice_with_partial ,:class => 'Question' do |f|
    f.id 2
    f.name "multiple answer"
    f.user_id 1
    f.body({:question_type=>"Multiple Choice", :answer=>["{1}", "{3,5}", "{3,5,12}", "{3,5,4,7}"], :point=>0, :partial=>"true", :question=>"E = { 1, 3,4,7,0,5} tap hop con", :index=>[0, 1, 2, 3], :points=>[1, 2, 0, 4], :choices_random_order=>"true", :questions_random_order=>"true"})
  end

  factory :question_multiple_answer ,:class => 'Question' do |f|
    f.id 3
    f.name "multiple answer"
    f.user_id 1
    f.body({:question_type=>"Multiple Answer", :answer=>["5", "4", "3", "7", "8"], :point=>2, :correct=>[0, 3, 4], :question=>"So nao lon hon so  5 ", :index=>[0, 1, 2, 3, 4], :choices_random_order=>"true", :questions_random_order=>"true"})
  end

  factory :question_true_false ,:class => 'Question' do |f|
    f.id 4
    f.name "True false"
    f.user_id 1
    f.body({:question_type=>"True/False", :answer=>"false", :point=>2, :question=>"15 * 6 = 70", :questions_random_order=>"true"})
  end

  factory :question_matching ,:class => 'Question' do |f|
    f.id 5
    f.name  "Matching"
    f.user_id 1
    f.body({:question_type=>"Matching", :stems=>[{"3"=>"So mot"}, {"6"=>"So muoi"}, {"7"=>"so chin"}, {"8"=>"So bay"}], :choice=>[{"2"=>"1"}, {"1"=>"10"}, {"6"=>"9"}, {"4"=>"7"}], :point=>2, :question=>"Chu so - So", :correct=>["3-2", "6-1", "7-6", "8-4"], :index=>[0, 1, 2, 3], :rand_stems=>"true", :rand_choices=>"random", :questions_random_order=>"true"})
  end

  factory :question_sorting ,:class => 'Question' do |f|
    f.id 6
    f.name "sorting aphabet"
    f.user_id 1
    f.body({:question_type => "Sorting", :answer => [{"7"=>"A"}, {"2"=>"B"}, {"4"=>"C"}, {"3"=>"D"}], :point=>2, :question=>"Sap sap theo thu tu aphabet", :correct=>"7-2-4-3", :questions_random_order=>"true"})
  end

  factory :question_short_answer ,:class => 'Question' do |f|
    f.id 7
    f.name "Short answer"

    f.user_id 1
    f.body({:question_type=>"Short Answer", :answer=>"99", :point=>2, :question=>"So lon nhat co hai chu so ? ", :questions_random_order=>"true"})
  end

  factory :question_long_answer ,:class => 'Question' do |f|
    f.id 8
    f.name "Long answer"
    f.user_id 1
    f.body({:question_type=>"Long Answer", :question=>"So nho nhat co ba chu so ? ", :point=>3, :answer=>"100", :questions_random_order=>"true"})
  end

  factory :params_multiple_choice, :class => Hash do |f|

    f.question({:name => "multiple choice "})
    f.body({"question_type"=>"Multiple Choice", "point"=>"2", "question"=>"100 - 77 = ? ", "answer"=>["77", "45", "33", "23", "13"], "correct"=>"3", "choices_random_order"=>"true", "questions_random_order"=>"true"}.symbolize_keys)

  end
  factory :params_multiple_choice_with_answer_correct_nil, :class => Hash do |f|
    f.question({:name => "multiple choice"})
    f.body({"question_type"=>"Multiple Choice", "point"=>"2", "question"=>"100 - 77 = ? ", "answer"=>["77", "45", "33", "3", ""], "correct"=>"4", "choices_random_order"=>"true", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_multiple_choice_with_answer_include_nil, :class => Hash do |f|
    f.question({:name => "multiple choice"})
    f.body({"question_type"=>"Multiple Choice", "point"=>"2", "question"=>"100 - 77 = ? ", "answer"=>["77", "45", "33", "", "3"], "correct"=>"3", "choices_random_order"=>"true", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_multiple_choice_with_partial, :class => Hash do |f|
    f.question({:name => " Multiple choice Partial or extra credit"})
    f.body({"question_type"=>"Multiple Choice", "point"=>"2", "partial"=>"true", "question"=>"E = { 1, 2, 3, 4, 7,9} , chon tap hop con ? ", "answer"=>["{1,2 }", "{1,2,3}", "{1,2,3,4}", "{1,2,3,7,9}", "{5,6,7}"], "choices_random_order"=>"true", "questions_random_order"=>"true"}.symbolize_keys)
    f.points ["1", "2", "3", "4", "0"]
  end

  factory :params_multiple_choice_with_partial_with_answer_point_nil, :class => Hash do |f|
    f.question({:name => " Multiple choicePartial or extra credit"})
    f.body({"question_type"=>"Multiple Choice", "point"=>"2", "partial"=>"true", "question"=>"E = { 1, 2, 3, 4, 7,9} , chon tap hop con ? ", "answer"=>["{1,2 }", "{1,2,3}", "", "{1,2,3,7,9}", "{5,6,7}"], "choices_random_order"=>"true", "questions_random_order"=>"true"}.symbolize_keys)
    f.points ["1", "2", "3", "4", "0"]
  end

  factory :params_multiple_answer, :class => Hash do |f|
    f.question({:name => "multiple answer"})
    f.body({"question_type"=>"Multiple Answer", "point"=>"2", "question"=>"so nao chia het cho so 3", "answer"=>[{"text"=>"9", "correct"=>"0"}, {"text"=>"12", "correct"=>"1"}, {"text"=>"11"}, {"text"=>"7"}, {"text"=>"6", "correct"=>"4"}], "choices_random_order"=>"true", "questions_random_order"=>"true"}.symbolize_keys)
  end

  factory :params_multiple_answer_with_answer_text_nil, :class => Hash do |f|
    f.question({:name => "multiple answer"})
    f.body({"question_type"=>"Multiple Answer", "point"=>"2", "question"=>"so nao chia het cho so 3", "answer"=>[{"text"=>"", "correct"=>"0"}, {"text"=>"12", "correct"=>"1"}, {"text"=>"11"}, {"text"=>"7"}, {"text"=>"6", "correct"=>"4"}], "choices_random_order"=>"true", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_multiple_answer_with_answer_text_nil_correct_nil, :class => Hash do |f|
    f.question({:name => "multiple answer"})
    f.body({"question_type"=>"Multiple Answer", "point"=>"2", "question"=>"so nao chia het cho so 3", "answer"=>[{"text"=>"", "correct"=>""}, {"text"=>"12", "correct"=>"1"}, {"text"=>"11"}, {"text"=>"7"}, {"text"=>"6", "correct"=>"4"}], "choices_random_order"=>"true", "questions_random_order"=>"true"}.symbolize_keys)
  end

  factory :params_true_false, :class => Hash do |f|
    f.question({:name => "True fales"})
    f.body({"question_type"=>"True/False", "point"=>"2", "question"=>"9 > 11 ? ", "answer"=>"false", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_matching, :class => Hash do |f|
    f.question({:name => "Matching"})
    f.body({"question_type"=>"Matching", "point"=>"2", "question"=>"Chu hoa - chu thuong", "answer"=>[{"stems"=>"A", "choice"=>"a"}, {"stems"=>"B", "choice"=>"b"}, {"stems"=>"C", "choice"=>"c"}, {"stems"=>"D", "choice"=>"d"}, {"stems"=>"E", "choice"=>"e"}], "stems_random_order"=>"true", "choices_random_order"=>"random", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_matching_with_stems_choice_nil, :class => Hash do |f|
    f.question({:name => "Matching"})
    f.body({"question_type"=>"Matching", "point"=>"2", "question"=>"Chu hoa - chu thuong", "answer"=>[{"stems"=>"", "choice"=>""}, {"stems"=>"B", "choice"=>"b"}, {"stems"=>"C", "choice"=>"c"}, {"stems"=>"D", "choice"=>"d"}, {"stems"=>"E", "choice"=>"e"}], "stems_random_order"=>"true", "choices_random_order"=>"random", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_sorting, :class => Hash do |f|
    f.question({:name => "Sorting"})
    f.body({"question_type"=>"Sorting", "point"=>"2", "question"=>"Aphabet", "answer"=>["A", "B", "C", "D", "E"], "questions_random_order"=>"true"}.symbolize_keys)
  end

  factory :params_sorting_to_edit, :class => Hash do |f|
    f.question({:name => "Sorting"})
    f.body({"question_type"=>"Sorting", "point"=>"2", "question"=>"Aphabet", "answer"=>[{"text"=>"A", "index"=>"1"}, {"text"=>"B", "index"=>"2"}, {"text"=>"C", "index"=>"3"}, {"text"=>"D", "index"=>"6"}, {"text"=>"E", "index"=>"4"}], "questions_random_order"=>"true"}.symbolize_keys)
  end


  factory :params_sorting_answer_include_nil, :class => Hash do |f|
    f.question({:name => "Sorting"})
    f.body({"question_type"=>"Sorting", "point"=>"2", "question"=>"Aphabet", "answer"=>["A", "B", "C", "", "E"], "questions_random_order"=>"true"}.symbolize_keys)
  end

factory :params_sorting_answer_include_nil_to_edit, :class => Hash do |f|
    f.question({:name => "Sorting"})
    f.body({"question_type"=>"Sorting", "point"=>"2", "question"=>"Aphabet", "answer"=>[{"text"=>"A", "index"=>"1"}, {"text"=>"B", "index"=>"2"}, {"text"=>"C", "index"=>"3"}, {"text"=>"", "index"=>"6"}, {"text"=>"E", "index"=>"4"}], "questions_random_order"=>"true"}.symbolize_keys)
  end


  factory :params_short, :class => Hash do |f|
    f.question({:name => "Short answer"})
    f.body({"question_type"=>"Short Answer", "point"=>"2", "question"=>"So lon nhat co ba chu so la ? ", "answer_correct"=>"999", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_long, :class => Hash do |f|
    f.question({:name => "Long answer"})
    f.body({"question_type"=>"Long Answer", "point"=>"2", "question"=>"9 +  9 = ? ", "answer_correct"=>"18", "questions_random_order"=>"true"}.symbolize_keys)
  end

  # invaild params ***********************************************************************************************

  factory :params_multiple_choice_nil, :class => Hash do |f|
    f.question({:name => ""})
    f.body({"question_type"=>"Multiple Choice", "point"=>"", "question"=>"", "answer"=>["","","","",""],  "choices_random_order"=>"true", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_multiple_choice_question_nil, :class => Hash do |f|
    f.question({:name => "multiple choice"})
    f.body({"question_type"=>"Multiple Choice", "point"=>"2", "question"=>"", "answer"=>["77","23","44","55","66"], "choices_random_order"=>"true", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_multiple_choice_number_answer_less_than_2, :class => Hash do |f|
    f.question({:name => "multiple choice"})
    f.body({"question_type"=>"Multiple Choice", "point"=>"2", "question"=>"100 - 77 = ? ", "answer"=>["77"], "choices_random_order"=>"true", "questions_random_order"=>"true"}.symbolize_keys)
  end

  factory :params_multiple_choice_not_check_correct, :class => Hash do |f|
    f.question({:name => "multiple choice "})
    f.body({"question_type"=>"Multiple Choice", "point"=>"2", "question"=>"100 - 77 = ? ", "answer"=>["77","23","44","55","66"], "choices_random_order"=>"true", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_multiple_choice_answer_not_unique, :class => Hash do |f|
    f.question({:name => "multiple choice "})
    f.body({"question_type"=>"Multiple Choice", "point"=>"2", "question"=>"100 - 77 = ? ", "answer"=>["23","23","44","55","66"],"correct"=>"1", "choices_random_order"=>"true", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_multiple_choice_correct_answer_nil, :class => Hash do |f|
    f.question({:name => "multiple choice "})
    f.body({"question_type"=>"Multiple Choice", "point"=>"2", "question"=>"100 - 77 = ? ", "answer"=>["77","","44","55","66"],"correct"=>"1", "choices_random_order"=>"true", "questions_random_order"=>"true"}.symbolize_keys)

  end

  factory :params_multiple_answer_nil, :class => Hash do |f|
    f.question({:name => ""})
    f.body({"question_type"=>"Multiple Answer", "point"=>"", "question"=>"", "answer"=>[{"text"=>""}, {"text"=>""}, {"text"=>""}, {"text"=>""}, {"text"=>""}], "choices_random_order"=>"true", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_multiple_answer_with_body_question_nil, :class => Hash do |f|
    f.question({:name => "Multiple Answer"})
    f.body({"question_type"=>"Multiple Answer", "point"=>"", "question"=>"","answer"=>[{"text"=>"9", "correct"=>"0"}, {"text"=>"12", "correct"=>"1"}, {"text"=>"11"}, {"text"=>"7"}, {"text"=>"6", "correct"=>"4"}], "choices_random_order"=>"true", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_multiple_answer_body_answer_nil, :class => Hash do |f|
    f.question({:name => "multiple answer"})
    f.body({"question_type"=>"Multiple Answer", "point"=>"2", "question"=>"so nao chia het cho so 3", "answer"=>[{"text"=>""}, {"text"=>""}, {"text"=>""}, {"text"=>""}, {"text"=>""}], "choices_random_order"=>"true", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_multiple_answer_body_answer_not_unique, :class => Hash do |f|
    f.question({:name => "Multiple Answer"})
    f.body({"question_type"=>"Multiple Answer", "point"=>"", "question"=>"","answer"=>[{"text"=>"9", "correct"=>"0"}, {"text"=>"9", "correct"=>"1"}, {"text"=>"11"}, {"text"=>"7"}, {"text"=>"6", "correct"=>"4"}], "choices_random_order"=>"true", "questions_random_order"=>"true"}.symbolize_keys)
  end

  factory :params_true_false_body_question_nil, :class => Hash do |f|
    f.question({:name => "True fales"})
    f.body({"question_type"=>"True/False", "point"=>"2", "question"=>"", "answer"=>"false", "questions_random_order"=>"true"}.symbolize_keys)
  end

  factory :params_matching_nil, :class => Hash do |f|
    f.question({:name => "Matching"})
    f.body({"question_type"=>"Matching", "point"=>"2", "question"=>"", "answer"=>[{"stems"=>"", "choice"=>""}, {"stems"=>"", "choice"=>""}, {"stems"=>"", "choice"=>""}, {"stems"=>"", "choice"=>""}, {"stems"=>"", "choice"=>""}], "stems_random_order"=>"true", "choices_random_order"=>"random", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_matching_body_question_nil, :class => Hash do |f|
    f.question({:name => "Matching"})
    f.body({"question_type"=>"Matching", "point"=>"2", "question"=>"", "answer"=>[{"stems"=>"A", "choice"=>"a"}, {"stems"=>"B", "choice"=>"b"}, {"stems"=>"C", "choice"=>"c"}, {"stems"=>"D", "choice"=>"d"}, {"stems"=>"E", "choice"=>"e"}], "stems_random_order"=>"true", "choices_random_order"=>"random", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_matching_number_stems_less_than_2, :class => Hash do |f|
    f.question({:name => "Matching"})
    f.body({"question_type"=>"Matching", "point"=>"2", "question"=>"Chu hoa - chu thuong", "answer"=>[{"stems"=>"A", "choice"=>"a"}, {"stems"=>"", "choice"=>""}, {"stems"=>"", "choice"=>""}, {"stems"=>"", "choice"=>""}, {"stems"=>"", "choice"=>""}], "stems_random_order"=>"true", "choices_random_order"=>"random", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_matching_body_stems_not_matching_choice, :class => Hash do |f|
    f.question({:name => "Matching"})
    f.body({"question_type"=>"Matching", "point"=>"2", "question"=>"Chu hoa - chu thuong", "answer"=>[{"stems"=>"A", "choice"=>""}, {"stems"=>"B", "choice"=>"b"}, {"stems"=>"C", "choice"=>"c"}, {"stems"=>"D", "choice"=>"d"}, {"stems"=>"E", "choice"=>"e"}], "stems_random_order"=>"true", "choices_random_order"=>"random", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_matching_body_stems_not_unique, :class => Hash do |f|
    f.question({:name => "Matching"})
    f.body({"question_type"=>"Matching", "point"=>"2", "question"=>"Chu hoa - chu thuong", "answer"=>[{"stems"=>"A", "choice"=>"a"}, {"stems"=>"A", "choice"=>"b"}, {"stems"=>"C", "choice"=>"c"}, {"stems"=>"D", "choice"=>"d"}, {"stems"=>"E", "choice"=>"e"}], "stems_random_order"=>"true", "choices_random_order"=>"random", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_matching_body_choice_not_unique, :class => Hash do |f|
    f.question({:name => "Matching"})
    f.body({"question_type"=>"Matching", "point"=>"2", "question"=>"Chu hoa - chu thuong", "answer"=>[{"stems"=>"A", "choice"=>"a"}, {"stems"=>"A", "choice"=>"a"}, {"stems"=>"C", "choice"=>"c"}, {"stems"=>"D", "choice"=>"d"}, {"stems"=>"E", "choice"=>"e"}], "stems_random_order"=>"true", "choices_random_order"=>"random", "questions_random_order"=>"true"}.symbolize_keys)
  end

  factory :params_sorting_nil, :class => Hash do |f|
    f.question({:name => ""})
    f.body({"question_type"=>"Sorting", "point"=>"2", "question"=>"", "answer"=>["", "", "", "", ""], "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_sorting_body_question_nil, :class => Hash do |f|
    f.question({:name => "Sorting"})
    f.body({"question_type"=>"Sorting", "point"=>"2", "question"=>"", "answer"=>["A", "B", "C", "D", "E"], "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_sorting_number_body_answer_less_than_2, :class => Hash do |f|
    f.question({:name => "Sorting"})
    f.body({"question_type"=>"Sorting", "point"=>"2", "question"=>"Aphabet", "answer"=>["A", "", "", "", ""], "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_sorting_body_answer_nil, :class => Hash do |f|
    f.question({:name => "Sorting"})
    f.body({"question_type"=>"Sorting", "point"=>"2", "question"=>"Aphabet", "answer"=>["", "", "", "", ""], "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_sorting_body_answer_not_unique, :class => Hash do |f|
    f.question({:name => "Sorting"})
    f.body({"question_type"=>"Sorting", "point"=>"2", "question"=>"Aphabet", "answer"=>["A", "A", "C", "D", "E"], "questions_random_order"=>"true"}.symbolize_keys)
  end

  factory :params_short_nil, :class => Hash do |f|
    f.question({:name => ""})
    f.body({"question_type"=>"Short Answer", "point"=>"2", "question"=>"", "answer_correct"=>"", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_short_body_question_nil, :class => Hash do |f|
    f.question({:name => "Short answer"})
    f.body({"question_type"=>"Short Answer", "point"=>"2", "question"=>"", "answer_correct"=>"999", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_short_body_answer_nil, :class => Hash do |f|
    f.question({:name => "Short answer"})
    f.body({"question_type"=>"Short Answer", "point"=>"2", "question"=>"So lon nhat co ba chu so", "answer_correct"=>"", "questions_random_order"=>"true"}.symbolize_keys)
  end

  factory :params_long_nil, :class => Hash do |f|
    f.question({:name => "Long answer"})
    f.body({"question_type"=>"Long Answer", "point"=>"2", "question"=>"", "answer_correct"=>"", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_long_body_question_nil, :class => Hash do |f|
    f.question({:name => "Long answer"})
    f.body({"question_type"=>"Long Answer", "point"=>"2", "question"=>"", "answer_correct"=>"18", "questions_random_order"=>"true"}.symbolize_keys)
  end
  factory :params_long_body_answer_nil, :class => Hash do |f|
    f.question({:name => "Long answer"})
    f.body({"question_type"=>"Long Answer", "point"=>"2", "question"=>"9 +  9 = ? ", "answer_correct"=>"", "questions_random_order"=>"true"}.symbolize_keys)
  end

end
