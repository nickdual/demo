# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :additional_emergency_contact do
    id 1
    name "name"
    address "MyString"
    phone "MyString"
    email "test@gmail.com"
    user_id 10
  end
end
