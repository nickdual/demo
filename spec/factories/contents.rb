# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :content do
    id 1
    name "MyString"
    body "MyText"
    user_id 1
    draft false
    end
  factory :content_2, :class =>"Content" do
    id 2
    name "MyString content"
    body "MyText My String"
    user_id 1
    draft false
  end
end
