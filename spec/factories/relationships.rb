# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :relationship do
    user_id_a 1
    user_id_b 1
    relationship_type 1
  end
end
