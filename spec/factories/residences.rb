# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :residence do
    name "MyString"
    code 1
  end
end
