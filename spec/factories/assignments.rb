# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do

  #vaild
  factory :assignment,:class => Assignment do |f|
    f.assignment({"name"=>"Assignment 5", "description"=>"description", "assignment_library_id"=>"1", "grade_system_id"=>"1", "id"=>"", "user_ids"=>["2", "3", "4", "5",""]}.symbolize_keys)
  end

  factory :assignment_list_students_nil, :class => Assignment do |f|
    f.assignment({"name"=>"Assignment 1", "description"=>"", "assignment_library_id"=>"1", "grade_system_id"=>"1", "id"=>"", "user_ids"=>[""]}.symbolize_keys)
  end

  #invaild
  factory :assignment_nil, :class => Assignment do |f|
    f.assignment({"name"=>"", "description"=>"", "assignment_library_id"=>"1", "grade_system_id"=>"1", "id"=>"", "user_ids"=>[""]}.symbolize_keys)
  end

  factory :assignment_name_nil, :class => Assignment do |f|
    f.assignment({"name"=>"", "description"=>"", "assignment_library_id"=>"5", "grade_system_id"=>"1", "id"=>"", "user_ids"=>[""]}.symbolize_keys)
  end
  factory :assignment_assignment_library_nil, :class => Assignment do |f|
    f.assignment({"name"=>"Assignment 1", "description"=>"", "assignment_library_id"=>"", "grade_system_id"=>"1", "id"=>"", "user_ids"=>[""]}.symbolize_keys)
  end

  #params update
  factory :assignment_update, :class => Assignment do |f|
    f.assignment({"name"=>"Assignment 5", "description"=>"description", "assignment_library_id"=>"5", "grade_system_id"=>"2", "id"=>"1", "user_ids"=>["2", "3", ""]}.symbolize_keys)

  end
end
