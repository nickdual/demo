# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :phone do
    user_id 1
    phone "MyString"
    phonetype_id 1
  end
end
