# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :grading_detail do
    body "MyText"
    question_id 1
    grading_id 1
  end
end
