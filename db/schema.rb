# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130305070856) do

  create_table "additional_emergency_contacts", :force => true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "phone"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "user_id"
    t.string   "email"
  end

  create_table "additional_emergency_contacts_educations", :id => false, :force => true do |t|
    t.integer "education_id"
    t.integer "additional_emergency_contact_id"
  end

  create_table "addresses", :force => true do |t|
    t.integer  "user_id"
    t.string   "street"
    t.string   "street2"
    t.integer  "city_id"
    t.integer  "state_id"
    t.integer  "postal_code"
    t.string   "comment"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "addresses", ["city_id"], :name => "index_addresses_on_city_id"
  add_index "addresses", ["state_id"], :name => "index_addresses_on_state_id"

  create_table "assignment_libraries", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "assignment_libraries_questions", :id => false, :force => true do |t|
    t.integer "question_id"
    t.integer "assignment_library_id"
  end

  create_table "assignments", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "assignment_library_id"
    t.integer  "grade_system_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "assignments_users", :id => false, :force => true do |t|
    t.integer "assignment_id"
    t.integer "user_id"
  end

  add_index "assignments_users", ["assignment_id", "user_id"], :name => "index_assignments_users_on_assignment_id_and_user_id"
  add_index "assignments_users", ["user_id", "assignment_id"], :name => "index_assignments_users_on_user_id_and_assignment_id"

  create_table "badges", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "image_url"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "cities", :force => true do |t|
    t.integer "country_id",                                                  :null => false
    t.integer "geonames_id",                                                 :null => false
    t.string  "name",                                                        :null => false
    t.string  "asciiname"
    t.text    "alternate_name"
    t.decimal "latitude",                     :precision => 14, :scale => 8, :null => false
    t.decimal "longitude",                    :precision => 14, :scale => 8, :null => false
    t.string  "country_iso_code_two_letters"
    t.integer "geonames_timezone_id"
  end

  add_index "cities", ["country_id", "name"], :name => "index_cities_on_country_id_and_name", :unique => true
  add_index "cities", ["geonames_id"], :name => "index_cities_on_geonames_id", :unique => true

  create_table "comments", :force => true do |t|
    t.integer  "commentable_id",   :default => 0
    t.string   "commentable_type", :default => ""
    t.string   "title",            :default => ""
    t.text     "body",             :default => ""
    t.string   "subject",          :default => ""
    t.integer  "user_id",          :default => 0,  :null => false
    t.integer  "parent_id"
    t.integer  "lft"
    t.integer  "rgt"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
  end

  add_index "comments", ["commentable_id"], :name => "index_comments_on_commentable_id"
  add_index "comments", ["user_id"], :name => "index_comments_on_user_id"

  create_table "concepts", :force => true do |t|
    t.integer  "subject_id"
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "concepts", ["name"], :name => "index_concepts_on_name"
  add_index "concepts", ["subject_id"], :name => "index_concepts_on_subject_id"

  create_table "content_photos", :force => true do |t|
    t.string   "description"
    t.string   "image"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "filename"
  end

  create_table "contents", :force => true do |t|
    t.string   "name"
    t.text     "body"
    t.integer  "user_id"
    t.boolean  "draft"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "parent_id"
  end

  create_table "countries", :force => true do |t|
    t.string  "iso_code_two_letter",   :null => false
    t.string  "iso_code_three_letter", :null => false
    t.integer "iso_number",            :null => false
    t.string  "name",                  :null => false
    t.string  "capital"
    t.string  "continent"
    t.integer "geonames_id",           :null => false
  end

  add_index "countries", ["geonames_id"], :name => "index_countries_on_geonames_id"
  add_index "countries", ["iso_code_two_letter"], :name => "index_countries_on_iso_code_two_letter", :unique => true

  create_table "educations", :force => true do |t|
    t.string   "name"
    t.integer  "code"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "elements", :force => true do |t|
    t.integer  "concept_id"
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "elements", ["concept_id"], :name => "index_elements_on_concept_id"
  add_index "elements", ["name"], :name => "index_elements_on_name"

  create_table "emails", :force => true do |t|
    t.integer  "user_id"
    t.string   "email"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "emails", ["email"], :name => "index_emails_on_email"
  add_index "emails", ["user_id"], :name => "index_emails_on_user_id"

  create_table "game_assets", :force => true do |t|
    t.string   "name"
    t.integer  "price"
    t.text     "description"
    t.string   "image_url"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "game_assets", ["name"], :name => "index_game_assets_on_name"

  create_table "grade_systems", :force => true do |t|
    t.string   "name"
    t.text     "settings"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "grading_details", :force => true do |t|
    t.text     "body"
    t.integer  "question_id"
    t.integer  "grading_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.text     "comment"
    t.float    "point"
  end

  create_table "gradings", :force => true do |t|
    t.text     "body"
    t.integer  "assignment_id"
    t.integer  "user_id"
    t.integer  "grade_system_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.text     "comment"
    t.float    "point"
  end

  create_table "hall_of_fames", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.decimal  "amount"
    t.text     "reason"
    t.string   "owner"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "hall_of_fames", ["name"], :name => "index_hall_of_fames_on_name"
  add_index "hall_of_fames", ["owner"], :name => "index_hall_of_fames_on_owner"

  create_table "healths", :force => true do |t|
    t.string   "name"
    t.string   "comment"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "healths_users", :id => false, :force => true do |t|
    t.integer "health_id"
    t.integer "user_id"
  end

  add_index "healths_users", ["health_id", "user_id"], :name => "index_healths_users_on_health_id_and_user_id"
  add_index "healths_users", ["user_id", "health_id"], :name => "index_healths_users_on_user_id_and_health_id"

  create_table "hobbies", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "hobbies", ["name"], :name => "index_hobbies_on_name"

  create_table "institutions", :force => true do |t|
    t.string   "name"
    t.string   "logo"
    t.string   "street"
    t.string   "street2"
    t.integer  "city_id"
    t.integer  "state_id"
    t.string   "postal_code"
    t.string   "comment"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "country_id"
  end

  add_index "institutions", ["country_id"], :name => "index_institutions_on_country_id"
  add_index "institutions", ["name"], :name => "index_institutions_on_name"

  create_table "links", :force => true do |t|
    t.string   "url"
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "menu_items", :force => true do |t|
    t.string   "name"
    t.string   "link"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "phones", :force => true do |t|
    t.integer  "user_id"
    t.string   "phone"
    t.integer  "phonetype_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "phones", ["phonetype_id"], :name => "index_phones_on_phonetype_id"
  add_index "phones", ["user_id"], :name => "index_phones_on_user_id"

  create_table "phonetypes", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "questions", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.text     "body"
    t.boolean  "draft"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "questions", ["user_id"], :name => "index_questions_on_user_id"

  create_table "races", :force => true do |t|
    t.string   "name"
    t.string   "comment"
    t.integer  "code"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "races", ["code"], :name => "index_races_on_code"

  create_table "races_users", :id => false, :force => true do |t|
    t.integer "race_id"
    t.integer "user_id"
  end

  create_table "relationship_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "relationship_types", ["name"], :name => "index_relationship_types_on_name"

  create_table "relationships", :force => true do |t|
    t.integer  "user_id_a"
    t.integer  "user_id_b"
    t.integer  "relationship_type_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  add_index "relationships", ["relationship_type_id"], :name => "index_relationships_on_relationship_type"
  add_index "relationships", ["relationship_type_id"], :name => "index_relationships_on_relationship_type_id"
  add_index "relationships", ["user_id_a"], :name => "index_relationships_on_user_id_a"
  add_index "relationships", ["user_id_b"], :name => "index_relationships_on_user_id_b"

  create_table "residences", :force => true do |t|
    t.string   "name"
    t.integer  "code"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "rooms", :force => true do |t|
    t.string   "name"
    t.string   "location"
    t.integer  "institution_id"
    t.string   "descritption"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "rooms", ["institution_id"], :name => "index_rooms_on_institution_id"

  create_table "school_classes", :force => true do |t|
    t.integer  "school_grade_id"
    t.integer  "teacher_id"
    t.integer  "year"
    t.integer  "institution_id"
    t.integer  "room_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "school_classes", ["institution_id"], :name => "index_school_classes_on_school_id"
  add_index "school_classes", ["room_id"], :name => "index_school_classes_on_room_id"
  add_index "school_classes", ["school_grade_id"], :name => "index_school_classes_on_grade_id"
  add_index "school_classes", ["teacher_id"], :name => "index_school_classes_on_teacher_id"
  add_index "school_classes", ["year"], :name => "index_school_classes_on_year"

  create_table "school_grades", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "sponsors", :force => true do |t|
    t.string   "company"
    t.string   "firstname"
    t.string   "lastname"
    t.string   "street"
    t.integer  "city_id"
    t.string   "state"
    t.string   "postalcode"
    t.string   "phone"
    t.string   "email"
    t.decimal  "amount"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "description"
  end

  add_index "sponsors", ["city_id"], :name => "index_sponsors_on_city_id"
  add_index "sponsors", ["company"], :name => "index_sponsors_on_company"

  create_table "states", :force => true do |t|
    t.string   "name_short"
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "subjects", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "subjects", ["name"], :name => "index_subjects_on_name"

  create_table "taggings", :force => true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       :limit => 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id"], :name => "index_taggings_on_tag_id"
  add_index "taggings", ["taggable_id", "taggable_type", "context"], :name => "index_taggings_on_taggable_id_and_taggable_type_and_context"

  create_table "tags", :force => true do |t|
    t.string "name"
  end

  create_table "uploaders", :force => true do |t|
    t.string   "filename"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                :default => "", :null => false
    t.string   "encrypted_password",                   :default => ""
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                        :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                           :null => false
    t.datetime "updated_at",                                           :null => false
    t.string   "name"
    t.date     "dob"
    t.string   "first"
    t.string   "middle"
    t.string   "gender"
    t.string   "ethnicity"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "last"
    t.string   "invitation_token",       :limit => 60
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.integer  "school_class_id"
    t.integer  "residence_id"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["ethnicity"], :name => "index_users_on_ethnicity"
  add_index "users", ["gender"], :name => "index_users_on_gender"
  add_index "users", ["invitation_token"], :name => "index_users_on_invitation_token"
  add_index "users", ["invited_by_id"], :name => "index_users_on_invited_by_id"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "users_roles", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "index_users_roles_on_user_id_and_role_id"

end
