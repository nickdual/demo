class HealthsUsers < ActiveRecord::Migration
  def up
    create_table :healths_users, :id => false do |t|
      t.integer  :health_id
      t.integer  :user_id
    end
    add_index :healths_users, [:health_id, :user_id]
    add_index :healths_users, [:user_id, :health_id]
  end

  def self.down
    drop_table :healths_users
  end
end
