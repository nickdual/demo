class CreateAssignmentLibraries < ActiveRecord::Migration
  def change
    create_table :assignment_libraries do |t|
      t.string :name
      t.string :description
      t.timestamps
    end
  end
end
