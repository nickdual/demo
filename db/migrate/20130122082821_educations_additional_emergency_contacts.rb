class EducationsAdditionalEmergencyContacts < ActiveRecord::Migration
  def up
    create_table :additional_emergency_contacts_educations, :id => false do |t|
      t.integer  :education_id
      t.integer  :additional_emergency_contact_id
    end
    #add_index :educations_additional_emergency_contacts, [:education_id, :additional_emergency_contact_id]
    #add_index :educations_additional_emergency_contacts, [:additional_emergency_contact_id, :education_id]
  end

  def self.down
    drop_table :educations_additional_emergency_contacts
  end
end
