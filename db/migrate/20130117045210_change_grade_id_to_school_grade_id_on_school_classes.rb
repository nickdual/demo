class ChangeGradeIdToSchoolGradeIdOnSchoolClasses < ActiveRecord::Migration
  def up
    rename_column :school_classes, :grade_id, :school_grade_id
    
  end

  def down
  end
end
