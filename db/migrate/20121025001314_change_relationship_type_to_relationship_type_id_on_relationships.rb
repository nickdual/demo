class ChangeRelationshipTypeToRelationshipTypeIdOnRelationships < ActiveRecord::Migration
  def up
    rename_column :relationships, :relationship_type, :relationship_type_id
    add_index :relationships, :relationship_type_id
  end

  def down
  end
end
