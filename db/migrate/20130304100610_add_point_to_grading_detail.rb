class AddPointToGradingDetail < ActiveRecord::Migration
  def change
    add_column :grading_details, :point, :float
  end
end
