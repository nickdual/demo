class CreateSchoolClasses < ActiveRecord::Migration
  def change
    create_table :school_classes do |t|
      t.integer :grade_id
      t.integer :teacher_id
      t.integer :year
      t.integer :school_id
      t.integer :room_id

      t.timestamps
    end
    add_index :school_classes, :grade_id
    add_index :school_classes, :teacher_id
    add_index :school_classes, :year
    add_index :school_classes, :school_id
    add_index :school_classes, :room_id
  end
end
