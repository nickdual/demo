class CreateInstitutions < ActiveRecord::Migration
  def change
    create_table :institutions do |t|
      t.string :name
      t.string :logo
      t.string :street
      t.string :street2
      t.integer :city_id
      t.integer :state_id
      t.string :postal_code
      t.string :comment

      t.timestamps
    end
    add_index :institutions, :name
  end
end
