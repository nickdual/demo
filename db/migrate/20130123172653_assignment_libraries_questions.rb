class AssignmentLibrariesQuestions < ActiveRecord::Migration
  def up
    create_table :assignment_libraries_questions, :id => false do |t|
      t.integer  :question_id
      t.integer  :assignment_library_id
    end

  end
  def self.down
    drop_table :questions_assignment_libraries
  end
end




