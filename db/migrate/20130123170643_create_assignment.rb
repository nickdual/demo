class CreateAssignment< ActiveRecord::Migration
  def change
    create_table :assignments do |t|
      t.string :name
      t.string :description
      t.integer :assignment_library_id
      t.integer :grade_system_id
      t.timestamps
    end

  end
end
