class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :name
      t.integer :user_id
      t.text :body
      t.boolean :draft

      t.timestamps
    end
    add_index :questions, :user_id
  end
end
