class AddEthnicityToUsers < ActiveRecord::Migration
  def change
    add_column :users, :ethnicity, :string
    add_index :users, :ethnicity
  end
end
