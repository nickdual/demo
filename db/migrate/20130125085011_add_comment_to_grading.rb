class AddCommentToGrading < ActiveRecord::Migration
  def change
    add_column :gradings, :comment, :text
  end
end
