class CreateContentPhotos < ActiveRecord::Migration
  def change
    create_table :content_photos do |t|
      t.string :description
      t.string :image

      t.timestamps
    end
  end
end
