class AddCountryIdToInstitutions < ActiveRecord::Migration
  def change
    add_column :institutions, :country_id, :integer
    add_index :institutions, :country_id
  end
end
