class CreateAdditionalEmergencyContacts < ActiveRecord::Migration
  def change
    create_table :additional_emergency_contacts do |t|
      t.string :name
      t.string :address
      t.string :phone

      t.timestamps
    end
  end
end
