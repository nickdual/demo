class CreateRelationships < ActiveRecord::Migration
  def change
    create_table :relationships do |t|
      t.integer :user_id_a
      t.integer :user_id_b
      t.integer :relationship_type

      t.timestamps
    end
    add_index :relationships, :user_id_a
    add_index :relationships, :user_id_b
    add_index :relationships, :relationship_type
  end
end
