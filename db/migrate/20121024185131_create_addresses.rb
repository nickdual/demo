class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.integer :user_id
      t.string :street
      t.string :street2
      t.integer :city_id
      t.integer :state_id
      t.integer :postal_code
      t.string :comment

      t.timestamps
    end
    add_index :addresses, :city_id
    add_index :addresses, :state_id
  end
end
