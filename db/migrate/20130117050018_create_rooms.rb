class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.string :name
      t.string :location
      t.integer :institution_id
      t.string :descritption

      t.timestamps
    end
    add_index :rooms, :institution_id
  end
end
