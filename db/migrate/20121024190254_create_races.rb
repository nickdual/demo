class CreateRaces < ActiveRecord::Migration
  def change
    create_table :races do |t|
      t.string :name
      t.string :comment
      t.integer :code

      t.timestamps
    end
    add_index :races, :code
  end
end
