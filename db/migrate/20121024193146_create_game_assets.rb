class CreateGameAssets < ActiveRecord::Migration
  def change
    create_table :game_assets do |t|
      t.string :name
      t.integer :price
      t.text :description
      t.string :image_url

      t.timestamps
    end
    add_index :game_assets, :name
  end
end
