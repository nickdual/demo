class CreateHobbies < ActiveRecord::Migration
  def change
    create_table :hobbies do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
    add_index :hobbies, :name
  end
end
