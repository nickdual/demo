class AddCommentToGradingDetail < ActiveRecord::Migration
  def change
    add_column :grading_details, :comment, :text
  end
end
