class CreateSponsors < ActiveRecord::Migration
  def change
    create_table :sponsors do |t|
      t.string :company
      t.string :firstname
      t.string :lastname
      t.string :street
      t.integer :city_id
      t.string :state
      t.string :postalcode
      t.string :phone
      t.string :email
      t.decimal :amount

      t.timestamps
    end
    add_index :sponsors, :company

  end
end
