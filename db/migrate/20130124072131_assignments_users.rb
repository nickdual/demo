class AssignmentsUsers < ActiveRecord::Migration
  def up
    create_table :assignments_users, :id => false do |t|
      t.integer  :assignment_id
      t.integer  :user_id
    end
    add_index :assignments_users, [:assignment_id, :user_id]
    add_index :assignments_users, [:user_id, :assignment_id]
  end

  def self.down
    drop_table :assignments_users
  end
end
