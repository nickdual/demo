class CreateGradeSystems < ActiveRecord::Migration
  def change
    create_table :grade_systems do |t|
      t.string :name
      t.text :settings

      t.timestamps
    end
  end
end
