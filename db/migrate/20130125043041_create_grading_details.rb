class CreateGradingDetails < ActiveRecord::Migration
  def change
    create_table :grading_details do |t|
      t.text :body
      t.integer :question_id
      t.integer :grading_id

      t.timestamps
    end
  end
end
