class AddPointToGrading < ActiveRecord::Migration
  def change
    add_column :gradings, :point, :float
  end
end
