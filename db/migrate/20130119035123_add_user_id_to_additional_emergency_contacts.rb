class AddUserIdToAdditionalEmergencyContacts < ActiveRecord::Migration
  def change
    add_column :additional_emergency_contacts, :user_id, :integer
  end
end
