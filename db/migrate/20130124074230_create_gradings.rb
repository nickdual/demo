class CreateGradings < ActiveRecord::Migration
  def change
    create_table :gradings do |t|
      t.text :body
      t.integer :assignment_id
      t.integer :user_id
      t.integer :grade_system_id
      t.timestamps
    end
  end
end
