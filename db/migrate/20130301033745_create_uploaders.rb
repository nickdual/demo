class CreateUploaders < ActiveRecord::Migration
  def change
    create_table :uploaders do |t|
      t.string :filename
      t.string :description

      t.timestamps
    end
  end
end
