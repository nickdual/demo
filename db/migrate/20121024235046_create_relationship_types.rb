class CreateRelationshipTypes < ActiveRecord::Migration
  def change
    create_table :relationship_types do |t|
      t.string :name

      t.timestamps
    end
    add_index :relationship_types, :name
  end
end
