class AddCityIdToIndex < ActiveRecord::Migration
  def change
    add_index :cities, [:country_id, :name],:unique => true
  end
end
