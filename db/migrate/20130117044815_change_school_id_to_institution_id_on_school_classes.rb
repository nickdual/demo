class ChangeSchoolIdToInstitutionIdOnSchoolClasses < ActiveRecord::Migration
  def up
    rename_column :school_classes, :school_id, :institution_id
  end

  def down
  end
end
