class AddResidenceIdToUser < ActiveRecord::Migration
  def change
    add_column :users, :residence_id, :integer
  end
end
