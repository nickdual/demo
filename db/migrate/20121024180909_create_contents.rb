class CreateContents < ActiveRecord::Migration
  def change
    create_table :contents do |t|
      t.string :name
      t.text :body
      t.integer :user_id
      t.boolean :draft

      t.timestamps
    end
  end
end
