class CreatePhones < ActiveRecord::Migration
  def change
    create_table :phones do |t|
      t.integer :user_id
      t.string :phone
      t.integer :phonetype_id

      t.timestamps
    end
    add_index :phones, :user_id
    add_index :phones, :phonetype_id
  end
end
