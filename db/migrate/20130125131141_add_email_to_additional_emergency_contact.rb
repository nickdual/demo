class AddEmailToAdditionalEmergencyContact < ActiveRecord::Migration
  def change
    add_column :additional_emergency_contacts, :email, :string
  end
end
