class CreateConcepts < ActiveRecord::Migration
  def change
    create_table :concepts do |t|
      t.integer :subject_id
      t.string :name

      t.timestamps
    end
    add_index :concepts, :subject_id
    add_index :concepts, :name
  end
end
