class AddFileNameToContentPhotos < ActiveRecord::Migration
  def change
    add_column :content_photos, :filename, :string
  end
end
