class CreateElements < ActiveRecord::Migration
  def change
    create_table :elements do |t|
      t.integer :concept_id
      t.string :name

      t.timestamps
    end
    add_index :elements, :concept_id
    add_index :elements, :name
  end
end
