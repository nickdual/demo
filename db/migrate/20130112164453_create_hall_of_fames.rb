class CreateHallOfFames < ActiveRecord::Migration
  def change
    create_table :hall_of_fames do |t|
      t.string :name
      t.text :description
      t.decimal :amount
      t.text :reason
      t.string :owner

      t.timestamps
    end
    add_index :hall_of_fames, :name
    add_index :hall_of_fames, :owner
  end
end
